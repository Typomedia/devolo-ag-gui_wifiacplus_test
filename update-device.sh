#!/bin/sh

DEVICE_IP=192.168.178.40
TFTP_SERVER_IP=192.168.178.87
TFTP_DIR=.

TAR_FILE=www.tar

# create TAR file in the TFTP directory
if [ -d "$TFTP_DIR" ]; then
    rm -f "$TFTP_DIR/$TAR_FILE";
    tar --exclude='./frontend' --exclude='.svn' --exclude='.DS_Store' -cf "$TFTP_DIR/$TAR_FILE" www;
fi

# trigger TFTP download
wget -O /dev/null --post-data=":sys:System.UpdateWWW=$TFTP_SERVER_IP,$TAR_FILE" "$DEVICE_IP/cgi-bin/htmlmgr"

# wait until the device has finished
sleep 8

