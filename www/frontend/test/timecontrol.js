// <script> 

var _errorcount = "${_errorcount}";
// tmp
_errorcount = "0";

var wtSchedActive = "${wlanSchedActive}";
var wtSchedActive_post = "${wlanSchedActive_post}";
var wtSchedActive_err = "${err_SchedActive}";
// tmp
wtSchedActive = "on";


var wtSchedWait = "${wlanSchedWait}";
var wtSchedWait_post = "${wlanSchedWait_post}";
var wtSchedActive_err = "${err_SchedWait}";
// tmp
wtwtSchedWait = "yes";

var errTimeInvalid = "${errTimeInvalid}";
var errTimeBegin = "${errTimeBeginn}";
// tmp
errTimeInvalid = "Zeitangabe ungültig";
errTimeBegin = "Startzeit > Endzeit";

var dateRegCheck = /([01]?[0-9]|2[0-3]):[0-5][0-9]$/;

var formValid;

var uuid = 0;

var addTime = new Object();
addTime.Entry = new Object();
addTime.Entry.Index = new Object();
addTime.Entry.On    = new Object();
addTime.Entry.Off   = new Object();

var timeTable = new Array();

timeTable[0] = new Object();
timeTable[0]['dayshort'] = 'mon';
timeTable[0]['daylong'] = 'Monday';
timeTable[0]['daytrans'] = '${day_mon}';
timeTable[0]['entries'] = new Array();

timeTable[1] = new Object();
timeTable[1]['dayshort'] = 'tue';
timeTable[1]['daylong'] = 'Tuesday';
timeTable[1]['daytrans'] = '${day_tue}';
timeTable[1]['entries'] = new Array();

timeTable[2] = new Object();
timeTable[2]['dayshort'] = 'wed';
timeTable[2]['daylong'] = 'Wednesday';
timeTable[2]['daytrans'] = '${day_wed}';
timeTable[2]['entries'] = new Array();

timeTable[3] = new Object();
timeTable[3]['dayshort'] = 'thu';
timeTable[3]['daylong'] = 'Thursday';
timeTable[3]['daytrans'] = '${day_thu}';
timeTable[3]['entries'] = new Array();

timeTable[4] = new Object();
timeTable[4]['dayshort'] = 'fri';
timeTable[4]['daylong'] = 'Friday';
timeTable[4]['daytrans'] = '${day_fri}';
timeTable[4]['entries'] = new Array();

timeTable[5] = new Object();
timeTable[5]['dayshort'] = 'sat';
timeTable[5]['daylong'] = 'Saturday';
timeTable[5]['daytrans'] = '${day_sat}';
timeTable[5]['entries'] = new Array();

timeTable[6] = new Object();
timeTable[6]['dayshort'] = 'sun';
timeTable[6]['daylong'] = 'Sunday';
timeTable[6]['daytrans'] = '${day_sun}';
timeTable[6]['entries'] = new Array();

var dM;
var eM;

$(document).ready(function() {

	// click on add
	$(document.body).on('click', '.timeTableAdd' ,function(){
		var a = $(this).attr("id").split("_");
		addEmptyEntry(a[0],a[1],a[2],a[3]);
	});

	// click on del
	$(document.body).on('click', '.timeTableDel' ,function(){
		var a = $(this).attr("id").split("_");
		delEntry(a[0],a[1],a[2],a[3]);
	});

	$("#buttonApply").click(function(e){
		$("#spinner").hide();
		formValid = true;
		checkForm();
		if ( formValid ){
			fillTimeTable();
		}
		// e.preventDefault();
	});

	$("#tmp_wlantimeOnOff").click(function(e){
		var iV = $('#tmp_wlantimeOnOff').prop('checked');
		switch ( iV ){
			case false:
				wtSchedActiveSetOff();
			break;
			case true:
				wtSchedActiveSetOn();
			break;
		}
	});

	init();
	console.log(timeTable);
	renderTimeTable();
});


function init(){
	tmplDayMarkup();
	tmplEntryMarkup();
	setWtSchedActiveMode();
	getSavedEntries();
}

/* wtactive-mode */
function setWtSchedActiveMode(){
	if ( wtSchedActive == "on" ){
		$("#tmp_wlantimeOnOff").prop("checked", true);
		toggleWtSchedActiveMode("show");
	}else{
		toggleWtSchedActiveMode("hide");
	}
}
function toggleWtSchedActiveMode(v){
	switch (v){
		case "show":
		toggleWtWaitMode("show");
		$("#timeTable").removeClass("inactive").addClass("active");
		break;
		case "hide":
		toggleWtWaitMode("hide");
		$("#timeTable").removeClass("active").addClass("inactive");
		break;
	}
}
function wtSchedActiveSetOff(){
	$("#wtSchedActive").val("off");
	toggleWtSchedActiveMode("hide");
}
function wtSchedActiveSetOn(){
	$("#wtSchedActive").val("on");
	toggleWtSchedActiveMode("show");
}

/* wtwait */
function setWtWaitMode(){
	if ( wtSchedWait == "yes" ){
		$("#tmp_wtwaitOnOff").prop("checked", true);
	}else{
		$("#tmp_wtwaitOnOff").prop("checked", false);
	}
}
function toggleWtWaitMode(v){
	switch (v){
		case "show":
		$("#wtWait").removeClass("inactive").addClass("active");
		break;
		case "hide":
		$("#wtWait").removeClass("active").addClass("inactive");
		break;
	}
}
function wtWaitOff(){
	$("#wtSchedWait").val("no");
}
function wtWaitOff(){
	$("#wtSchedWait").val("yes");
}

function getSavedEntries(){
	// savedEntries = eval("(<~ query `Wireless.Schedule.Day{*}:format=json` json ~>)"); 
}

function getEntriesPost(){
	
}

function fillTimeTable(){
	var c=0;
	var cc=0;
	var tD="";
	$("div[id^='timeTableEntry_']").each(function(i,v){
		$on = $(this).find(".maskDateOn").val();
		$off = $(this).find(".maskDateOff").val();
		// console.log($on,$off);
		if ($on != "" && $off != ""){
			$id = $(this).attr('id');
			$ar = $id.split("_");
			$dayindex = $ar[1];
			if($dayindex!=tD){
				c=0;
				tD=$dayindex;
			}
			c++;
			$daylong = timeTable[$dayindex]['daylong'];
			// console.log($daylong,c);
			$cl = $("#timeTableWildcard").clone();
			var n = $cl.attr("name");
			n = n.replace("_WILDCARD_DAY_",$daylong);
			n = n.replace("_WILDCARD_INDEX_",c);
			$cl.attr("name", n);
			$cl.attr("id", "timeTableWildcard"+cc);
			var obj = $.extend({}, addTime);
			obj.Entry.Index = c;
			obj.Entry.On = $on;
			obj.Entry.Off = $off;
			$cl.val(addslashes(JSON.stringify(obj)));
			$("#form_post").append($cl);
			cc++;
		}
	});
}

function tmplDayMarkup(){
	dM = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
	dM+= '<div class="row">';
	dM+= '<div class="row" id="row${dayshort}">';
	dM+= '<p class="component-title">${daylong}</p>';
	dM+= '</div>';
	dM+= '</div>';
	dM+= '</div>';
	dM+= '</div>';
}
function tmplEntryMarkup(){
	eM = '<div id="${entryId}" class="row">';
    eM+= ' <div class="col-xs-3 col-sm-4 col-md-4 col-lg-4">';
    eM+= '  <div class="form-group">';
    eM+= '   <input type="text" class="form-control caption-type-2 maskDateOff" placeholder="von">';
    eM+= '  </div>';
    eM+= ' </div>';
    eM+= ' <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    eM+= '  <p class="component-title text-center">&nbsp;</p>';
    eM+= ' </div>';
    eM+= ' <div class="col-xs-3 col-sm-4 col-md-4 col-lg-4">';
    eM+= '  <div class="form-group">';
    eM+= '   <input type="text" class="form-control caption-type-2 maskDateOff" placeholder="bis">';
    eM+= '  </div>';
    eM+= ' </div>';
    eM+= ' <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    eM+= '  <p class="component-title">&nbsp;</p>';
    eM+= ' </div>';
    eM+= ' <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">';
    eM+= '  <div class="button-delete timeTableDel" id="${delId}">';
    eM+= '   <a href="javascript:void(0);">';
    eM+= '    <div class="icon- button-delete-icon">d</div>';
    eM+= '   </a>';
    eM+= ' 	</div>';
    eM+= ' </div>';
    eM+= ' <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">';
    eM+= ' 	<div class="button-add timeTableAdd" id="${addId}">';
    eM+= '   <a href="javascript:void(0);">';
    eM+= '    <div class="icon- button-add-icon">b</div>';
    eM+= '   </a>';
    eM+= '  </div>';
    eM+= ' </div>';
    eM+= '</div>';
    eM+= '<div class="row error-message-box">';
    eM+= ' <p class="text-error" id="${errorId}"></p>';
    eM+= '</div>';
}



function renderTimeTable(){
	$("#timeTable").html("");
	$.template( "timeTableTmpl", dM );
	$.tmpl ( "timeTableTmpl", timeTable ).appendTo("#timeTable");
	$.each(timeTable, function( index, value ) {
		/*
		var l = value.entries.length;
		switch(l){
			case 0:
			value.entries.push(addTime);
			l = 1;
			break;
			default:
		
			break;
		}
		*/
		$.each(value.entries, function( indexe, valuee ) {
			valuee.entryId = "timeTableEntry_"+index+"_"+value.dayshort+"_"+uuid;
			valuee.delId = "timeTableDel_"+index+"_"+value.dayshort+"_"+uuid;
			valuee.addId = "timeTableAdd_"+index+"_"+value.dayshort+"_"+uuid;
			valuee.errorId = "timeTableErr_"+index+"_"+value.dayshort+"_"+uuid;
			$.template( "entryTmpl", eM );
			$.tmpl ( "entryTmpl", valuee ).appendTo("#row"+value.dayshort);
			uuid++;
		});
		addEmptyEntry("timeTableEntry",index,value.dayshort,uuid);
	});
}

function addEmptyEntry(a,b,c,d){
	addEmptyForm(a,b,c,d);
	checkBuForm(a,b,c,d);
}
function addEmptyForm(a,b,c,d){
	uuid++;

	var Entry = new Object();
	Entry.Index = new Object();
	Entry.On    = new Object();
	Entry.Off   = new Object();

	Entry.entryId = "timeTableEntry_"+b+"_"+c+"_"+uuid;
	Entry.delId = "timeTableDel_"+b+"_"+c+"_"+uuid;
	Entry.addId = "timeTableAdd_"+b+"_"+c+"_"+uuid;
	Entry.errorId = "timeTableErr_"+b+"_"+c+"_"+uuid;

	$.template( "entryTmpl", eM );
	$.tmpl ( "entryTmpl", Entry ).appendTo("#row"+c);
}

function delEntry(a,b,c,d){
	delForm(a,b,c,d);
}

function delForm(a,b,c,d){
	var l = $("div[id^='timeTableEntry_"+b+"']").length;
	if (l==1){
		addEmptyForm(a,b,c,d);
	}
	$("#timeTableEntry_"+b+"_"+c+"_"+d).remove();
	checkBuForm(a,b,c,d);
}

function checkBuForm(a,b,c,d){
	$("div[id^='timeTableEntry_"+b+"']:not(:last)").find(".timeTableAdd").hide();
	$("div[id^='timeTableEntry_"+b+"']:last").find(".timeTableAdd").show();
}

function checkForm(){
	$("div[id^='timeTableEntry']").each(function(index){
		var on = $(this).find(".maskDateOn").val();
		var off = $(this).find(".maskDateOff").val();
		var c = checkEntry($(this),on,off);
		formValid = formValid && c;
	});
}

function checkEntry(a,b,c){
	retval = true;
	if ( b == "" && c == "" ){
        releaseErrorText(a);
        return true;
	}
	eB = dateRegCheck.exec(b);
	eC = dateRegCheck.exec(c);
	if ( eB && eC ){
		if ( b > c ){
			setErrorText(a,errTimeBegin);
			var c = setHasError(a,b,c);
			return false;
		}
		releaseHasError(a,b,c);
		releaseErrorText(a);
		return true;
	}else{
		if (!eB){
			setErrorText(a,errTimeInvalid);
			var c = setHasError(a,b,false);
			retval = false;
		}else{
			releaseHasError(a,b,false);
			retval = true;
		}
		if (!eC){
			setErrorText(a,errTimeInvalid);
			var c = setHasError(a,false,c);
			retval = false;
		}else{
			releaseHasError(a,false,c);
			retval = true;
		}
	}
    return retval;
}

function releaseErrorText(a){
	$c = a.find(".error-message-box");
	$c.hide();
	$c.find(".text-error").html('');
}
function setErrorText(a,b){
	$c = a.find(".error-message-box");
	$c.show();
	$c.find(".text-error").html(b);
}
function setHasError(a,b,c){
    if ( b && c ){
		a.find(".form-group").addClass("has-error");
		return true;
	}
	if ( b ){
		a.find(".maskDateOn").parent().addClass("has-error");
		return true;
	}
	if ( c ){
		a.find(".maskDateOff").parent().addClass("has-error");
		return true;
	}
}
function releaseHasError(a,b,c){
	if ( b && c ){
		a.find(".form-group").removeClass("has-error");
		return true;
	}
	if ( b ){
		a.find(".maskDateOn").parent().removeClass("has-error");
		return true;
	}
	if ( c ){
		a.find(".maskDateOff").parent().removeClass("has-error");
		return true;
	}
}

// 	</script>