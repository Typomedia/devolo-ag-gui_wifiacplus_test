var connectionlist = null;
var refreshTime = 5000;
var connSt;
var notConnSt;

var macHeader = "<~ trans `page_status_wireless_mac_address` ~>";
var nameHeader = "<~ trans `page_status_wireless_name` ~>";
var ipHeader = "<~ trans `page_status_wireless_ip_address` ~>";
var timeHeader = "<~ trans `page_status_wireless_time_connected` ~>";
var rateHeader = "<~ trans `page_status_wireless_rate` ~>";
var mBits = "<~ trans `page_status_home_mbits` ~>";

var notAvail = "<~ trans `not_available` ~>";

$(document).ready(function() {
    init();
});

function init() {
    $.template("connectionTemplate", getTemplateForConnectionElement());
    // first load
    reloadConnections();
    // reload on refreshtime basis
    setInterval(function() {
        reloadConnections();
    }, refreshTime);
}

function checkConNotCon(o) {
    switch (o.State) {
        case "notConnected":
            itNotConnSt(o);
            break;
        case "connected":
            itConnSt(o);
            break;
    }
}

function initStArr() {
    connSt = new Array();
    notConnSt = new Array();
}

function itConnSt(o) {
    connSt.push(o);
}

function itNotConnSt(o) {
    notConnSt.push(o);
}

function reloadConnections() {
    $.get("/cgi-bin/htmlmgr?_file=getjson&service=knownstations").done(function(data) {
        // data = {"KnownStations":{"Station":[{"Ip":"","Mac":"78:31:C1:C3:D9:00","Name":"","Rate":"866","State":"notConnected","Time":"10:45 15.03.2014"},{"Ip":"192.168.178.97","Mac":"A8:06:00:B7:81:49","Name":"","Rate":"","State":"notConnected","Time":""}]}}
        // var $data = jQuery.extend(true, {}, data);
        // console.log($data);
        reloadConnectionList(data.KnownStations.Station);
    }).fail(function() {
        reloadConnectionList(false);
    });
}

function reloadConnectionList(stations) {
    initStArr();
    if (stations == false) {
        handleDataloadFailed();
    }
    if (stations instanceof Array) {
        for (i = 0; i < stations.length; i++) {
            checkConNotCon(stations[i]);
        }
    } else if (stations instanceof Object) {
        checkConNotCon(stations);
    } else {
        handleDataloadFailed();
    }
    c = connSt.length + notConnSt.length;
    // console.log(c);
    renderStatList(connSt, "divConnStat", c);
    renderStatList(notConnSt, "divNotConnStat", c);
}

function renderStatList(o, d, c) {
    leng = o.length;
    if (leng > 0) {
        $("#" + d).removeClass("inactive").addClass("active").empty();
        $("#" + d + "Empty").removeClass("active").addClass("inactive");
        c = 0;
        for (i = 0; i < leng; i++) {
            obj = o[i];
            if (c < leng) {
                // obj.classHR = "inactive";
            }
            if (obj.Name == '') {
                obj.Name = notAvail;
                // obj.className = "inactive";
            }
            if (obj.Ip == '') {
                obj.Ip = notAvail;
                // obj.classIp = "inactive";
            }
            if (obj.Rate == '') {
                obj.Rate = notAvail;
                obj.classRatePost = "inactive";
                // obj.classRate = "inactive";
            }
            if (obj.Time == '') {
                obj.Time = notAvail;
                // obj.classTimePost = "inactive";
                // obj.classTime = "inactive";
            }else{

            }
            if(c==leng-1){
                obj.classHR = 'inactive';
            }
            $.tmpl("connectionTemplate", obj).appendTo("#" + d);
            c++;
        }
        if(d == "divNotConnStat"){
            $("#divNotWrap").removeClass("inactive");
        }
    } else {
        switch(d){
            case "divConnStat":
                $("#" + d).removeClass("active").addClass("inactive");
                $("#" + d + "Empty").removeClass("inactive").addClass("active");
            break;
            case "divNotConnStat":
                $("#divNotWrap").removeClass("inactive");
                $("#" + d).removeClass("active").addClass("inactive");
                if (c == 0) //the list has never been populated before
                {
                    //show empty list with hint about no known stations
                    $("#" + d + "Empty").removeClass("inactive").addClass("active");
                }
                else
                {
                    //hide hint about no known stations
                    $("#" + d + "Empty").removeClass("active").addClass("inactive");
                }
            break;
        }
    }
}

function handleDataloadFailed() {
    // console.warn("GET-Request for details failed.")
}

function getTemplateForConnectionElement(){
    eM = '<div class="row col-xs-12 col-sm-12 col-md-9 col-lg-8">';
    eM+= '  <div class="row">';
    eM+= '      <p class="component-title"></p>';
    eM+= '          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 approved-wlan-stations multiline">';
    eM+= '              <span class="caption-type-2">';
    eM+= '                  <table class="wlan-monitor">';
    eM+= '                    <tbody>';
    eM+= '                      <tr>';
    eM+= '                          <td><span class="text-black">'+macHeader+'</span></td>';
    eM+= '                          <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    eM+= '                          <td>$' + '{Mac}</td>';
    eM+= '                      </tr>';
    eM+= '                      <tr>';
    eM+= '                          <td><span class="text-black">'+nameHeader+'</span></td>';
    eM+= '                          <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    eM+= '                          <td>$' + '{Name}</td>';
    eM+= '                      </tr>';
    eM+= '                      <tr>';
    eM+= '                          <td><span class="text-black">'+ipHeader+'</span></td>';
    eM+= '                          <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    eM+= '                          <td>$' + '{Ip}</td>';
    eM+= '                      </tr>';
    eM+= '                      <tr>';
    eM+= '                          <td><span class="text-black">'+rateHeader+'</span></td>';
    eM+= '                          <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    eM+= '                          <td>$' + '{Rate}'+' <span class="$' + '{classRatePost}">['+mBits+']</span></td>';
    eM+= '                      </tr>';
    eM+= '                      <tr>';
    eM+= '                          <td><span class="text-black">'+timeHeader+'</span></td>';
    eM+= '                          <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    eM+= '                          <td>$' + '{Time}</td>';
    eM+= '                      </tr>';
    eM+= '                    </tbody>';
    eM+= '                  </table>';
    eM+= '              </span>';
    eM+= '          </div>';
    eM+= '      </div>';
    eM+= '      <div class="row $' + '{classHR}><hr></div>';
    eM+= '</div>';
    return eM;
}