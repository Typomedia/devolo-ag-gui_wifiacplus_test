var _errorcount = "${_errorcount}";

var current_mac_address = "${mac_address_post}";
// currentDevice called async, so check if not null
var currentDevice;
// set mac_address as post param to make submit redirect work
$("#_mac_addr").val(current_mac_address);
var dlan_device = "${dlan_device}";

var bubbleTextFR = "<~ trans `page_homeplug_message_factory_reset_station` ~>";
var bubbleTextRE = "<~ trans `page_homeplug_message_release_station` ~>";

$(document).ready(function() {
    $("#release_device").click(function() {
        showInputChangeHint(bubbleTextRE);
        prepareReleaseDevice();
        showOnChangeMessage();
    });
    $("#reset_device").click(function() {
        showInputChangeHint(bubbleTextFR);
        prepareFactoryReset();
        showOnChangeMessage();
    });
    $("#buttonApply").click(function() {
        if (!$("#post_factory_reset").is(":disabled")) {
            $("#form_post").submit();
        } else if (!$("#post_leave_avln").is(":disabled")) {
            $("#form_post").submit();
        } else {
            prepareNameChange();
            $("#form_post").submit();
        }
    });
    // modal footer close
    $("#modalFooterClose").click(function(e) {
        resetHiddenFRMForm();
    });
    init();
});

function init() {
    loadDevice();
}

function loadDevice() {
    $.get("/cgi-bin/htmlmgr?_file=getjson&service=hpdevices").done(function(data) {
        searchDeviceInList(data);
    }).fail(function() {
        deviceNotFound();
    });
}

function searchDeviceInList(parsedData) {
    for (var i = 0; i < parsedData.length && typeof parsedData[i].mac !== undefined; i++) {
        if (parsedData[i].mac == current_mac_address) {
            // mac address found
            deviceFound(parsedData[i]);
            return;
        }
    }
    // mac address not found
    deviceNotFound();
}

function deviceFound(device) {
    // set retrieved values
    /// console.log(device);
    currentDevice = device;
    $("#dlan_device").text(device.name);
    $("#mac_address").text(device.mac);
    $("#device_name").val(device.ustr);
    $("#device_name").prop("disabled", false);
    // enable form specific buttons 
    if (device.loc == "local") {
        makeFormForLocalDevice();
        $("#post_setname_local").disabled = false;
    } else if (device.loc == "remote") {
        makeFormForRemoteDevice();
    }
}

function deviceNotFound() {
    // remove content
    $("#form-wrapper").hide();
    // display header error
    $("#headerHint").removeClass("inactive").addClass("active");
    $("#headerHintText").html("<~ trans `header_error` ~>");
    // show detail error
    $("#no-mac-error").removeClass("inactive");
    $("#securety-id-error").addClass("active");
}

function makeFormForLocalDevice() {
    // enable only the change name button
    $("#rename_device").prop("disabled", false);
    // $("#release_device,#reset_device").addClass("active").removeClass("inactive");
}

function makeFormForRemoteDevice() {
    // enable all buttons
    $("#reset_device").prop("disabled", false);
    $("#release_device").prop("disabled", false);
    $("#rename_device").prop("disabled", false);
    $("#release_device,#reset_device").addClass("active").removeClass("inactive");
}
// submits the form for a name change
function prepareNameChange() {
    // reset the form
    resetHiddenFRMForm();
    var field_name = currentDevice.loc == "local" ? "#post_setname_local" : "#post_setname";
    $(field_name).val($("#device_name").val());
    $(field_name).prop("disabled", false);
}

function prepareFactoryReset() {
    // reset the form
    resetHiddenFRMForm();
    // enable facotry reset
    $("#post_factory_reset").prop("disabled", false);
}

function prepareReleaseDevice() {
    // check if device already called async
    if (!currentDevice) {
        console.warn("Device not loaded yet.");
        return false;
    }
    // reset the form
    resetHiddenFRMForm();
    $("#post_leave_avln").value = currentDevice.mac;
    $("#post_leave_avln").prop("disabled", false);
}
// disables all .frm hidden form fields
function resetHiddenFRMForm() {
    hideInputChangeHint();
    $("#post_factory_reset").prop("disabled", true);
    $("#post_leave_avln").prop("disabled", true);
    $("#post_setname").prop("disabled", true);
    $("post_setname_local").prop("disabled", true);
}

function showInputChangeHint(text) {
    $("#modalFooter").show();
    $("#modalFooterText").hide();
    $("#modalFooterText2").html(text);
    // $("#postShowDialogAgain").val("yes").prop("disabled", false);
}

function hideInputChangeHint() {
    // $("#postShowDialogAgain").prop("disabled", false);
}