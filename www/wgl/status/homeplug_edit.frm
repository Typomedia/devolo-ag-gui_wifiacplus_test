<input
    disabled
    type="hidden"
    id="post_factory_reset"
    name=":sys:HomePlug.Remote.Device{MacAddress=${_mac_address}}.Commands.FactoryReset"
    value="PLConly"
/>

<input 
    type="hidden"
    disabled
    id="post_leave_avln"
    name=":sys:HomePlug.Remote.Device{MacAddress=${_mac_address}}.Commands.LeaveAVLN"
    value=""
/>

<input 
    type="hidden"
    disabled
    id="post_setname"
    name=":sys:HomePlug.Remote.Device{MacAddress=${_mac_address}}.Config.UserString"
    value=""
/>
<input 
	name="_mac_address"
	type="hidden"
	id="_mac_addr"
/>
<input 
    type="hidden"
    disabled
    id="post_setname_local"
    name=":sys:HomePlug.Local.Device{Index=1}.Config.UserString"
    value=""
/>

<input type="hidden" name="_okdir"          value="spec"          id="post_okdir"           />
<input type="hidden" name="_okpage"         value="result"        id="post_okpage"          />
<input type="hidden" name="_okfollowdir"    value="status"        id="post_okfollowdir"     />
<input type="hidden" name="_okfollowpage"   value="homeplug"      id="post_okfollowpage"    />
<input type="hidden" name="_okplain"        value="1"             id="post_okplain"         />
<input type="hidden" name="_oktype"         value="dlanstatus"    id="post_oktype"          />
