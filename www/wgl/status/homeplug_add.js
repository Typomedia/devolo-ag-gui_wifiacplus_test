var _errorcount = "${_errorcount}";

var current_secid_error = 0;
var error_text_short = "<~ trans `error_secid_too_short` ~>"; //error 1
var error_text_long = "<~ trans `error_secid_too_long` ~>"; //error 2
var error_text_length_invalid = "<~ trans `error_secid_length_invalid` ~>"; //error 3
var error_text_chars_invalid = "<~ trans `error_secid_chars_invalid` ~>"; //error 4

$(document).ready(function() {
    $("input[id^='view_secid']").each(function(index) {
        $(this).keypress(function(event) {
            hideSecIdError();
            var code = event.which;
            if (!((code >= 97 && code <= 122) || (code >= 65 && code <= 90) || code == null || code == 0 || code == 9 || code == 8)) {
                event.preventDefault();
            }
        });
        $(this).keyup(function(event) {
            change_input_focus(event, $(this)[0]);
        });
    });
    $("#startSID").click(function() {
        if (doSave() && !$("#post_secid").is(":disabled")) {
            $("#spinner").show();
            $("#form_post").submit();
        } else {
            $("#spinner").hide();
        }
    });
    $("#startPLC").click(function() {
        $("#post_secid").prop("disabled", true);
        $("#post_pairingtoggle").prop("disabled", false);
        $("#spinner").show();
        $("#form_post").submit();
    });
    $("#add-by-sec-id").click(function() {
        $("#securityID").toggle();
    });
});

function check_secid(onsave) {
    current_secid_error = 0;
    temp = false;
    if (onsave) {
        for (i = 0; i < 4; i++) {
            var fieldname = "view_secid" + i;
            if ($("#" + fieldname).val().length < 4) {
                current_secid_error = 1; //too short
                temp = true;
            } else if ($("#" + fieldname).val().length > 4) {
                current_secid_error = 2; //too long
                temp = true;
            }
            if (current_secid_error == 0) {
                var secid_matches = $("#" + fieldname).val().match(/[A-Za-z]{4}/g);
                if (!secid_matches || secid_matches[0].length != $("#" + fieldname).val().length) {
                    current_secid_error = 4;
                    temp = true;
                }
            }
            if(temp){
            	$("#" + fieldname).parent().addClass("has-error");
            }else{
            	$("#" + fieldname).parent().removeClass("has-error");
            }
            temp = false;
        }
    }
    update_secid_error_text();
    return current_secid_error == 0
}

function update_secid_error_text() {
    switch (current_secid_error) {
        case 1:
            showSecIdError(error_text_short);
            break;
        case 2:
            showSecIdError(error_text_long);
            break;
        case 3:
            showSecIdError(error_text_length_invalid);
            break;
        case 4:
            showSecIdError(error_text_chars_invalid);
            break;
        default:
            hideSecIdError()
            break;
    }
}

function change_input_focus(evt, ipt) {
    // browser compatibility
    var code = evt.keyCode;
    if (code == 0) code = evt.charCode;
    // move to the next input field after typing the 4th character
    if ((code >= 97 && code <= 122) || (code >= 65 && code <= 90)) {
        if (ipt.value.length == 4) {
            $("#"+ipt.id).parent().removeClass("has-error");
            var nipt = null;
            if (ipt == $("#view_secid0")[0]) nipt = $("#view_secid1")[0];
            if (ipt == $("#view_secid1")[0]) nipt = $("#view_secid2")[0];
            if (ipt == $("#view_secid2")[0]) nipt = $("#view_secid3")[0];
            if (nipt != null) {
                nipt.focus();
                // workaround for IE
                nipt.value = nipt.value;
            }
        }
    }
    // move to the previous input field after removing the last character
    if (code == 8 || code == 46) {
        if (ipt.value.length == 0) {
            var pipt = null;
            if (ipt == $("#view_secid1")[0]) pipt = $("#view_secid0")[0];
            if (ipt == $("#view_secid2")[0]) pipt = $("#view_secid1")[0];
            if (ipt == $("#view_secid3")[0]) pipt = $("#view_secid2")[0];
            if (pipt != null) {
                pipt.focus();
                // workaround for IE
                pipt.value = pipt.value;
            }
        }
    }
}
// get the cannonical security id
function canonical_secid() {
    return $("input[id^='view_secid']").map(function() {
        return $(this).val().toUpperCase();
    }).get().join("-");
}
// store to hidden text fields
function doSave() {
    check_secid(true);
    if (current_secid_error != 0) {
        return false;
    }
    $("#post_secid").prop("disabled", false);
    $("#post_secid").val(canonical_secid());
    return true
}

function showSecIdError(text) {
    if (!$("#securety-id-error").hasClass('active')) {
        $("#securety-id-error").addClass('active');
    }
    $("#securety-id-error").find(".text-error").html(text);
}

function hideSecIdError() {
    $("#securety-id-error").removeClass('active');
}