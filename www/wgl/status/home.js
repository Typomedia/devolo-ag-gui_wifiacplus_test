var _errorcount = "${_errorcount}";
var bubbleEnabled = "${bubbleEnabled}";
/* radiomode */
var radioMode = "${radioMode}";
/* 2,4 ghz */
var wlanRadio0 = "${wlanRadio0}";
var security0 = "${security0}";
/* 5 ghz */
var wlanRadio1 = "${wlanRadio1}";
var security1 = "${security1}";
var unifiedRadios = "${unifiedRadios}";
var wlanRadio2 = "${wlanRadio0}";
var security2 = "${security0}";
/* guest */
var wlanRadio3 = "${wlanRadio3}";
var security3 = "${security3}";
var wifiOn = true;
var status;
var hpSecurity = "${hpSecurity}";
var webUiPwIsSet = "${webUiPwIsSet}";
var ethStatus = JSON.stringify("${ethStatus}");

/* ip */
var ipType = "${ipType}";
var refresh_allowed = false;
var ipAdr = "${ipAdr}";
var ipNet = "${ipNet}";
var ipGat = "${ipGat}";
var ipNam = "${ipNam}";
var refresh_time = 5000;
$(document).ready(function() {
    setNavMainBuHi("${_dir}");
    init();
});

function init() {
    setEthernet();
    setWifi();
    setDlan();
    setAccess();
    setIp();
    setMainRowVisible();
}

function setMainRowVisible(){
    $("#mainRow").removeClass("inactive");
}

function setWifi() {
    if (wlanRadio0 == "on" || wlanRadio1 == "on") {
        wifiOn = true;
    } else {
        wifiOn = false;
    }
    switch (wifiOn) {
        case true:
            $("#wifiOn").show();
            setHtml("wifiOnOff", "<~ trans `page_status_wireless_on` ~>");
            // access point
            // unified and both radios on 
            if(wlanRadio0 == "on" && wlanRadio1 == "on" && unifiedRadios == "yes"){
                $("#245ghz").removeClass("inactive");
                switch (security2) {
                    case "none":
                        $("#245ghzPW").hide();
                        inactiveActive("245ghzEr");
                        break;
                    default:
                        $("#245ghzPW").show();
                        activeInactive("245ghzEr");
                        break;
                }
            }else {
                // 2,4 ghz
                if(wlanRadio0 == "on"){
                    $("#24ghz").removeClass("inactive");
                    switch (security0) {
                        case "none":
                            $("#24ghzPW").hide();
                            inactiveActive("24ghzEr");
                            break;
                        default:
                            $("#24ghzPW").show();
                            activeInactive("24ghzEr");
                            break;
                    }
                }
                // 5 ghz
                if(wlanRadio1 == "on"){
                    $("#5ghz").removeClass("inactive");
                    switch (security1) {
                        case "none":
                            $("#5ghzPW").hide();
                            inactiveActive("5ghzEr");
                            break;
                        default:
                            $("#5ghzPW").show();
                            activeInactive("5ghzEr");
                            break;
                    }
                }
            }
            // guest account
            if(wlanRadio3 == "on"){
                $("#Guestghz").removeClass("inactive");
                switch (security3) {
                    case "none":
                        $("#GuestghzPW").hide();
                        inactiveActive("GuestghzEr");
                        break;
                    default:
                        $("#GuestghzPW").show();
                        activeInactive("GuestghzEr");
                        break;
                }
            }
            break;
        default:
            $("#wifiOn").hide();
            setHtml("wifiOnOff", "<~ trans `page_status_wireless_off` ~>");
            break;
    }
}

function setEthernet() {
    _getJSON("/cgi-bin/htmlmgr?_file=getjson&service=switchstatus", getSwitchStatus);
}

function getSwitchStatus(j) {
    // console.log(j);
    if (getObjType(j) == "Object") {
        if (j.Info.Port.length <= 3) {
            $.each(j.Info.Port, function(index, value) {
                if (value.LinkType != "disabled" && value.LinkState != "down") {
                    $("#port" + (index + 1) + "Safe").removeClass("unsafe").addClass("safe");
                } else {
                    $("#port" + (index + 1) + "p").removeClass("active").addClass("inactive");
                }
                inactiveActive("port" + (index + 1));
                setHtml("port" + (index + 1) + "Speed", value.LinkSpeed);
            });
        }
        $("#ethStatusPH").removeClass("active").addClass("inactive");
        $("#ethStatus").removeClass("inactive").addClass("active");
    }
}

function setDlan() {
    if (hpSecurity == "on") {
        $("#dlanIc").addClass("safe");
    }else{
        $("#dlanIc").addClass("unsafe");
    }
}

function setAccess(){
    if (webUiPwIsSet == "yes") {
        $("#webUiPwIsSet").addClass("safe");
    }else{
        $("#webUiPwIsSet").addClass("unsafe");
    }
}

function setIp() {
    if (ipType == "DHCP") {
        refresh_allowed = true;
        reloadIpSettings();
    	intervalSet = setInterval(reloadIpSettings, refresh_time);
    } else {
        if (ipAdr != "") {
            $("#ipAdr").removeClass("inactive");
            // inactiveActive("ipAdr");
            setHtml("ipAdrV", ipAdr);
        }
        if (ipNet != "") {
            $("#ipNet").removeClass("inactive");
            // inactiveActive("ipNet");
            setHtml("ipNetV", prefLngToNetmask(ipNet));
        }
        if (ipGat != "") {
            $("#ipGat").removeClass("inactive");
            // inactiveActive("ipGat");
            setHtml("ipGatV", ipGat);
        }
        if (ipNam != "") {
            $("#ipNam").removeClass("inactive");
            // inactiveActive("ipNam");
            setHtml("ipNamV", ipNam);
        }
    }
}

function reloadIpSettings() {
    $.get("/cgi-bin/htmlmgr?_file=getjson&service=networksettings").done(function(data) {
        console.log(data);
        refreshIpSettings(data);
    }).fail(function() {
        // reloadDevicesList(false);
    }).always(function() {})
}

function refreshIpSettings(d) {
    if (!refresh_allowed) return;
    settings = null;
    if (isObject(d)) {
        inactiveActive("ipAdr");
        setHtml("ipAdrV", d.Management.V4.IpAddress);
        inactiveActive("ipNet");
        setHtml("ipNetV", prefLngToNetmask(d.Management.V4.PrefixLength));
        if (d.Management.V4.DefaultGateway.IpDynamic != "0.0.0.0") {
            inactiveActive("ipGat");
            setHtml("ipGatV", d.Management.V4.DefaultGateway.IpDynamic);
        }
        if (d.Management.V4.Nameserver != "0.0.0.0") {
        	inactiveActive("ipNam");
            setHtml("ipNamV", d.Management.V4.Nameserver);
        } 
    }
}