var refreshIntervalInMs = 15000;

var macHeader = "<~ trans `page_status_wireless_mac_address` ~>";
var nameHeader = "<~ trans `page_status_wireless_name` ~>";
var ipHeader = "<~ trans `page_status_wireless_ip_address` ~>";
var timeHeader = "<~ trans `page_status_wireless_connect_time` ~>";
var rateHeader = "<~ trans `page_status_wireless_rate` ~>";
var mBits = "<~ trans `page_status_home_mbits` ~>";

var notAvail = "<~ trans `not_available` ~>";

$(document).ready(function() {
    init();
});

function init() {
    $.template("deviceTemplate", getStationTemplate());
    $.template("nodeviceTemplate", getNoStationConnectedTemplate());
    // reload devices 
    reloadDevices();
    // reload devices every x ms
    setInterval(function() {
        reloadDevices();
    }, refreshIntervalInMs);
}

function getNoStationConnectedTemplate() {
    return "<div class=\"row\">" + "<p class=\"caption-type-2\"><~ trans `page_status_dlan_no_remote_device` ~></p>" + "</div>";
}

function getStationTemplate() {
    eM  = '<div class="row">';
    eM += ' <div class="row col-xs-10 col-sm-10 col-md-10 col-lg-10">';
    eM += '  <p class="component-title"></p>';
    eM += '  <div class="row">';
    eM += '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 approved-wlan-stations multiline">';
    eM += '    <span class="caption-type-2">';
    eM += '     <table class="wlan-monitor">';
    eM += '      <tr>';
    eM += '       <td><span class="text-black">'+macHeader+'</span></td>';
    eM += '       <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    eM += '       <td>$' + '{mac}</td>';
    eM += '      </tr>';
    eM += '      <tr>';
    eM += '       <td><span class="text-black">'+nameHeader+'</span></td>';
    eM += '       <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    eM += '       <td>$' + '{deviceName}</td>';
    eM += '      </tr>';
    eM += '      <tr class="$' + '{classrx}">';
    eM += '       <td><span class="text-black">'+rateHeader+'</span></td>';
    eM += '       <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>';
    eM += '       <td>$' + '{rx}'+' ['+mBits+']</td>';
    eM += '      </tr>';
    eM += '     </table>';
    eM += '    </span>';
    eM += '   </div>';
    eM += '  </div>';
    eM += '</div>';
    eM += '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>';
    eM += ' <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    eM += '  <p class="component-title"></p>';
    eM += '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
    eM += '    <div class="button-add">';
    eM += '     <a href="${path_base}&_dir=status&_page=homeplug_edit&_mac_address=$' + '{mac}">';
    eM += '       <div class="icon- button-add-icon">i</div>';
    eM += '     </a>';
    eM += '    </div>';
    eM += '   </div>';
    eM += '   <div class="row">&nbsp;</div>';
    eM += '   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
    /*
    eM += '    <div class="button-add">';
    eM += '     <a href="javascript:void(0);">';
    eM += '      <div class="icon- button-add-icon">T</div>';
    eM += '     </a>';
    eM += '    </div>';
    */
    eM += '   </div>';
    eM += '  </div>';
    eM += '  <p class="caption-type-2">&nbsp;</p>';
    eM += '  <div class="row"><hr></div>';
    eM += '</div>';
    return eM;                
}

// called async by reloadDevices
function reloadDevicesList(parsedData) {
    if (parsedData === false) {
        // console.warn("Fehler! JSON kann nicht abgerfragt werden");
        return;
    }
    // remove all previous entries
    $("#remote-wrapper").empty();
    $("#local-wrapper").empty();
    var temp;
    for (var i = 0; i < parsedData.length && typeof parsedData[i].mac !== undefined; i++) {
        currentDevice = parsedData[i];
        currentDevice.deviceName = currentDevice.ustr ? currentDevice.ustr : currentDevice.name;
        temp = currentDevice.deviceName;
        if(temp){
            currentDevice.deviceName = temp.toString().htmlEncode();
        }
        if (currentDevice.loc == "remote") {
            currentDevice.tx = Math.round(currentDevice.tx);
            currentDevice.rx = Math.round(currentDevice.rx);
            $.tmpl("deviceTemplate", currentDevice).appendTo("#remote-wrapper");
        } else if (currentDevice.loc == "local") {
            currentDevice.classrx = "inactive";
            $.tmpl("deviceTemplate", currentDevice).appendTo("#local-wrapper");
        }
    }
    // local
    if (parsedData.length == 2) {
        $.tmpl("nodeviceTemplate", null).appendTo("#remote-wrapper")
    }
}

function reloadDevices() {
    $.get("/cgi-bin/htmlmgr?_file=getjson&service=hpdevices").done(function(data) {
        // console.log(data);
        reloadDevicesList(data);
    }).fail(function() {
        reloadDevicesList(false);
    }).always(function() {})
}

function blinkLED(m) {
    alert("blink-blink" + " " + m);
};