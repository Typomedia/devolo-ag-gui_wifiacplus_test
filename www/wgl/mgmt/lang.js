
var actLang = "${_actlang}";

$( document ).ready(function() {
	$("#buttonApply").click(function(e){
		$( "#form_post" ).submit();
		e.preventDefault();
	});
	$("input[id^='tmpLang']").click(function(e){
		actLang = $(this).val();
		setLang();
	});	
	init();
});

function init(){
	setLang();
	checkLang();
}

function setLang(){
	setValue("ref_lang", actLang);
}

function checkLang(){
	setProp("tmpLang_"+actLang,"checked",true);
}
