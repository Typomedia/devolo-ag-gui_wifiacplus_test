
var _errorcount = "${_errorcount}";

/* au */
var autoFU = "${autoFU}";
var autoFU_post = "${autoFU_post}";
if(_errorcount != "0" && autoFU_post != ""){
    autoFU = autoFU_post;
}
var bubbleTextAU = "${bubbleTextAU}";
var bubbleTextDU = "${bubbleTextDU}";
var bubbleTextFU = "${bubbleTextFU}";
var bubbleTextCU = "${bubbleTextCU}";
var bubbleTextCR = "${bubbleTextCR}";

var actionHolderDefault = "formPostAU";

$(document).ready(function() {
    $("#buttonApply").click(function(e){
        $a = $("#actionHolder").val();
        if ( $a != "" ){
            $("#"+$a).submit();
        }else{
            $("#spinner").hide();
        }
        e.preventDefault();
    });
     // auto fu
    $("#tmp_autoFUOnOff").click(function(e){
        var iV = $('#tmp_autoFUOnOff').prop('checked');
        switch ( iV ){
            case false:
                auSetOff();
                $("#modalFooter").hide();
            break;
            case true:
                auSetOn();
                setBubble(bubbleTextAU, "formPostAU", false);
                showOnChangeMessage();
            break;
        }
    });
    $("#firmwareUpdateNowButton").click(function(e) {
        $("#spinner").show();
        $("#formPostDU").submit();
    });
    // firmware update
    $('#firmwareUpdateFile').filestyle({
        buttonBefore: true, 
        icon: false,
        buttonText: '<span class="icon- button-action-icon">R</span><span class="caption-type-1 text-action"><nobr><~ trans `page_mgmt_fw_update_filename` ~></nobr></span>',
    });
    $("#firmwareUpdateButton").click(function(e) {
        ret = validateInputFile("firmwareUpdateFile", "dvl", "errorFirmwareUpdateFile");
        if (ret){
            $("#errorFirmwareUpdateFileFormGroup").removeClass("has-error");
            activeInactive("errorFirmwareUpdateFile");
            setBubble(bubbleTextFU, "formPostFU", false); 
            showOnChangeMessage();
        }else{
            $("#errorFirmwareUpdateFileFormGroup").addClass("has-error");
            inactiveActive("errorFirmwareUpdateFile");
        }	
    });
    // cfg get 
    $("#configGetButton").click(function(e) {
        $("#spinner").show();
        setValue("post_bind_to_snr_cg", "no");
        $("#formPostCG").submit();
        setTimeout("spinnerHide()",5000);
        e.preventDefault();
    });
    // cfg update
    $('#configUpdateFile').filestyle({
        buttonBefore: true,
        icon: false,
        buttonText: '<span class="icon- button-action-icon">R</span><span class="caption-type-1 text-action"><nobr><~ trans `page_mgmt_cfg_update_filename` ~></nobr></span>',
    });
    $("#configUpdateButton").click(function(e) {
        ret = validateInputFile("configUpdateFile", "cfg", "errorConfigUpdateFile");
        if (ret){
            $("#errorConfigUpdateFileFormGroup").removeClass("has-error");
            activeInactive("errorConfigUpdateFile");
            setBubble(bubbleTextCU, "formPostCU", false); 
            showOnChangeMessage();
        }else{
            $("#errorConfigUpdateFileFormGroup").addClass("has-error");
            inactiveActive("errorConfigUpdateFile");
        }   
    });
    // cfg reset
    $("#configResetButton").click(function(e) {
        setBubble(bubbleTextCR, "formPostCR", false);
        showOnChangeMessage();
    });
    // modal footer
    $("#modalFooterClose").click(function(e) {
        aV = $("#actionHolder").val();
        if(aV=="formPostAU"){
            setTimeout(function(){showOnChangeMessage()}, 1);
        }
        $("#actionHolder").val(actionHolderDefault);
    });
    init();
});

function init(){
    setAU();
}

function setBubble(t,f,c){
    switch(c){
        case true:
        $("#modalFooterText").show();
        $("#modalFooterText2").html(t);
        break;
        case false:
        default:
        $("#modalFooterText").hide();
        $("#modalFooterText2").html(t);
        break;
    }
    $("#modalFooter").show();
    $("#actionHolder").val(f);
}

// au 
function setAU(){
    if ( autoFU == "on" ){
        $("#tmp_autoFUOnOff").prop("checked", true);
    }
}
function auSetOff(){
    $("#postAU").val("off");
}
function auSetOn(){
    $("#postAU").val("on");
}

function spinnerHide(){
    $("#spinner").hide();
}

var resetCfg = new function(){
    $("#spinner").show();
    $("#formPostCU").submit();  
}

function modalShowSaveCancel(f,t){
    var f = f;
    $("#modalSCText").html(t);
    $("#modalSaveCancel").modal();
    $("#modalSave").click(function(e){
        f();
    });    
}

function validateInputFile(i,v,e){
    ret = false;
    l = $("#"+i).val().length;
    if ( l > 0 ){  
        s = $("#"+i).val().slice(-3);
        if ( s != v ){
            inactiveActive(e);    
        }else{
            ret = true;     
        }
    }
    return ret;
}
