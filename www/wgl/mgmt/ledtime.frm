<input 
	disabled
	type="hidden"
	id="post_enable"
	name=":sys:NTPClient.EnableNTP"
	value="<~ if ${_errorcount} eq `0` then <~ query `NTPClient.EnableNTP:format=scalar` ~> else ${_sys_NTPClient_EnableNTP} ~>"
/>

<input 
	disabled
	type="hidden"
	id="post_ntpserver"
	name=":sys:NTPClient.NTPServer"
	value="<~ if ${_errorcount} eq `0` then <~ query `NTPClient.NTPServer:format=scalar` ~> else ${_sys_NTPClient_NTPServer} ~>"
/>

<input
	disabled
	type="hidden"
	id="post_gmtoffset"
	name=":sys:NTPClient.GMTOffset"
	value="<~ if ${_errorcount} eq `0` then <~ query `NTPClient.GMTOffset:format=scalar` ~> else ${_sys_NTPClient_GMTOffset} ~>"
/>

<input
	disabled
	type="hidden"
	id="post_autods"
	name=":sys:NTPClient.AutoDaylightSaving"
	value="<~ if ${_errorcount} eq `0` then <~ query `NTPClient.AutoDaylightSaving:format=scalar` ~> else ${_sys_NTPClient_AutoDaylightSaving} ~>"
/>
<input
	disabled
	type="hidden"
	id="post_leds_disable"
	name=":sys:LedsAndButtons.Leds.AlwaysOff"
	value="<~ if ${_errorcount} eq `0` then <~ query `LedsAndButtons.Leds.AlwaysOff:format=raw` ~> else ${_sys_LedsAndButtons_Leds_AlwaysOff} ~>"
/>

<input type="hidden"
name="_okdir"
value="mgmt"
id="post_okdir" />

<input type="hidden"
name="_okpage"
value="home"
id="post_okpage" />
