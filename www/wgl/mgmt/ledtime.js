var _errorcount = "${_errorcount}";
var ntptEnabled = "${ntptEnabled}";

var ntptNoTime1 = "${ntptNoTime1}";
var ntptNoTime2 = "${ntptNoTime2}";

var formValid;
var errMess ={
    hostInvalid: "${ntptNotValid}",
    toShortTS: "<~ trans `error_invalid_value` ~>",
    toLongTS: "<~ trans `error_invalid_value` ~>"
}

$(document).ready(function() {
    // listen for auto-time state change
    $("#time_enable").change(function() {
        // disable setting fields if the auto-time is diabled
        disableTimeSubFields(!$(this).is(':checked'));
    });
    $("#buttonApply").click(function() {
    	formValid = true;
        prepareSubmit();
		$(".has-error").removeClass("has-error");
		$(".error-message-text").removeClass("active").addClass("inactive");
        if($("#time_enable").is(':checked')){
        	checkForm();
        }
        if(formValid){
        	// $("#spinner").hide();
        	$("#form_post").submit();
        }else{
        	$("#spinner").hide();
        }
    });
    setNtpt();
    init();
});

function setNtpt(){
    temp = eval("(<~ query `NTPClient.CurrentTime:format=json` json ~>)"); 
    if(jQuery.type(temp.CurrentTime) == "string"){
        ntptTime = temp.CurrentTime;
    }
    temp = eval("(<~ query `NTPClient.TimeIsValid:format=json` json ~>)");
    if(jQuery.type(temp.TimeIsValid) == "string"){
        ntptTimeValid = temp.TimeIsValid;
    } 
}

function init(){
	initTime();
    initLed();
    setTime();
}

function initLed() {
    $("#view_leds_disable").prop("checked", $("#post_leds_disable").val() == "no");
}

function initTime() {
    $("#time_enable").prop("checked", $("#post_enable").val() == "on");
    $("#view_autods").prop("checked", $("#post_autods").val() == "on");
    $("#view_gmtoffset").val($("#post_gmtoffset").val());
    $("#view_ntpserver").val($("#post_ntpserver").val());
    // trigger change event
    // this solution makes the frontend think something is changed
    // --> the change dialog is displayed
    //$("#time_enable").change(); 
    // duplicated code
    disableTimeSubFields(!$("#time_enable").is(':checked'));
}

function disableTimeSubFields(bool) {
    // disable all child elements
    $("#view_gmtoffset").prop("disabled", bool);
    $("#view_ntpserver").prop("disabled", bool);
    $("#view_autods").prop("disabled", bool);
    // hide child warpper
    $("#time-detail-wrapper").css("display", bool ? "none" : "block");
}

function prepareSubmit() {
    // prepare led settings
    $("#post_leds_disable").prop("disabled", false);
    $("#post_leds_disable").val($("#view_leds_disable").is(":checked") ? "no" : "yes");
    // prepare auto-time state
    $("#post_enable").prop("disabled", false);
    $("#post_enable").val($("#time_enable").is(":checked") ? "on" : "off");
    if ($("#time_enable").is(':checked')) {
        // timezone
        $("#post_gmtoffset").prop("disabled", false);
        $("#post_gmtoffset").val($("#view_gmtoffset").val());
        // timeserver
        $("#post_ntpserver").prop("disabled", false);
        $("#post_ntpserver").val($("#view_ntpserver").val());
        // prepare time settings
        $("#post_autods").prop("disabled", false);
        $("#post_autods").val($("#view_autods").is(":checked") ? "on" : "off");
    }
}

function setTime() {
    if (ntptEnabled == "on") {
        $("#actDateTime").removeClass("inactive");
        getTime();
    }
}
function getTime() {
    $.get("/cgi-bin/htmlmgr?_file=getjson&service=time").done(function(data) {
        // console.log(data);
        tI = data.CurrentTime;
        tIf = "";
        // if (tI.match(/^([^ ]+) (.....)/)) {
        //    tIF = RegExp.$1 + " " + RegExp.$2;
        // }
        if(tI != ""){
        	$("#time").html(tI);
        	$("#messageTime").removeClass("inactive").addClass("active");
        	$("#errTime").removeClass("active").addClass("inactive");
        }else{
        	$("#messageTime").removeClass("active").addClass("inactive");
        	$("#errTime").removeClass("inactive").addClass("active");
        }
        setTimeout("getTime()", 10000);
    }).fail(function(data) {
        
    });
}

// check form 
function checkForm(){
	formValid = true;
	var obj = $("#form_post");
	obj.find("input[data-ifValidation='true']:enabled").each(function () {
            if($(this).is(":visible")){
                eM = $(this).data("errormessageid");
                
                $(this).closest(".form-group").removeClass("has-error");
                $("#"+eM).removeClass("active");

                var validation = $(this).data("validation") ? $(this).data("validation") : "charValid";
                var validationEK = $(this).data("validationerrorkey") ? $(this).data("validationerrorkey") : "charInvalid";
                var minLength = $(this).data("minlength") ? $(this).data("minlength") : -1;
                var minLengthEK = $(this).data("minlengtherrorkey") ? $(this).data("minlengtherrorkey") : -1;
                var maxLength = $(this).data("maxlength") ? $(this).data("maxlength") : -1;
                var maxLengthEK = $(this).data("maxlengtherrorkey") ? $(this).data("maxlengtherrorkey") : -1;
                var errorMId = eM ? eM : -1;
                var userInput = $(this).val();

			    if(minLength > userInput.length) { 
			 	    showError(this, errorMId, errMess[minLengthEK]); 
			 	    formValid = false;
			 	    return true;
			 	} else if(maxLength < userInput.length) { 
			 	    showError(this, errorMId, errMess[maxLengthEK]); 
			 	    formValid = false;
			 	    return true;
			    }else if(errReg[validation] && !errReg[validation].test(userInput)) {
			 	    showError(this, errorMId, errMess[validationEK]); 
			 	    formValid = false;
			 	    return true;
			    } else { // no validation errors
			 	    return true; 
			    }
            }
	});
}
function showError(a,b,c){
	$(a).closest(".form-group").addClass("has-error");
	$("#"+b).addClass("active").find(".text-error").html(c);
}