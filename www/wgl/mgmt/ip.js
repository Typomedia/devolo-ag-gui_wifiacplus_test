/* type */
var type = "${type}";
var type_post = "${type_post}";
var err_type = "${err_type}";
if (err_type != '' || type_post != '') {
    type = type_post;
}
/* ipadresse */
var ipaddress = "${ipaddress}";
var ipaddress_post = "${ipaddress_post}";
/* prefixlength */
var prefixlength = "${prefixlength}";
var prefixlength_post = "${prefixlength_post}";
var netmask = "";
/* gateway */
var gateway = "${gateway}";
var gateway_post = "${gateway_post}";
/* nameserver */
var nameserver = "${nameserver}";
var nameserver_post = "${nameserver_post}";
/* inl error */
var error_text_ip = "${error_text_ip}";
var error_text_mask = "${error_text_mask}";
var error_text_gateway = "${error_text_gateway}";
var error_text_nameserver = "${error_text_nameserver}";
var ty;
var iP;
var nM;
var pL;
var gW;
var nS;
var refresh_allowed = false;
var refresh_time = 5000;
var intervalSet;
$(document).ready(function() {
    $("#buttonApply").click(function(e) {
        // ty = getValue("tmp_dhcpOnOff");
        iP = getValue("post_ipaddress");
        nM = getValue("tmp_post_prefixlength");
        pL = netMaskToPrefLength(nM);
        gW = getValue("post_gateway");
        nS = getValue("post_nameserver");
        var iV = $('#tmp_dhcpOnOff').prop('checked');
        $(".has-error").removeClass("has-error");
        $(".error-message-box").removeClass("active");
        switch (iV) {
            case false:
                typeSetOff();
                if (formCheckStatic()) {
                    // submit
                    // $("#post_type").val(ty);
                    $("#post_ipaddress").prop("disabled", false);
                    $("#post_prefixlength").val(pL).prop("disabled", false);
                    $("#post_gateway").prop("disabled", false);
                    // temp
                    // if (gW == "") {
                    //    $("#post_gateway").prop("disabled", true);
                    // }
                    //if (nS==""){
                    $("#post_nameserver").prop("disabled", false);
                    //}
                    setValue("post_reloadip", iP);
                    // $("#spinner").hide();
                    $("#form_post").submit();
                } else {
                    $("#spinner").hide();
                }
                break;
            case true:
                typeSetOn();
                // $("#post_type").val(ty);
                $("#post_ipaddress").prop("disabled", true);
                $("#post_prefixlength").prop("disabled", true);
                $("#post_gateway").prop("disabled", true);
                $("#post_nameserver").prop("disabled", true);
                setValue("post_reloadip", "");
                $("#form_post").submit();
                break;
        }
        e.preventDefault();
    });
    $("#tmp_dhcpOnOff").click(function(e) {
        var iV = $('#tmp_dhcpOnOff').prop('checked');
        switch (iV) {
            case false:
                typeSetOff();
                break;
            case true:
                typeSetOn();
                break;
        }
    });
    init();
});

function init() {
    setType();
    netmask = prefLngToNetmask(prefixlength);
    setNetmask();
}

function setNetmask() {
    setValue("tmp_post_prefixlength", netmask);
}

function formCheckStatic() {
    retS = true;
    /* ipadresse */
    retIp = checkIp(iP);
    if (!retIp) {
        hasError("fGIpAdrDiv");
        inactiveActive("errorIpAdrDiv");
        setHtml("errorIpAdrP", error_text_ip);
    } else {
        hasNoError("fGIpAdrDiv");
        activeInactive("errorIpAdrDiv");
        setHtml("errorIpAdrP", "");
    }
    retS = retS && retIp;
    /* netmask */
    retPl = checkIp(nM);
    if (!retPl) {
        hasError("fGPrLenDiv");
        inactiveActive("errorPrLenDiv");
        setHtml("errorPrLenP", error_text_mask);
    } else {
        hasNoError("fGPrLenDiv");
        activeInactive("errorPrLenDiv");
        setHtml("errorPrLenP", "");
    }
    retS = retS && retPl;
    /* gateway */
    retGw = true;
    if (gW != "") {
        retGw = checkIp(gW);
        if (!retGw) {
            hasError("fGGaWaDiv");
            inactiveActive("errorGaWaDiv");
            setHtml("errorGaWaP", error_text_gateway);
        } else {
            hasNoError("GGaWaDiv");
            activeInactive("errorGaWaDiv");
            setHtml("errorGaWaP", "");
        }
    }
    retS = retS && retGw;
    /* nameserver */
    retNs = true;
    if (nS != "") {
        retNs = checkIp(nS);
        if (!retNs) {
            hasError("fGNaSeDiv");
            inactiveActive("errorNaSeDiv");
            setHtml("errorNaSeP", error_text_nameserver);
        } else {
            hasNoError("fGNaSeDiv");
            activeInactive("errorNaSeDiv");
            setHtml("errorNaSeP", "");
        }
    }
    retS = retS && retNs;
    return retS;
}
/* dhcp */
function setType() {
    // setValue("tmp_dhcpOnOff", type);
    if (type == "DHCP") {
        $("#tmp_dhcpOnOff").prop("checked", true);
        // toggleType("hide");
        typeSetOn();
    } else {
        $("#tmp_dhcpOnOff").prop("checked", false);
        // toggleType("show");
        typeSetOff();
    }
}

function toggleType(v) {
    switch (v) {
        case "show":
            // inactiveActive('divStatic');
            $("#divStatic :input").prop("disabled", false).removeClass("semiTransparent");
            $("#divStatic").removeClass("semiTransparent");
            activeInactive('messageDHCPDiv');
            break;
        case "hide":
            // activeInactive('divStatic');
            $("#divStatic :input").prop("disabled", true);
            $("#divStatic").addClass("semiTransparent");
            inactiveActive('messageDHCPDiv');
            break;
    }
}

function typeSetOff() {
    // $("#tmp_dhcpOnOff").val("static");
    refresh_allowed = false;
    if (intervalSet) {
        clearInterval(intervalSet);
    }
    $("#post_type").val("static");
    toggleType("show");
}

function typeSetOn() {
    // $("#tmp_dhcpOnOff").val("DHCP");
    refresh_allowed = true;
    reloadSettings();
    intervalSet = setInterval(reloadSettings, refresh_time);
    $("#post_type").val("DHCP");
    toggleType("hide");
}

function netMaskToPrefLength(v) {
    var prefixLength = 0;
    var netmaskBytes = v.split(".");
    var byte;
    if (netmaskBytes.length != 4) {
        prefixLength = ""; //insert invalid prefix length to provoke an error in sysmgr
        return false;
    } else {
        for (var i = 0; i < 4; i++) {
            byte = parseInt(netmaskBytes[i]);
            if (byte == 0) {
                continue;
            } else if (prefixLength < (8 * i)) {
                prefixLength = ""; //insert invalid prefix length to provoke an error in sysmgr
                break;
            }
            for (var j = 7; j >= 0; j--) {
                if (byte < 256 && byte >= Math.pow(2, j)) {
                    prefixLength++;
                    byte -= Math.pow(2, j);
                } else if (byte == 0) {
                    break;
                } else {
                    prefixLength = ""; //insert invalid prefix length to provoke an error in sysmgr
                    break;
                }
            }
        }
    }
    return prefixLength;
}

function IPToNum(ipstr) {
    var retVal = ipstr.split('.');
    return ((((((+retVal[0]) * 256) + (+retVal[1])) * 256) + (+retVal[2])) * 256) + (+retVal[3]);
}

function checkIp(v) {
    retIP = true;
    retIP = errReg['ipValid'].test(v);
    /*
    ip = v.split(".");  
    if (ip.length == 4){  
        for(i = 0 ; i < ip.length; i++){  
            if (!(parseInt(ip[i]) >= 0 && parseInt(ip[i]) <= 255)){
                ret = false;
                break;
            }  
        }  
    }else{
        ret = false;
    }
    */
    return retIP;
}

function checkGateway(iP, gW, pL, nM) {
    ret = true;
    retI = checkIp(gW);
    //is gateway a valid ip
    if (!retI) {
        ret = false;
    } else {
        iP = IPToNum(iP);
        gW = IPToNum(gW);
        pL = parseInt(pL);
        nM = IPToNum(nM);
        if ((gW & (~nM)) == 0) {
            alert(1);
            ret = false;
        } else if ((gW & (~nM)) - (Math.pow(2, (32 - pL)) - 1) == 0) {
            alert(2);
            ret = false;
        } else if ((iP & nM) != (gW & nM)) {
            alert(3);
            ret = false;
        }
    }
    return ret;
}

function checkReservedIp() {
    //look at DVT Testcase 231
    check_reserved_ip_error = 0;
    checkIp($dvl("stat_ipaddress").value);
    if (check_ip_error != 0) {
        check_reserved_ip_error = 1;
        return;
    } else {
        checkIp($dvl("stat_netmask").value);
        if (check_ip_error != 0) {
            check_reserved_ip_error = 1;
            return;
        }
    }
    ipaddress = IPToNum($dvl("stat_ipaddress").value);
    pefixlength = parseInt($dvl("stat_prefixlength").value);
    netmask = IPToNum($dvl("stat_netmask").value);
    if ((ipaddress & (~netmask)) == 0) check_reserved_ip_error = 1;
    else if ((ipaddress & (~netmask)) - (Math.pow(2, (32 - pefixlength)) - 1) == 0) check_reserved_ip_error = 1;
    return check_reserved_ip_error;
}

function reloadSettings() {
    $.get("/cgi-bin/htmlmgr?_file=getjson&service=networksettings").done(function(data) {
        refreshSettings(data);
    }).fail(function() {
        // reloadDevicesList(false);
    }).always(function() {})
}

function refreshSettings(d) {
    if (!refresh_allowed) return;
    settings = null;
    if (isObject(d)) {
        $("#post_ipaddress").val(d.Management.V4.IpAddress);
        $("#tmp_post_prefixlength").val(prefLngToNetmask(d.Management.V4.PrefixLength));
        if (d.Management.V4.Nameserver != "0.0.0.0") {
            $("#post_nameserver").val(d.Management.V4.Nameserver);
        } else {
            $("#post_nameserver").val("");
        }
        if (d.Management.V4.DefaultGateway.IpDynamic != "0.0.0.0") {
            $("#post_gateway").val(d.Management.V4.DefaultGateway.IpDynamic);
        } else {
            $("#post_gateway").val("");
        }
    }
}