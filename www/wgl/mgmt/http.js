
ifModalFooter = "no";

var _errorcount = "${_errorcount}";

/* errors */
var err_oldPassword = "${old_password_err}";
var err_newPassword1 = "${new_password1_err}";
var err_newPassword2 = "${new_password2_err}";
var formValid;
var errMess ={
    charInvalid: "<~ trans `error_password_invalid` ~>",
    toLong: "<~ trans `error_password_too_long` ~>",
    notEqual: "<~ trans `error_password_confirm` ~>"
}

$(document).ready(function() {

	function preparePasswordSubmit() {
		$("#post_password_old").val($("#password").val());
		$("#post_password_1").val($("#password1").val());
		$("#post_password_2").val($("#password2").val());
		return true;
	}
	
	// set action-listener on actionHolder to submit form
	$("#buttonApply").click(function () {
		checkForm();
		checkeEqPW();
		preparePasswordSubmit();
		if(formValid){
			// $("#spinner").hide();
			$("#form_post").submit();
		}else{
			$("#spinner").hide();
		}
	}); 
	init();
});

function init(){
	setErrors();
}


/* errors */
function setErrors(){
	if(err_oldPassword != ""){
		$("#err_oldPasswordFormGroup").addClass("has-error");
		$("#err_oldPasswordMessage").addClass("active");
	}
	if(err_newPassword1 != ""){
		$("#err_newPassword1FormGroup").addClass("has-error");
		$("#err_newPassword1Message").addClass("active");
	}
	if(err_newPassword2 != ""){
		$("#err_newPassword2FormGroup").addClass("has-error");
		$("#err_newPassword2Message").addClass("active");
	}
}

// check form 
function checkForm(){
	console.log("jo");
	formValid = true;
	var obj = $("#form_post");
	obj.find("input[data-ifvalidation='true']:enabled").each(function () {
            if($(this).is(":visible")){

            	$iD = $(this).attr("id");

                eM = $(this).data("errormessageid");
                
                $(this).closest(".form-group").removeClass("has-error");
                $("#"+eM).removeClass("active");

                var validation = $(this).data("validation") ? $(this).data("validation") : "charValid";
                var validationEK = $(this).data("validationerrorkey") ? $(this).data("validationerrorkey") : "charInvalid";
                var minLength = $(this).data("minlength") ? $(this).data("minlength") : -1;
                var minLengthEK = $(this).data("minlengtherrorkey") ? $(this).data("minlengtherrorkey") : -1;
                var maxLength = $(this).data("maxlength") ? $(this).data("maxlength") : -1;
                var maxLengthEK = $(this).data("maxlengtherrorkey") ? $(this).data("maxlengtherrorkey") : -1;
                var errorMId = eM ? eM : -1;
                var userInput = $(this).val();

			    if(minLength > userInput.length) { 
			 	    showError(this, errorMId, errMess[minLengthEK]); 
			 	    formValid = false;
			 	    return true;
			 	} else if(maxLength < userInput.length) { 
			 	    showError(this, errorMId, errMess[maxLengthEK]); 
			 	    formValid = false;
			 	    return true;
			    } else if(errReg[validation] && !errReg[validation].test(userInput)) {
			 	    showError(this, errorMId, errMess[validationEK]); 
			 	    formValid = false;
			 	    return true;
			    } else { // no validation errors
			 	    return true; 
			    }
            }
	});
}
function checkeEqPW(){
	$pw1 = $("#password1");
	$pw2 = $("#password2");
	if($pw1.val()!=$pw2.val()){
		formValid = false;
		showError($pw1, "err_newPassword1Message", errMess['notEqual']);
		showError($pw2, "err_newPassword2Message", errMess['notEqual']);  
	}
}
function showError(a,b,c){
	$(a).closest(".form-group").addClass("has-error");
	$("#"+b).addClass("active").find(".text-error").html(c);
}

