
var reloadpage = "/cgi-bin/htmlmgr?_file=fs&_lang=${_lang}";
var followdir = "<~ + ${_followdir} ~>";
var followpage = "<~ + ${_followpage} ~>";
var reloadip = "${_reloadip}"
var reloaddelay = 10000;
var resultmsg   = "";
var device_reachable = false;
var do_start_reboot_watchdog = false;
var watchdogtype = "${_type}";
var xhr;
var url;
var isError = false;

<~ if ${_type} eq `` then `
    resultmsg = "<~ trans `page_result_please_wait` ~>&nbsp;<~ trans `page_result_refresh` ~>&nbsp;<~ trans `page_result_not_reachable_2014` ~>";
    reloaddelay = 10000;
` ~>

<~ if ${_type} eq `createbubble` then `
    resultmsg = "<~ trans `page_result_please_wait` ~>&nbsp;<~ trans `page_result_refresh` ~>&nbsp;<~ trans `page_result_not_reachable_2014` ~>";
    <~ if <~ query `HomePlug.SecureNMK` ~> eq `on` then `
        reloaddelay = 10000;
    ` else `
        reloaddelay = 25000;
    ` ~>
` ~>
 
<~ if ${_type} eq `dlanstatus` then `
    resultmsg = "<~ trans `page_result_please_wait` ~>&nbsp;<~ trans `page_result_refresh` ~>";
    reloaddelay = 20000;
` ~>

<~ if ${_type} eq `wlanstatus` then `
    resultmsg = "<~ trans `page_result_op_started` ~>";
    reloaddelay = 2000;
` ~>

<~ if ${_type} eq `clonemode` then `
    resultmsg = "<~ trans `page_result_clonemode_2014` ~>&nbsp;<~ trans `page_result_refresh` ~>";
    reloaddelay = 150000; //30 seconds more than max WPS window, since on early completion, the remaining timeout is discarded anyway.
` ~>

<~ if ${_type} eq `reboot` then `
    resultmsg = "<~ trans `page_result_reboot` ~>&nbsp;<~ trans `page_result_refresh` ~>";
    reloaddelay = 60000;
    do_start_reboot_watchdog = true;
` ~>
        
<~ if ${_type} eq `ipaddress` then `
    resultmsg = "<~ trans `page_result_please_wait` ~>&nbsp;<~ trans `page_result_refresh` ~>&nbsp;<~ trans `page_result_not_reachable_2014` ~>";
    reloaddelay = 15000;
` ~>
        
<~ if ${_type} eq `defaults` then `
    resultmsg = "<~ trans `page_result_defaults` ~>&nbsp;<~ trans `page_result_refresh` ~>&nbsp;<~ trans `page_result_not_reachable_2014` ~>";
    reloaddelay = 60000;
    do_start_reboot_watchdog = true;
` ~>
        
<~ if ${_type} eq `update` then `
    isError = true;
    resultmsg = "<~ trans `page_result_error` ~>:&nbsp;${_error_consistency}";
    reloaddelay = 10000;
    setHtml("headerTitle", "<~ trans `nav_mgmt_fw_update` ~>");
    
    <~ if ${_error_consistency} eq `` then `
        isError = false;
        resultmsg = "<~ trans `page_result_update` ~> <~ trans `page_result_dont_interrupt` ~>";
        reloaddelay = 180000;
        do_start_reboot_watchdog = true;
    ` ~>
    <~ if ${_error_consistency} eq `fw_update_check_invalid_file` then `
        isError = true;
        resultmsg = "<~ trans `page_result_file` ~>";
        reloaddelay = 10000;
    ` ~>
    <~ if ${_error_consistency} eq `fw_update_check_same_version` then `
        isError = true;
        resultmsg = "<~ trans `page_result_firmware_same_version_2014` ~>";
        reloaddelay = 10000;
    ` ~>
    <~ if ${_error_consistency} eq `fw_update_check_incorrect_device` then `
        isError = true;
        resultmsg = "<~ trans `page_result_firmware_incorrect_device_2014` ~>";
        reloaddelay = 10000;
    ` ~>
    <~ if ${_error_consistency} eq `fw_update_check_no_firmware` then `
        isError = true;
        resultmsg = "<~ trans `page_result_firmware_no_firmware` ~>";
        reloaddelay = 10000;
    ` ~>
    <~ if ${_error_consistency} eq `fw_update_check_no_config` then `
        isError = true;
        resultmsg = "<~ trans `page_result_firmware_no_config` ~>";
        reloaddelay = 10000;
    ` ~>
    <~ if ${_error_consistency} eq `fw_update_in_progress` then `
        isError = false;
        resultmsg = "<~ trans `page_result_please_wait` ~><p></p><~ trans `page_result_reboot` ~>&nbsp;";
        reloaddelay = 180000;
        do_start_reboot_watchdog = true;
    ` ~>
    <~ if ${_error_consistency} eq `cfg_restore_check_failed` then `
        isError = true;
        resultmsg = "<~ trans `page_result_file` ~>";
        reloaddelay = 10000;
    ` ~>
    resultmsg = resultmsg + "<~ trans `page_result_refresh` ~>&nbsp;<~ trans `page_result_not_reachable_2014` ~>";
` ~>

$( document ).ready(function() {
    init();
});

function init(){
    setMessage();
    setProgressBar();
    setUrl();
    switch ( do_start_reboot_watchdog ){
        case true:
            startRebootWatchdog();
        break;
        default:
            sendXHR();
        break;
    }
}

function setMessage(){
    switch ( isError ){
        case true:
            $("messageBox").addClass("text-red");
        break;
        default:
            $("messageBox").removeClass("text-red");
        break;
    }
    setHtml("messageBoxText", resultmsg);
}

function setProgressBar(){
    rdPb = parseInt(reloaddelay/1000);
    var style = ('<style>.progress .progress-bar.x-sec-ease-in-out { -webkit-transition: width '+rdPb+'s ease-in-out; -moz-transition: width '+rdPb+'s ease-in-out; -ms-transition: width '+rdPb+'s ease-in-out; -o-transition: width '+rdPb+'s ease-in-out; transition: width '+rdPb+'s ease-in-out; } </style>');   
    $('html > head').append(style);
    $('.progress-bar').progressbar({
        display_text: 'center',
        done: function() {
            doReload();
        }
    });
}

function setUrl(){
    switch(watchdogtype){
        case "clonemode":
            url = "/cgi-bin/htmlmgr?_file=getjson&service=clonemodestate";
        break;
        case "update":
        default:
            url = "/cgi-bin/htmlmgr?_file=getjson&service=hpconnected";
        break;
    }
}

function sendXHR(){
    if (url.length > 0){
        getJSON();
    }
}

function abortXHR(){
    xhr.abort();
}

function getJSON(){
    var timeout = 0;
    xhr = $.ajax({
        dataType: "json",
        url: url,
        success: function(json){
            if ( getObjType(json) == "Object" ){
                switch(watchdogtype){
                    case "clonemode":
                        if (json.State == 'idle'){
                            followpage = "home"; //forward to wireless->home on success
                            doReload();
                            return;
                        }else{
                            timeout = 5000;
                        }
                    break;
                    case "update":
                    default:
                    if (json.IsConnected == '1' || json.IsConnected == '0'){
                        device_reachable = true;
                    }
                    break;
                }    
            }
            if (timeout > 0){
                setTimeout("getJSON()", timeout);
            }
        }
    });
}

function doReload(){
    switch(reloadip){
        case "":
            if (followdir.length > 0 && followpage.length > 0){
                document.location.href = path_base+"&_dir="+followdir+"&_page="+followpage;
            }else{
                top.location.href = reloadpage;
            }
        break;
        default:
            if (followdir.length > 0 && followpage.length > 0){
                document.location.href = "http://"+reloadip+path_base+"&_dir="+followdir+"&_page="+followpage;
            }else{
                top.location.href = "http://"+reloadip+reloadpage;
            }
        break;    
    }
}

function startRebootWatchdog(){
    //initially, wait 20 seconds before checking the first time
    setTimeout("checkForReboot()", 20000);
}

function checkForReboot(){
    //are there race conditions possible in ajax? anyway, who cares in this case?
    if (device_reachable){
        doReload();
    }else{
        //to avoid waiting for too long, we abort the old request and start a new one
        abortXHR();
        sendXHR();
        //check for xhr side-effect after 2 seconds
        setTimeout("checkForReboot()", 2000);
    }
}

