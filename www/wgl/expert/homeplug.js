var _errorcount = "${_errorcount}";

/* errors */
var err_pW = "${err_pW}";
var formValid;
var errMess ={
    charInvalid: "<~ trans `misc_chars_allowed_text` ~>: <~ trans `misc_chars_allowed_symbols` ~>",
    toShortPW: "<~ trans `error_expert_homeplug_password_min_length` ~>",
    toLongPW: "<~ trans `error_expert_homeplug_password_max_length` ~>",
}

$(document).ready(function() {
    $("#buttonApply").click(function() {
    	formValid = true;
        preparePasswordSubmit();
        checkForm();
        if(formValid){
        	$("#form_post").submit();
        }else{
        	$("#spinner").hide();
        }
    });
    init();
});

function init(){
	setErrors();
}

/* errors */
function setErrors(){
	if(err_pW != ""){
		$("#errPWFormGroup").addClass("has-error");
		$("#errPWMessage").addClass("active");
	}
}
function preparePasswordSubmit() {
    // check which checkbox is selected 
    $("#post_password_avln").prop('disabled', false);
    $("#post_password_avln").val($("#password1").val());
    return true;
}
// check form 
function checkForm(){
	formValid = true;
	var obj = $("#form_post");
	obj.find("input[data-ifValidation='true']:enabled").each(function () {
            if($(this).is(":visible")){
                eM = $(this).data("errormessageid");
                
                $(this).closest(".form-group").removeClass("has-error");
                $("#"+eM).removeClass("active");

                var validation = $(this).data("validation") ? $(this).data("validation") : "charValid";
                var validationEK = $(this).data("validationerrorkey") ? $(this).data("validationerrorkey") : "charInvalid";
                var minLength = $(this).data("minlength") ? $(this).data("minlength") : -1;
                var minLengthEK = $(this).data("minlengtherrorkey") ? $(this).data("minlengtherrorkey") : -1;
                var maxLength = $(this).data("maxlength") ? $(this).data("maxlength") : -1;
                var maxLengthEK = $(this).data("maxlengtherrorkey") ? $(this).data("maxlengtherrorkey") : -1;
                var errorMId = eM ? eM : -1;
                var userInput = $(this).val();

			    if(minLength > userInput.length) { 
			 	    showError(this, errorMId, errMess[minLengthEK]); 
			 	    formValid = false;
			 	    return true;
			 	} else if(maxLength < userInput.length) { 
			 	    showError(this, errorMId, errMess[maxLengthEK]); 
			 	    formValid = false;
			 	    return true;
			    }else if(errReg[validation] && !errReg[validation].test(userInput)) {
			 	    showError(this, errorMId, errMess[validationEK]); 
			 	    formValid = false;
			 	    return true;
			    } else { // no validation errors
			 	    return true; 
			    }
            }
	});
}
function showError(a,b,c){
	$(a).closest(".form-group").addClass("has-error");
	$("#"+b).addClass("active").find(".text-error").html(c);
}