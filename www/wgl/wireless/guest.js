var _errorcount = "${_errorcount}";
var wlanActive = "${wlanActive}";
/* radiomode */
var wlanGuActive = "${wlanGuActive}";
var wlanGuActive_post = "${wlanGuActive_post}";
var err_wlanGuActive = "${err_wlanGuActive";
// if ( err_wlanGuActive != '' || wlanGuActive_post != '' ){
if (_errorcount > 0) {
    if ( wlanGuActive_post != ""){
        wlanGuActive = wlanGuActive_post;
    }
}
var wlanGuRestPos = "${wlanGuRestPos}";
var wlanGuRestAct = "${wlanGuRestAct}";
var wlanGuRestAct_post = "${wlanGuRestAct_post}";
if (_errorcount > 0) {
    if ( wlanGuRestAct_post != "" ){
        wlanGuRestAct = wlanGuRestAct_post;
    }
}
var wlanGuTimeOut = "${wlanGuTimeOut}";
var wlanGuTimeOut_post = "${wlanGuTimeOut_post}";
if (_errorcount > 0) {
    if ( wlanGuTimeOut_post != "" ){
        wlanGuTimeOut = wlanGuTimeOut_post;
    }
}
var wlanGuTimeOutDef = 1800;
var bubbleTextAbort = "${bubbleTextAbort}";
var intervalTimeOut;
/* wlanradio */
var wlanRadio0 = "${wlanRadio0}";
var wlanRadio0_post = "${wlanRadio0_post}";
if (_errorcount > 0) {
    if ( wlanRadio0_post != "" ){
        wlanRadio0 = wlanRadio0_post;
    }
}
ifBubbleEnabled = "${ifBubbleEnabled}";
showBubbleDialog = "${showBubbleDialog}";
bubbleText = "${bubbleText}";
/* errors */
var err_ssid0 = "${err_ssid0}";
var err_wpaKey0 = "${err_wpaKey0}";
var formValid;
var errMess = {
    charInvalid: "<~ trans `misc_chars_allowed_text` ~>: <~ trans `misc_chars_allowed_symbols` ~>",
    toShortSSID: "<~ trans `error_wireless_ssid_min_length` ~>",
    toLongSSID: "<~ trans `error_wireless_ssid_max_length` ~>",
    toShortWpaKey: "<~ trans `error_wireless_key_min_length` ~>",
    toLongWpaKey: "<~ trans `error_wireless_key_max_length` ~>"
}
/* security */
var security0 = "${security0}";
var security0_post = "${security0_post}";
if (!security0 && !security0_post) {
    security0 = "WPA";
}
if (_errorcount > 0) {
    security0 = security0_post;
}
var secCheck = true;
var secCheckMes = "";
var secCheckMes0 = "<~ trans `wireless_guest_error` ~>";
var arrIdMir = ['wlanRadio', 'ssid', 'security', 'wpaKey'];
$(document).ready(function() {
    $("#buttonApply").click(function(e) {
        var iV = $('#tmp_wifiOnOff').prop('checked');
        checkForm();
        if (formValid) {
            syncForm();
            // disable unneeded inputs
            switch (iV) {
                case false:
                    disableInputs("guest");
                    break;
                case true:
                    break;
            }
            // console.log($("#form_post").serialize());
            // $("#spinner").hide();
            $("#form_post").submit();
        } else {
            $("#spinner").hide();
        }
    });
    $("#tmp_wifiOnOff").click(function(e) {
        var iV = $('#tmp_wifiOnOff').prop('checked');
        switch (iV) {
            case false:
                wifiSetOff();
                break;
            case true:
                wifiSetOn();
                break;
        }
    });
    $("#tmp_guRestOnOff").click(function(e) {
        var iV = $('#tmp_guRestOnOff').prop('checked');
        switch (iV) {
            case false:
                guRestSetOff();
                break;
            case true:
                guRestSetOn();
                break;
        }
    });
    $("#tmp_guTimeOutOnOff").click(function(e) {
        var iV = $('#tmp_guTimeOutOnOff').prop('checked');
        switch (iV) {
            case false:
                GuTimeOutSetOff();
                break;
            case true:
                GuTimeOutSetOn();
                break;
        }
    });
    $("#tmp_guTimeOut").change(function(e) {
        var iV = $('#tmp_guTimeOut').val();
        setGuTimeOut(iV);
    });
    $("#buttonGuTimeOutAbort").click(function(e) {
        $("#wlanGuTimeOut").prop("disabled", false);
        setBubble(bubbleTextAbort, "", false);
        showOnChangeMessage();
    });
    $("#modalFooter, #modalFooterClose").click(function(e) {
        $("#wlanGuTimeOut").prop("disabled", true);
    });
    $(".securityDiv").find(":input").click(function(e) {
        $id = this.id;
        $a = $id.split("_");
        $s = $a[1];
        $n = $a[0].replace("security", "");
        switch ($s) {
            case "none":
                disablePassword($n);
                break;
            case "WPA":
                enablePassword($n);
                break;
        }
        setSecurity($s);
    });
    init();
});

function init() {
    setWifiMode();
    setGuTimeOutMode();
    setGuestRestrictionMode();
    setEncryption();
    setErrors();
}
/* errors */
function setErrors() {
    if (err_wpaKey0 != "") {
        $("#err_wpaKey0FormGroup").addClass("has-error");
        $("#err_wpaKey0Message").addClass("active");
    }
    if (err_ssid0 != "") {
        $("#err_ssid0FormGroup").addClass("has-error");
        $("#err_ssid0Message").addClass("active");
    }
}

function syncForm() {
    $.each(arrIdMir, function(key, value) {
        var v = $("#" + value + "0").val();
        $("#" + value + "1").val(v);
    });
}
// disabling ununsed inputs
function disableInputs(a) {
    $(".guest").each(function() {
        $(this).attr("disabled", true);
    });
}
/* wifi mode */
function setWifiMode() {
    if (wlanRadio0 == "on") {
        $("#tmp_wifiOnOff").prop("checked", true);
        toggleWifiMode("show");
    } else {
        toggleWifiMode("hide");
    }
}

function toggleWifiMode(v) {
    switch (v) {
        case "show":
            if (wlanGuRestPos == "yes") {
                toggleGuestRestrictionMode("show");
            }
            $("#divWlanRadios").removeClass("inactive").addClass("active");
            break;
        case "hide":
            toggleGuestRestrictionMode("hide");
            $("#divWlanRadios").removeClass("active").addClass("inactive");
            break;
    }
}

function wifiSetOff() {
    $("#wlanRadio0,#wlanRadio1").val("off");
    toggleWifiMode("hide");
}

function wifiSetOn() {
    $("#wlanRadio0,#wlanRadio1").val("on");
    toggleWifiMode("show");
}
/* guest restriction  */
function setGuestRestrictionMode() {
    switch (wlanGuRestPos) {
        case "yes":
            if (wlanGuActive == "on") {
                toggleGuestRestrictionMode("show");
                switch (wlanGuRestAct) {
                    case "yes":
                        guRestSetOn();
                        $("#tmp_guRestOnOff").prop("checked", true);
                        break;
                    case "no":
                        $("#tmp_guRestOnOff").prop("checked", false);
                        guRestSetOff();
                        break;
                }
            }
            break;
        case "no":
        default:
            toggleGuestRestrictionMode("hide");
            $("#wlanGuRestAct").prop("disabled", "disabled");
            break;
    }
}

function toggleGuestRestrictionMode(v) {
    switch (v) {
        case "show":
            $("#guRestPos").removeClass("inactive").addClass("active");
            break;
        case "hide":
            $("#guRestPos").removeClass("active").addClass("inactive");
            break;
    }
}

function guRestSetOff() {
    $("#wlanGuRestAct").val("no");
}

function guRestSetOn() {
    $("#wlanGuRestAct").val("yes");
}
/* guest time out mode */
function setGuTimeOutMode() {
    if (wlanActive == "on") {
        // toggleGuTimeOutMode("show");
        if (wlanGuTimeOut != "0") {
            toggleGuTimeOutMode("divGuTimeOut", "hide");
            toggleGuTimeOutMode("divGuTimeOutRun", "show");
            setTimeOutRun();
            intervalTimeOut = setInterval(setTimeOutRun, 1000);
        } else {
            toggleGuTimeOutMode("divGuTimeOut", "show");
            toggleGuTimeOutMode("divGuTimeOutRun", "hide");
            enableGuTimeOut("no");
        }
    } else {
        enableGuTimeOut("no");
    }
}

function toggleGuTimeOutMode(o, v) {
    switch (v) {
        case "show":
            $("#" + o).removeClass("inactive").addClass("active");
            break;
        case "hide":
            $("#" + o).removeClass("active").addClass("inactive");
            break;
    }
}

function GuTimeOutSetOff(v) {
    $("#wlanGuTimeOut").val("0");
    enableGuTimeOut("no");
}

function GuTimeOutSetOn() {
    var iV = $("#tmp_guTimeOut").val();
    setGuTimeOut(iV);
    enableGuTimeOut("yes");
}

function selectGuTimeOut(v) {
    $("#tmp_guTimeOut option[id='gto_" + v + "']").prop("selected", true);
}

function setGuTimeOut(v) {
    $("#wlanGuTimeOut").val(v);
}

function enableGuTimeOut(v) {
    switch (v) {
        case "yes":
            $("#divGuTimeOutTime").removeClass("semiTransparent");
            $("#tmp_guTimeOut").prop("disabled", false);
            $("#wlanGuTimeOut").prop("disabled", false);
            break;
        case "no":
            $("#divGuTimeOutTime").addClass("semiTransparent").prop("disabled", true);
            $("#tmp_guTimeOut").prop("disabled", true);
            $("#wlanGuTimeOut").prop("disabled", true);
            break;
    }
}

function setTimeOutRun() {
    if(_errorcount == "0"){
        if (wlanGuTimeOut > 0) {
            var hours = Math.floor(wlanGuTimeOut / 3600);
            var minutes = Math.floor((wlanGuTimeOut / 60) - (hours * 60)); //total minutes minus already counted hours
            var seconds = wlanGuTimeOut - (Math.floor(wlanGuTimeOut / 60) * 60); //total seconds minus already counted minutes
            var text = "";
            if (hours > 9) {
                text += hours + ":";
            } else if (hours > 0) {
                text += "0" + hours + ":";
            } else {
                text += "00:";
            }
            if (minutes > 9) {
                text += minutes + ":";
            } else {
                text += "0" + minutes + ":";
            }
            if (seconds > 9) {
                text += seconds;
            } else {
                text += "0" + seconds;
            }
            wlanGuTimeOut -= 1;
            $("#guTimeOutRun").html(text);
        } else {
           $(".guest").prop("disabled", true);
           $("#wlanRadio0, #wlanRadio1").prop("disabled",false).val("off");
           $("#spinner").show();
           $("#form_post").submit();
        }
    }
}
// encrpytion mode 
function setEncryption() {
    switch (_errorcount) {
        case "0":
            $("#security0_" + security0).prop("checked", true);
            break;
        default:
            $("#security0_" + security0_post).prop("checked", true);
            break;
    }
    if (security0 == "none") {
        disablePassword("0");
        if (wlanRadio0 == "on") {
            secCheck = false;
            secCheckMes += "<p>" + secCheckMes0 + "</p>";
        }
    }
    if (!secCheck) {
        // message no encrpytion
        $("#headerHint").removeClass("inactive").addClass("active");
        $("#headerHintText").html(secCheckMes);
    }
    setSecurity(security0);
}

function disablePassword(v) {
    $("#wpaKey" + v + "div").addClass("semiTransparent");
    $("#wpaKey" + v).attr("disabled", true);
    $("#wpaKey1").attr("disabled", true);
}

function enablePassword(v) {
    $("#wpaKey" + v + "div").removeClass("semiTransparent");
    $("#wpaKey" + v).attr("disabled", false);
    $("#wpaKey1").attr("disabled", false);
}

function setSecurity(v) {
    $("#security0").val(v);
}
// check form 
function checkForm() {
    formValid = true;
    var obj = $("#form_post");
    obj.find("input[data-ifValidation='true']:enabled").each(function() {
        if ($(this).is(":visible")) {
            eM = $(this).data("errormessageid");
            $(this).closest(".form-group").removeClass("has-error");
            $("#" + eM).removeClass("active");
            var validation = $(this).data("validation") ? $(this).data("validation") : "charValid";
            var validationEK = $(this).data("validationerrorkey") ? $(this).data("validationerrorkey") : "charInvalid";
            var minLength = $(this).data("minlength") ? $(this).data("minlength") : -1;
            var minLengthEK = $(this).data("minlengtherrorkey") ? $(this).data("minlengtherrorkey") : -1;
            var maxLength = $(this).data("maxlength") ? $(this).data("maxlength") : -1;
            var maxLengthEK = $(this).data("maxlengtherrorkey") ? $(this).data("maxlengtherrorkey") : -1;
            var errorMId = eM ? eM : -1;
            var userInput = $(this).val();
            if (minLength > userInput.length) {
                showError(this, errorMId, errMess[minLengthEK]);
                formValid = false;
                return true;
            } else if (maxLength < userInput.length) {
                showError(this, errorMId, errMess[maxLengthEK]);
                formValid = false;
                return true;
            } else if (errReg[validation] && !errReg[validation].test(userInput)) {
                showError(this, errorMId, errMess[validationEK]);
                formValid = false;
                return true;
            } else { // no validation errors
                return true;
            }
        }
    });
}

function showError(a, b, c) {
    $(a).closest(".form-group").addClass("has-error");
    $("#" + b).addClass("active").find(".text-error").html(c);
}
// bubble
function setBubble(t, f, c) {
    switch (c) {
        case true:
            $("#modalFooterText").show();
            $("#modalFooterText2").html(t);
            break;
        case false:
        default:
            $("#modalFooterText").hide();
            $("#modalFooterText2").html(t);
            break;
    }
    $("#modalFooter").show();
    // $("#actionHolder").val(f);
}