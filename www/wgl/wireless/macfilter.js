
var _errorcount = "${_errorcount}";
/* macfilter active */
var active_ch;

var wpsOn = "${wpsOn}";
var wpsOn_post = "${wpsOn_post}";
var wpsOn_err = "${err_wpsOn}";
if ( _errorcount != "0" && wpsOn_post != "" ){
    wpsOn = wpsOn_post;
}

var wpsOn1 = "${wpsOn1}";
var wpsOn1_post = "${wpsOn1_post}";
var wpsOn1_err = "${err_wpsOn1}";
if ( _errorcount != "0" && wpsOn1_post != "" ){
    wpsOn1 = wpsOn1_post;
}

var macEntryValid_err = "${macEntryValid_err}";
var macEntryExists_err = "${macEntryExists_err}";
var macEntryEmpty_err = "${macEntryEmpty_err}";

ifBubbleEnabled = "${ifBubbleEnabled}";
showBubbleDialog = "${showBubbleDialog}";
bubbleText = "${bubbleText}";

<~ if ${active} eq `` then ` 
    <~ if ${active_post} eq `` then ` 
var active = "off";
    ` ~>
` else `
var active = "${active}";
` ~>
var active_post = "${active_post}";
if (!active && !active_post) {
    active = "off";
    active_ch = false;
}
var connlist;
var currlist = new Array();
var fltrlist = new Array();
var fltrlistPost;
var stateT = {
    'Connected': '',
    'notConnected': ' *'
};
var no_clients = false;
var all_in_list = false;
$(document).ready(function() {
    $("#buttonApply").click(function(e) {
        makePostFltrList();
        setPostFltrList();
        $("#form_post").submit();   
        e.preventDefault();
    });
    $("#tmp_wpsOnOff").click(function(e){
        var iV = $('#tmp_wpsOnOff').prop('checked');
        switch ( iV ){
            case false:
                wpsSetOff();
            break;
            case true:
                // wpsSetOn();
            break;
        }
    });
    $(".macFilterMode").click(function(e) {
        var i = this.id;
        var iV = $(this).prop('checked');
        iV == true ? iV = "on" : iV = "off";
        active = iV;
        clickMacFilterMode(iV, i);
    });
    $(".addFltr").click(function(e) {
        var t = $('#knownStations').find('option:selected').val();
        if (t){
            pushFltr(t);
            showOnChangeMessage();
        }
        e.preventDefault();
    });
    $(".addFltrM").click(function(e) {
        var s = false;
        var er = macEntryValid_err;;
        var t = $('#manStation').val();
        if (t){
            t=t.toUpperCase();
            if(t.match(errReg['macAdrValid'])){
                if ( checkDoubleMac(t) ){
                    $("#knownStations option").each(function(iKs,vKs){
                        $m = cleanMac(t);
                        $v = $(this).html();
                        $vC = cleanMac($v);
                        if($m==$vC){
                            t = $v;
                        }   
                    });
                    pushFltr(t);
                    $('#manStation').val('');
                    s=true;
                    showOnChangeMessage();
                /*
                if ( fltrlist.checkDoubValArray(t) ){
                    pushFltr(t);
                    $('#manStation').val('');
                    s=true;
                    showOnChangeMessage();
                */
                }else{
                    er = macEntryExists_err;
                }
            }
        }else{
            er = macEntryEmpty_err;
        }
        if (!s){
            $("#macFilterErrBox").show().children().html(er);
            $("#addFltrMFormGroup").addClass("has-error");
        }else{
            $("#macFilterErrBox").hide().children().html('');
            $("#addFltrMFormGroup").removeClass("has-error");
        }
        e.preventDefault();
    });
    init();
});

function checkDoubleMac(m){
    var ret = true;
    $m = m;
    $(".macDiv").each(function(i,v){
        $m = cleanMac($m);
        $v = cleanMac($(this).html());
        if($m==$v){
            ret = ret && false;
        }
    });
    return ret;
}
function cleanMac(v){
    $v = v;
    return $v.toUpperCase().replace(/[:-]/g,"");
}
function beautifyMac(v){
    $v = cleanMac(v);
    l = $v.length;
    $v = $v.replace(/(.{2})/g,"$1:");
    $v = $v.substring(0,$v.length-1);
    return $v;
}

function init() {
    setWpsMode();
    setMacFilterMode();
    getConnCurrList();
    makeConnList();
    makeApprovedList();
}

function pushFltr(t){
    fltrlist.push(beautifyMac(t));
    makeLists();
    makeConnList();
    makeApprovedList();
}
function makePostFltrList(){
    fltrlistPost = "";
    if (fltrlist.length > 0){
        for (i = 0; i < fltrlist.length; i++) {
            if(i>0){
                fltrlistPost+=",";
            }
            fltrlistPost+='"Entry":{"Mac":"'+fltrlist[i]+'"}';
        };
    }
}
function setPostFltrList(){
    rP = $("#post_mac").val();
    if (fltrlistPost != ""){
        setValue("post_mac", rP.replace("*", fltrlistPost));
        setValue("post_mac_1", rP.replace("*", fltrlistPost));
    }else{
        setValue("post_mac", rP.replace("*", ""));
        setValue("post_mac_1", rP.replace("*", ""));
    }
}

function getConnCurrList() {
    var data = getKnownStations();
    if (isArray(data.KnownStations.Station)) {
        if (data.KnownStations.Station.length > 0) {
            for (i = 0; i < data.KnownStations.Station.length; i++) {
                station = new Array();
                station["mac"] = data.KnownStations.Station[i].Mac;
                station["state"] = data.KnownStations.Station[i].State;
                currlist.push(station);
            };
        }
    } else if (isObject(data.KnownStations.Station)) {
        station = new Array();
        station["mac"] = data.KnownStations.Station.Mac;
        station["state"] = data.KnownStations.Station.State;
        currlist.push(station);
    }
    var filterList = getApprovedStations();
    if (isArray(filterList.Entry)) {
        if (filterList.Entry.length > 0) {
            for (i = 0; i < filterList.Entry.length; i++) {
                fltrlist.push(filterList.Entry[i].Mac);
            };
        }
    }
    makeLists();
}
function makeLists(){
    connlist = new Array();
    var c, f, inc;
    for (c = 0; c < currlist.length; c++) {
        inc = true;
        for (f = 0; f < fltrlist.length; f++) {
            if (currlist[c]["mac"] == fltrlist[f]) {
                inc = false;
            }
        }
        if (inc) {
            station = new Array();
            station = currlist[c];
            connlist.push(station);
        }
    }
    if (connlist.length == 0) {
        if (currlist.length > 0) all_in_list = true;
        else no_clients = true;
    } else {
        no_clients = false;
        all_in_list = false;
    }
}
function getKnownStations() {
    var data = eval("(<~ query `Wireless.KnownStations:format=json` json ~>)");     
    return data;
}
function getApprovedStations() {
    var data = eval("(<~ query `Wireless.Radio[0].AP[0].MACFilter.Entry{*}:format=json` json ~>)");
    return data;
}
function makeConnList() {
    var opS;
    $('#knownStations').html('');
    if (no_clients) {
        inactiveActive('knownStationsText');
        activeInactive('knownStationsSel');
        $('#knownStationsTextI').html("<~ trans `page_wireless_filter_no_clients_2014` ~>");
    } else if (all_in_list) {
        inactiveActive('knownStationsText');
        activeInactive('knownStationsSel');
        $('#knownStationsTextI').html("<~ trans `page_wireless_filter_all_clients_in_list_2014` ~>");
    } else {
        inactiveActive('knownStationsSel');
        activeInactive('knownStationsText');
        $.each(connlist, function(i, v) {
            v.state == "notConnected" ? opS = v.mac + ' *' : opS = v.mac;
            $('#knownStations').append($('<option>', {
                value: v.mac,
                text: opS
            }));
        });
    }
}
function makeApprovedList() {
    var t;
    $('#apprStations').html('');
    if ( fltrlist.length == 0 ){
        inactiveActive("appStationsN");
        activeInactive("appStationsY");
    }else{
        inactiveActive("appStationsY");
        activeInactive("appStationsN");
        $.each(fltrlist, function(i, v) {
            addFiltr(v);
        });
    }
}
function addFiltr(v) {
    var str = '';
	str += '<div class="caption-type-2">&nbsp;</div>';
    str += '<div class="row" id="' + v + '">';
    str += '<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 approved-wlan-stations">';
    str += '<span class="caption-type-2 macDiv">' + v + '</span>';
    str += '</div>';
    str += '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">';
    str += '<div class="button-add">';
    str += '<a href="javascript:removeFltr(\'' + v + '\');" class="approvedList">';
    str += '<div class="icon- button-remove-icon">l</div>';
    str += '</a>';
    str += '</div>';
    str += '</div>';
    str += '</div>';
    str += '<div class="caption-type-2"><hr></div>';
    $("#apprStations").append(str);
}
function removeFltr(i) {
    fltrlist.removeValArray(i);
    makeLists();
    makeConnList();
    makeApprovedList();
    showOnChangeMessage();
}

/* wps mode */
function setWpsMode(){
    if ( wpsOn == "yes" || wpsOn1 == "yes" ){
        $("#tmp_wpsOnOff").prop("checked", true);
        toggleWpsMode("show");
    }else{
        toggleWpsMode("hide");
        $("#divMacFilter").removeClass("inactive").addClass("active");
    }
}
function toggleWpsMode(v){
    switch (v){
        case "show":
        $("#divWpsOn").removeClass("inactive").addClass("active");
        break;
        case "hide":
        $("#divWpsOn").removeClass("active").addClass("inactive");
        break;
    }
}
function wpsSetOff(){
    $("#wpsOn,#wpsOn1").val("no").attr("disabled", false);
    toggleWpsMode("hide");
    $("#divMacFilter").removeClass("inactive").addClass("active");
}

/* mac filter mode */
function setMacFilterMode() {
    var nN;
    switch (_errorcount) {
        case "0":
            if (active == "on") {
                active_ch = true;
            } else {
                active_ch = false;
            }
            nN = active;
            break;
        default:
            if (active_post == "on") {
                active_ch = true;
            } else {
                active_ch = false;
            }
            nN = active_post;
            break;
    }

    if (active_ch)
        macFilterShow("active");
    else
        macFilterHide("active");

    setValue("active", nN);
    setProp("active", "checked", active_ch);
}
function clickMacFilterMode(v, i) {
    $("#" + i).val(v);
    switch (v) {
        case "on":
            macFilterShow(i);
            break;
        case "off":
            macFilterHide(i);
            break;
    }
    $("#post_active").val(v);
    $("#post_active_1").val(v);
}
function macFilterHide(i) {
    activeInactive("div_" + i);
    inactiveActive("wlanFiltInactive");
    activeInactive("wlanFiltActive");
}
function macFilterShow(i) {
    inactiveActive("div_" + i);
    inactiveActive("wlanFiltActive");
    activeInactive("wlanFiltInactive");
} 
