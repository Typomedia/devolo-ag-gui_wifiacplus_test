
var _errorcount = "${_errorcount}";

var parentallist = new Array();
var parentalList;
var parActive = "${parActive}";
var parActiveStat = "off";

var macAddress = "${_macAddress}";

var ntptTime = "${ntptime}";
var ntptTimeH1 = "${ntptTimeH1}";
var ntptTimeH2 = "${ntptTimeH2}";

var errTimeInvalid = "${errTimeInvalid}";

var daysArr = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
var daysTransArr = ['${day_mon}','${day_tue}','${day_wed}','${day_thu}','${day_fri}','${day_sat}','${day_sun}'];

var eM;
var check = true;
var actCal;
var actCalIndex;
var calObj;

var formValid;

ifBubbleEnabled = "${ifBubbleEnabled}";
showBubbleDialog = "${showBubbleDialog}";
bubbleText = "${bubbleText}";

$(document).ready(function() {

    $("#buttonApply").click(function(e) {
		formValid = true;
		if (parActiveStat == "on"){
			checkForm();
		}else{
			formValid = true;
		}
		if (formValid){
			// hook form fields into station-collection ( on, off )
            // console.log("parentallist1",parentallist);
			hookFormStaCol();
            // console.log("parentallist2",parentallist);
			// hook station-collection to date-collection ( calObj )
			stationColToDateCol();
			$("#parCalendar").val(JSON.stringify(calObj));
            // console.log(calObj);
            // console.log(JSON.stringify(calObj));
            // $("#spinner").hide();
            $("#form_post").submit();
        }else{
        	$("#spinner").hide();
        }
        e.preventDefault();
    });
	$("#tmp_parentalOnOff").click(function(e){
		var iV = $('#tmp_parentalOnOff').prop('checked');
		switch ( iV ){
			case false:
				parActiveSetOff();
			break;
			case true:
				parActiveSetOn();
			break;
		}
	});
	// click on del
	$(document.body).on('click', '.entryDel' ,function(e){
        showOnChangeMessage();
		$(e.target).closest(".entryInputs").find("input").val("");
		$(e.target).closest(".entryInputs").find("input").val("");
		checkForm();
	});
    setNtpt();
	init();
});	
function setNtpt(){
    temp = eval("(<~ query `NTPClient.CurrentTime:format=json` json ~>)"); 
    if(jQuery.type(temp.CurrentTime) == "string"){
        ntptTime = temp.CurrentTime;
    }
    temp = eval("(<~ query `NTPClient.TimeIsValid:format=json` json ~>)");
    if(jQuery.type(temp.TimeIsValid) == "string"){
        ntptTimeValid = temp.TimeIsValid;
    } 
}
function init(){
	getParentalStations();
	dateColToStationCol();
	checkPar();
	if (check && isObject(actCal)){
		$("#divParStation").show();
		setParActiveMode();
		tmplCalEntryMarkup();
		renderCalendar();
	}else{
        $("#divParStation").hide();
    }
}
function setParActiveMode(){
	if ( parActiveStat == "on" ){
		$("#tmp_parentalOnOff").prop("checked", true);
		toggleParActiveMode("show");
	}else{
		toggleParActiveMode("hide");
	}
}
function toggleParActiveMode(v){
	switch (v){
		case "show":
		$("#parInactiveMessage").hide();
		$("#parActiveMessage").show();
		$("#divParActive").removeClass("inactive").addClass("active");
		break;
		case "hide":
		$("#parActiveMessage").hide();
		$("#parInactiveMessage").show();
		$("#divParActive").removeClass("active").addClass("inactive");
		break;
	}
}
function parActiveSetOff(){
	$("#parActive").val("off");
	parActiveStat = "off";
	toggleParActiveMode("hide");
}
function parActiveSetOn(){
	$("#parActive").val("on");
	parActiveStat = "on";
	toggleParActiveMode("show");
}
function checkPar(){
	check=check && checkActiveMode();
	if(check){
		check=check && checkInParList();
	}else{
		$("#headerHint").show();
		$("#headerHintText").html("<~ trans `header_error` ~>");
	}
	if(check){
		checkNTPT();
	}else{
		$("#headerHint").show();
		$("#headerHintText").html("<~ trans `header_error` ~>");
	}
}
function checkActiveMode(){
	retval = true;
	if(parActive == "no"){
		retval=false;
	}
	return retval;
}
function checkNTPT(){
	if (ntptTime == ""){
		$("#headerHint").show().find("#headerHintText").html(ntptTimeH1+"<br>"+ntptTimeH2);
	}
}
function checkInParList(){	
	var retval = false;
	$.each(parentallist, function(i,v){
		if(v.Mac == macAddress ){
			actCalIndex = i;
			actCal = v;
			parActiveStat = v.Active;
			retval = true;
		}
	});
	return retval;
}
function dateColToStationCol(){
    var ch;
    if ( isObject(parentalList) ){
        if ( isArray(parentalList.Calendar) ){
            if ( parentalList.Calendar.length > 0 ){
                $.each(parentalList.Calendar, function(iC,vC){
                    var Day = vC.Day;
                    if ( isArray(vC.Station) ){
                        if ( vC.Station.length > 0 ){
                            $.each(vC.Station, function(iS,vS){
                                var iS = iS;
                                var vS = vS;
                                obj = new Object();
                                obj.Mac = vS.Mac;
                                obj.Name = vS.Name;
                                obj.Active = vS.Active;
                                obj.Days = new Array();
                                ch = false;
                                $.each(parentallist, function(iT,vT){
                                    if(vT.Mac == vS.Mac){
                                        vS.Day = Day;
                                        vT.Days.push(vS);
                                        ch = true;
                                    }
                                });
                                if(!ch){
                                    vS.Day = Day;
                                    obj.Days.push(vS);
                                    parentallist.push(obj);
                                }   
                            });
                        }
                    }else if(isObject(vC.Station)){
                        obj = new Object();
                        obj.Mac = vC.Station.Mac;
                        obj.Name = vC.Station.Name;
                        obj.Active = vC.Station.Active;
                        obj.Days = new Array();
                        vC.Station.Day = Day;
                        $.each(parentallist, function(iT,vT){
                            ch = false;
                            if(vT.Mac == vC.Station.Mac){
                                vT.Days.push(vC.Station);
                                ch = true;
                            }
                        });
                        if(!ch){
                            obj.Days.push(vC.Station)
                            parentallist.push(obj);
                        }   
                    } 
                });
            }
        }
    }
    // console.log(parentallist);
}
function stationColToDateCol(){
    calObj = new Object;
    calObj.Calendar = [];
    // console.log("daysArr",daysArr);
    $.each(daysArr, function(iD,vD){
        var obj;
        obj = new Object();
        obj.Day = vD;
        obj.Station = [];
        calObj.Calendar.push(obj);
        $.each(parentallist, function(iP,vP){
            $.each(vP.Days, function(iT,vT){
                if(vT.Day == vD){
                    objD = new Object();
                    objD.Mac = vT.Mac;
                    objD.Active = vT.Active;
                    // objD.Name = vT.Name;
                    objD.TimeLimit = vT.TimeLimit;
                    // objD.RemainingTime = vT.RemainingTime;
                    obj.Station.push(objD);
                }
            });
        });
    });
}
function getParentalStations(){
    parentalList = eval("(<~ query `Wireless.ParentalControl.Calendar{*}:format=json` json ~>)");
}
function tmplCalEntryMarkup(){
	eM = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 calEntries">';
  	eM+= '	<div class="row">';
  	eM+= '    <div class="row">';
  	eM+= '       <p class="caption-type-2">$'+'{entryDayTrans}</p>';
	eM+= '       <div class="row entryInputs">';
    eM+= '    		<div class="col-xs-10 col-sm-4 col-md-3 col-lg-3">';
    eM+= '             <div class="form-group">';
    eM+= '				<input type="text" class="form-control caption-type-2 entryTimeLimit" value="$'+'{entryTimeLimit}" placeholder="<~ trans `page_wireless_parental_station_hours` ~>:<~ trans `page_wireless_parental_station_minutes` ~>">';
    eM+= '             </div>';
    eM+= '          </div>';
    //eM+= '          <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    //eM+= '             <p class="component-title text-center">&nbsp;</p>';
    //eM+= '          </div>';
    //eM+= '          <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">';
    //eM+= '             <div class="form-group">';
    //eM+= ' 					<input type="text" class="form-control caption-type-2 entryMinute" value="$'+'{entryMinute}" placeholder="<~ trans `page_wireless_parental_station_minutes` ~>">';
    //eM+= '             </div>';
    //eM+= '          </div>';
    eM+= '          <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    eM+= '	     		<p class="component-title">&nbsp;</p>';
    eM+= '          </div>';
    eM+= '	        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    eM+= '             <div class="button-add">';
    eM+= '                <a href="javascript:void(0);">';
    eM+= '                   <div class="icon- button-delete-icon entryDel">l</div>';
    eM+= '                </a>';
    eM+= '             </div>';
    eM+= '          </div>';
    eM+= '       </div>';
    eM+= '    </div>';
    eM+= '	  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error-message-box errorDiv">';
    eM+= ' 		<p class="text-error errorText"></p>';
    eM+= '	  </div>';
    eM+= '  </div>';
    eM+= '  <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">';
    eM+= ' 	  <div class="row"><hr></div>';
    eM+= '  </div>';     
    eM+= '</div>';     
}
function renderCalendar(){
    $("#divCalendar").html("");
    var c = 0;
    $.each(daysArr, function(iD,vD){
    	var obj=new Object();
    	obj.entryDayTrans = daysTransArr[iD];
    	obj.Day = vD;
    	// obj.entryHour = "";
    	// obj.entryMinute = "";
    	// obj.entryHourOptions = "";
    	// obj.entryMinuteOptions = "";
        obj.entryTimeLimit = "";
        obj.entryTimeLimitOptions = "";
    	$.each(actCal.Days, function(aC,vC){
    		if(vC.Day == obj.Day){
    			arr = vC.TimeLimit.split(":");
    			// console.log(arr);
    			// obj.entryHour=arr[0];
    			// obj.entryMinute=arr[1];
                obj.entryTimeLimit = vC.TimeLimit;
    		}
    	});
		$.template( "parEntriesTmpl", eM );
        $.tmpl ( "parEntriesTmpl", obj ).appendTo("#divCalendar");
    });
}
function hookFormStaCol(){
	var c = 0;
    var cI = 0;
	var obj = new Object();
	obj.Active = parActiveStat;
	obj.Mac = macAddress;
	obj.Days = [];
	$(".calEntries").each(function(iE,iV){
        $t = $(this).find(".entryTimeLimit").val();
		if($t!=""){
            $t=unifyTimeFormat($t);
			var objT = new Object();
			objT.TimeLimit = $t;
			objT.Day = daysArr[c];
			objT.Mac = macAddress;
			objT.Active = parActiveStat;
			obj.Days.push(objT);
            cI++;
		}
		c++;	
	});
    if(cI==0){
        var objT = new Object();
        objT.TimeLimit = "";
        objT.Day = daysArr[0];
        objT.Mac = macAddress;
        objT.Active = parActiveStat;
        obj.Days.push(objT);
    }
	parentallist[actCalIndex] = obj;
}
function checkForm(){
	$(".calEntries").each(function(index){
        var t = $(this).find(".entryTimeLimit").val();
		var c = checkEntry($(this),t);
		formValid = formValid && c;
	});
}
function unifyTimeFormat(v){
    if (v!=""){
        var a = v.split(":");
        if(a.length == 2){
            if(a[0].length == 1){
                v="0"+a[0]+":"+a[1];
            }
        }
    }
    return v;
}
function checkEntry(a,b){
	retval = true;
	if ( b == "" ){
		releaseHasError(a,true);
        releaseErrorText(a);
        return true;
	}
    b = unifyTimeFormat(b);
	eB = errReg['timeValid'].exec(b);
	if ( eB ){
		releaseHasError(a,true);
		releaseErrorText(a);
		return true;
	}else{
		if (!eB){
			setErrorText(a,errTimeInvalid);
			var ret = setHasError(a,b,false);
			retval = retval && false;
		}else{
			releaseHasError(a,b,false);
			retval = retval && true;
		}
        /*
		if (!eC){
			setErrorText(a,errTimeInvalid);
			var ret = setHasError(a,false,c);
			retval = retval && false;
		}else{
			releaseHasError(a,false,c);
			retval = retval && true;
		}
        */
	}
    return retval;
}

function releaseErrorText(a){
	$c = a.find(".error-message-box");
	$c.hide();
	$c.find(".text-error").html('');
}
function setErrorText(a,b){
	$c = a.find(".error-message-box");
	$c.show();
	$c.find(".text-error").html(b);
}
function setHasError(a,b){
	if ( b ){
		a.find(".entryTimeLimit").parent().addClass("has-error");
		return true;
	}
}
function releaseHasError(a,b){
	if ( b ){
		a.find(".entryTimeLimit").parent().removeClass("has-error");
		return true;
	}
}
