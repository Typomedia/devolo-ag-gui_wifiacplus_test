<input type="hidden"
	id="post_active"
	name=":sys:Wireless.Radio[0].AP[0].MACFilter.Active:format=scalar"
	value="${active}"
/>

<input type="hidden"
	id="post_mac"
	name=":sys:Wireless.Radio[0].AP[0].MACFilter.Entry{}:format=json"
	value="{*}"
/>

<input type="hidden"
	id="post_active_1"
	name=":sys:Wireless.Radio[1].AP[0].MACFilter.Active:format=scalar"
	value="${active}"
/>

<input type="hidden"
	id="post_mac_1"
	name=":sys:Wireless.Radio[1].AP[0].MACFilter.Entry{}:format=json"
	value="{*}"
/>

<input type="hidden"
    id="wpsOn"
    disabled="disabled" 
    name=":sys:Wireless.Radio[0].WPSEnabled"
    value="<~ if ${_errorcount} eq `0` then ${wpsOn} else ${wpsOn_post} ~>"
/>

<input type="hidden"
    id="wpsOn1"
    disabled="disabled" 
    name=":sys:Wireless.Radio[1].WPSEnabled"
    value="<~ if ${_errorcount} eq `0` then ${wpsOn1} else ${wpsOn1_post} ~>"
/>

<input type="hidden"
name="_okdir"
value="wireless"
id="post_okdir" />

<input type="hidden"
name="_okpage"
value="home"
id="post_okpage" />