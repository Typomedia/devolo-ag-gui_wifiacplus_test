
var _errorcount = "${_errorcount}";

var parActive = "${parActive}";
var parActive_post = "${parActive_post}";
var parActive_err = "${err_parActive}";
if ( _errorcount != "0" && parActive_post != "" ){
	parActive = parActive_post;
}

var ntptTime = "${ntptime}";
var ntptTimeH1 = "${ntptTimeH1}";
var ntptTimeH2 = "${ntptTimeH2}";

var macEntryValid_err = "${macEntryValid_err}";
var macEntryExists_err = "${macEntryExists_err}";
var macEntryEmpty_err = "${macEntryEmpty_err}";

ifBubbleEnabled = "${ifBubbleEnabled}";
showBubbleDialog = "${showBubbleDialog}";
bubbleText = "${bubbleText}";

var connlist;
var currlist = new Array();
var parentallist = new Array();
var parentallistPost;
var stateT = {
    'Connected': '',
    'notConnected': ' *'
};
var no_clients = false;
var all_in_list = false;
var parentalList;
var calObj;

var daysArr = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
var dayN = new Date();
var daysArrN = [];
var daysArrN = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
var actualDay = daysArrN[dayN.getDay()];

var eM;

// if something changed in client ( add / del )
var changedForm = false;

$(document).ready(function() {
    $("#buttonApply").click(function(e) {
        stationColToDateCol();
        // console.log(parentallist);
        // console.log(JSON.stringify(calObj));
        $("#parCalendar").val(JSON.stringify(calObj));
        // $("#spinner").hide();
        $("#form_post").submit();
        e.preventDefault();
    });
	$("#tmp_parentalOnOff").click(function(e){
		var iV = $('#tmp_parentalOnOff').prop('checked');
		switch ( iV ){
			case false:
				parActiveSetOff();
			break;
			case true:
				parActiveSetOn();
			break;
		}
	});
    $(".addParental").click(function(e) {
        var t = $('#knownStations').find('option:selected').val();
        if (t){
            addParental(t);
        }
        e.preventDefault();
    });
    $(".addParentalM").click(function(e) {
        var s = false;
        var er = macEntryValid_err;;
        var t = $('#manStation').val();
        if (t){
            t=t.toUpperCase();
            if(t.match(errReg['macAdrValid'])){
                if ( checkDoubleMac(t) ){
                    $("#knownStations option").each(function(iKs,vKs){
                        $m = cleanMac(t);
                        $v = $(this).html();
                        $vC = cleanMac($v);
                        if($m==$vC){
                            t = $v;
                        }
                    });
                    addParental(t);
                    $('#manStation').val('');
                    s=true;
                }else{
                    er = macEntryExists_err;
                }
            }
        }else{
            er = macEntryEmpty_err;
        }
        if (!s){
            $("#macFilterErrBox").show().children().html(er);
            $("#addMacFilterFormGroup").addClass("has-error");
        }else{
            $("#macFilterErrBox").hide().children().html('');
            $("#addMacFilterFormGroup").removeClass("has-error");
        }
        e.preventDefault();
    });
    // click on del
    $(document.body).on('click', '.entryDel' ,function(){
        var a = $(this).attr("id").split("_");
        removeParentalEntry(a[1]);
        changedForm = true;
    });
    // click on add
    $(document.body).on('click', '.entryEdit:not(.semiTransparent)' ,function(){
        // $("#spinner").show();   
        var a = $(this).attr("id").split("_");
        // if something changed => save the list an route to detail-page
        if(changedForm){
            $("#post_macaddress").val(a[1]);
            $("#post_okdir").val("wireless");
            $("#post_okpage").val("parental_station");
            $("#buttonApply").click();
        }else{
            // if nothing changed => link to the saved station
            goEdit(a[1]);
        }
        // $("#spinner").show();
        // $("#form_post").submit();
        // console.log(a);
        // goEdit(a[1]);
    });
    setNtpt();
	init();
});

function setNtpt(){
    temp = eval("(<~ query `NTPClient.CurrentTime:format=json` json ~>)"); 
    if(jQuery.type(temp.CurrentTime) == "string"){
        ntptTime = temp.CurrentTime;
    }
    temp = eval("(<~ query `NTPClient.TimeIsValid:format=json` json ~>)");
    if(jQuery.type(temp.TimeIsValid) == "string"){
        ntptTimeValid = temp.TimeIsValid;
    } 
}

function checkDoubleMac(m){
    var ret = true;
    $m = m;
    $(".macDiv").each(function(i,v){
        $m = cleanMac($m);
        $v = cleanMac($(this).html());
        if($m==$v){
            ret = ret && false;
        }
    });
    return ret;
}
function cleanMac(v){
    $v = v;
    return $v.toUpperCase().replace(/[:-]/g,"");
}
function beautifyMac(v){
    $v = cleanMac(v);
    l = $v.length;
    $v = $v.replace(/(.{2})/g,"$1:");
    $v = $v.substring(0,$v.length-1);
    return $v;
}
function init(){
	checkNTPT();
	setParActiveMode();
    tmplParEntryMarkup();
	getConnCurrList();
    makeConnList();
    // makeApprovedList();
    renderParentalTable();
}

function checkNTPT(){
	if (ntptTime == ""){
		$("#headerHint").show().find("#headerHintText").html(ntptTimeH1+"<br>"+ntptTimeH2);
	}
}

/* paractive-mode */
function setParActiveMode(){
	if ( parActive == "on" ){
		$("#tmp_parentalOnOff").prop("checked", true);
		toggleParActiveMode("show");
	}else{
		toggleParActiveMode("hide");
	}
}
function toggleParActiveMode(v){
	switch (v){
		case "show":
		$("#parInactiveMessage").hide();
		$("#parActiveMessage").show();
		toggleKnownStations("show");
		$("#divParActive").removeClass("inactive").addClass("active");
		break;
		case "hide":
		$("#parActiveMessage").hide();
		$("#parInactiveMessage").show();
		toggleKnownStations("hide");
		$("#divParActive").removeClass("active").addClass("inactive");
		break;
	}
}
function parActiveSetOff(){
	$("#parActive").val("off");
	toggleParActiveMode("hide");
}
function parActiveSetOn(){
	$("#parActive").val("on");
	toggleParActiveMode("show");
}

function toggleKnownStations(v){
	switch (v){
		case "show":
		$("#divKnownStations").removeClass("inactive").addClass("active");
		break;
		case "hide":
		$("#divKnownStations").removeClass("active").addClass("inactive");
		break;
	}
}
function getConnCurrList() {
    var data = getKnownStations();
    if (isArray(data.KnownStations.Station)) {
        if (data.KnownStations.Station.length > 0) {
            for (i = 0; i < data.KnownStations.Station.length; i++) {
                station = new Array();
                station["Mac"] = data.KnownStations.Station[i].Mac;
                station["State"] = data.KnownStations.Station[i].State;
                currlist.push(station);
            };
        }
    } else if (isObject(data.KnownStations.Station)) {
        station = new Array();
        station["Mac"] = data.KnownStations.Station.Mac;
        station["State"] = data.KnownStations.Station.State;
        currlist.push(station);
    }

    // existing entries
    parentalList = getParentalStations();

    // day-collection to station-collection
    dateColToStationCol();

    makeKnownStatList();
}
function dateColToStationCol(){
    var ch;
    if ( isObject(parentalList) ){
        if ( isArray(parentalList.Calendar) ){
            if ( parentalList.Calendar.length > 0 ){
                $.each(parentalList.Calendar, function(iC,vC){
                    var Day = vC.Day;
                    if ( isArray(vC.Station) ){
                        if ( vC.Station.length > 0 ){
                            $.each(vC.Station, function(iS,vS){
                                var iS = iS;
                                var vS = vS;
                                obj = new Object();
                                obj.Mac = vS.Mac;
                                obj.Name = vS.Name;
                                obj.Active = vS.Active;
                                obj.Days = new Array();
                                ch = false;
                                $.each(parentallist, function(iT,vT){
                                    if(vT.Mac == vS.Mac){
                                        vS.Day = Day;
                                        vT.Days.push(vS);
                                        ch = true;
                                    }
                                });
                                if(!ch){
                                    vS.Day = Day;
                                    obj.Days.push(vS);
                                    parentallist.push(obj);
                                }   
                            });
                        }
                    }else if(isObject(vC.Station)){
                        obj = new Object();
                        obj.Mac = vC.Station.Mac;
                        obj.Name = vC.Station.Name;
                        obj.Active = vC.Station.Active;
                        obj.Days = new Array();
                        vC.Station.Day = Day;
                        $.each(parentallist, function(iT,vT){
                            ch = false;
                            if(vT.Mac == vC.Station.Mac){
                                vT.Days.push(vC.Station);
                                ch = true;
                            }
                        });
                        if(!ch){
                            obj.Days.push(vC.Station)
                            parentallist.push(obj);
                        }   
                    } 
                });
            }
        }
    }
}
function stationColToDateCol(){
    calObj = new Object;
    calObj.Calendar = [];
    $.each(daysArr, function(iD,vD){
        obj = new Object();
        obj.Day = vD;
        obj.Station = [];
        calObj.Calendar.push(obj);
        $.each(parentallist, function(iP,vP){
            $.each(vP.Days, function(iT,vT){
                if(vT.Day == vD){
                    objD = new Object();
                    objD.Mac = beautifyMac(vT.Mac);
                    objD.Active = vT.Active;
                    // objD.Name = vT.Name;
                    objD.TimeLimit = vT.TimeLimit;
                    // objD.RemainingTime = vT.RemainingTime;
                    obj.Station.push(objD);
                }
            });
        });
    });
}
function makeKnownStatList(){
    connlist = new Array();
    var c, f, inc;
    for (c = 0; c < currlist.length; c++) {
        inc = true;
        $.each(parentallist, function(iP,vP){
            if (currlist[c]["Mac"] == vP.Mac) {
                inc = false;
            }
        });
        if (inc) {
            station = new Array();
            station = currlist[c];
            connlist.push(station);
        }
    }
    if (connlist.length == 0) {
        if (currlist.length > 0) all_in_list = true;
        else no_clients = true;
    } else {
        no_clients = false;
        all_in_list = false;
    }
}
function getKnownStations() {
    var data = eval("(<~ query `Wireless.KnownStations:format=json` json ~>)");  
    return data;
}
function getParentalStations(){
    var data = eval("(<~ query `Wireless.ParentalControl.Calendar{*}:format=json` json ~>)");
    // var $data = jQuery.extend(true, {}, data);
    // console.log($data);
    return data;
}
function makeConnList() {
    var opS;
    $('#knownStations').html('');
    if (no_clients) {
        inactiveActive('knownStationsText');
        activeInactive('knownStationsSel');
        $('#knownStationsTextI').html("<~ trans `page_wireless_filter_no_clients_2014` ~>");
    } else if (all_in_list) {
        inactiveActive('knownStationsText');
        activeInactive('knownStationsSel');
        $('#knownStationsTextI').html("<~ trans `page_wireless_filter_all_clients_in_list_2014` ~>");
    } else {
        inactiveActive('knownStationsSel');
        activeInactive('knownStationsText');
        $.each(connlist, function(i, v) {
            v.state == "notConnected" ? opS = v.Mac + ' *' : opS = v.Mac;
            $('#knownStations').append($('<option>', {
                value: v.Mac,
                text: opS
            }));
        });
    }
}
function tmplParEntryMarkup(){  
    eM = '<div class="row" id="$'+'{entryId}">';
    eM+= '  <div class="row">&nbsp;</div>';
    eM+= '  <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5">';
    eM+= '    <div class="col-xs-11 col-sm-9 col-md-12 col-lg-12">';
    eM+= '      <div class="approved-wlan-stations">';
    eM+= '        <span class="caption-type-2 macDiv">$'+'{entryMacId}</span>';
    eM+= '      </div>';
    eM+= '    </div>';
    eM+= '    <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11">';
    eM+= '      <div class="approved-wlan-stations $'+'{entryStationNameClass}"">';
    eM+= '        <span class="caption-type-2">$'+'{entryStationName}</span>';
    eM+= '      </div>';
    eM+= '    </div>';
    eM+= '    <div class="col-xs-1 col-sm-4 col-md-1 col-lg-1"></div>';
    eM+= '    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
    eM+= '      <p class="caption-type-2"><~ trans `page_wireless_parental_remaining_time` ~></p>';
    eM+= '    </div>';
    eM+= '    <div class="time-wrapper $'+'{entryTiWrClassLimit}">';                
    eM+= '     <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">';
    eM+= '       <div class="approved-wlan-stations">';
    eM+= '         <span class="caption-type-2">$'+'{entryRemainingTime}</span>';
    eM+= '       </div>';
    eM+= '     </div>';
    eM+= '     <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">';
    eM+= '       <p class="caption-type-2 text-center"><~ trans `page_wireless_parental_of_remaining_time` ~></p>';
    eM+= '     </div>';
    eM+= '     <div class="col-xs-4 col-sm-3 col-md-3 col-lg-3">';
    eM+= '       <div class="approved-wlan-stations">';
    eM+= '        <span class="caption-type-2">$'+'{entryTimeLimit}</span>';
    eM+= '       </div>';
    eM+= '     </div>';
    eM+= '    </div>';
    eM+= '    <div class="col-xs-11 col-sm-7 col-md-7 col-lg-7 $'+'{entryTiWrClassNoLimit}">';
    eM+= '      <div class="approved-wlan-stations">';
    eM+= '        <span class="caption-type-2"><~ trans `parental_no_time_limit` ~></span>';
    eM+= '      </div>';
    eM+= '    </div>';
    eM+= '  </div>';
    eM+= '  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">&nbsp;</div>';
    eM+= '  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    eM+= '      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
    eM+= '          <div class="button-add entryDel" id="del_$'+'{index}">';
    eM+= '              <a href="javascript:void(0);">';
    eM+= '                 <div class="icon- button-delete-icon">l</div>';
    eM+= '              </a>';
    eM+= '          </div>';
    eM+= '      </div>';
    eM+= '      <p class="caption-type-2">&nbsp;</p>';
    eM+= '      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
    eM+= '          <div class="button-add entryEdit $'+'{entryEditClass}" id="add_$'+'{entryId}">';
    eM+= '              <a href="javascript:void(0);">';
    eM+= '                  <div class="icon- button-add-icon">i</div>';
    eM+= '              </a>';
    eM+= '          </div>';
    eM+= '      </div>';
    eM+= '  </div>';
    eM+= '</div>';
    eM+= '</div>';
    eM+= '<div class="row">';
    eM+= '  <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7"><hr></div>';
    eM+= '</div>';
}
function renderParentalTable(){
    $("#divParEntries").html("");
    var c = 0;
    if (isArray(parentallist)){
        if(parentallist.length > 0){   
            $.each(parentallist, function( index, value ) {
                remTime = "";
                var remTimeCheck = false;
                if (value.Active != "on"){
                    value.entryTiWrClassLimit = "inactive";
                    value.entryTiWrClassNoLimit = "active";
                }else{
                    var cT = false;
                    if(isArray(value.Days)){
                        $.each(value.Days, function(iD,vD){
                            if(vD.Day==actualDay){
                                cT=true;
                                value.entryTimeLimit = vD.TimeLimit;
                                value.entryRemainingTime = vD.RemainingTime;
                            }
                        });
                    }
                    switch(cT){
                        case true:
                        value.entryTiWrClassLimit = "active";
                        value.entryTiWrClassNoLimit = "inactive";
                        break;
                        case false:
                        value.entryTiWrClassLimit = "inactive";
                        value.entryTiWrClassNoLimit = "active";
                        break;
                    }
                }      
                value.index = index;
                value.entryId = value.Mac;
                value.entryMacId = value.Mac;
                if(value.Name!=""){
                    value.entryStationName = value.Name;
                    value.entryStationNameClass = "active";
                }else{
                    value.entryStationName = "";
                    value.entryStationNameClass = "inactive";
                }
                if(value.New=="yes"){
                    // value.entryEditClass = "inactive";
                }
                $.template( "parEntriesTmpl", eM );
                $.tmpl ( "parEntriesTmpl", value ).appendTo("#divParEntries");
                c++;
                // console.log(value);
            });
            $("#parEntriesExText").html("");
        }else{
            $("#parEntriesExText").html("<~ trans `parental_no_stations_limit` ~>");
        }
    }
}
function addParental(t){
    var o = new Object();
    o.New = "yes";
    o.Mac = beautifyMac(t);
    o.Name = "";
    o.Active = "off";
    o.Days = new Array();
    oD = new Object();
    oD.Mac = t;
    oD.Day = "Monday";
    oD.Active = "off";
    oD.RemainingTime = "";
    oD.TimeLimit = "";
    o.Days.push(oD);
    parentallist.unshift(o);
    makeKnownStatList();
    makeConnList();
    renderParentalTable();
    showOnChangeMessage();
    changedForm = true;
}
function removeParentalEntry(i) {
    parentallist.splice(i,1);
    makeKnownStatList();
    makeConnList();
    renderParentalTable();
    showOnChangeMessage();
}
function goEdit(v){
    document.location.href=path_base+"&_dir=wireless&_page=parental_station&_macAddress="+v;
}
