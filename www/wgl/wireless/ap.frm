<!-- ######### unifiedRadio -->
<!-- unifiedRadios
Wireless.UnifiedRadios yes / no -->
<input type="hidden"
id="unifiedRadios"
name=":sys:Wireless.UnifiedRadios"
value="<~ if ${_errorcount} eq `0` then ${unifiedRadios} else ${unifiedRadios_post} ~>"
/>

<!-- ######### 2,4 Ghz -->
<!-- WLANRadio
Wireless.Radio[0].WLANRadio on / off -->
<input type="hidden"
id="wlanRadio0"
name=":sys:Wireless.Radio[0].WLANRadio"
value="<~ if ${_errorcount} eq `0` then ${wlanRadio0} else ${wlanRadio0_post} ~>"
/>

<!-- HideSSID
Wireless.Radio[0].AP[0].HideSSID on / off -->
<input type="hidden"
id="networkNameHide0"
class="radio0"
name=":sys:Wireless.Radio[0].AP[0].HideSSID"
value="<~ if ${_errorcount} eq `0` then ${networNameHide0} else ${networNameHide0_post} ~>"
/>

<!-- wpaMode
Wireless.Radio[0].AP[0].WPAMode WPA2 / both -->
<input type="hidden"
id="wpaMode0"
class="radio0" 
name=":sys:Wireless.Radio[0].AP[0].WPAMode"
value="<~ if ${_errorcount} eq `0` then ${wpaMode0} else ${wpaMode0_post} ~>"
/>

<!-- ######### 5 Ghz -->
<!-- WLANRadio
Wireless.Radio[1].WLANRadio on / off -->
<input type="hidden"
id="wlanRadio1"
name=":sys:Wireless.Radio[1].WLANRadio"
value="<~ if ${_errorcount} eq `0` then ${wlanRadio1} else ${wlanRadio1_post} ~>"
/>

<!-- HideSSID
Wireless.Radio[1].AP[0].HideSSID on / off -->
<input type="hidden"
id="networkNameHide1"
class="radio1" 
name=":sys:Wireless.Radio[1].AP[0].HideSSID"
value="<~ if ${_errorcount} eq `0` then ${networNameHide1} else ${networNameHide1_post} ~>"
/>

<!-- wpaMode
Wireless.Radio[1].AP[0].WPAMode WPA2 / both -->
<input type="hidden"
id="wpaMode1"
class="radio1" 
name=":sys:Wireless.Radio[1].AP[0].WPAMode"
value="<~ if ${_errorcount} eq `0` then ${wpaMode1} else ${wpaMode1_post} ~>"
/>

<input type="hidden"
name="_okdir"
value="wireless"
id="post_okdir" />

<input type="hidden"
name="_okpage"
value="home"
id="post_okpage" />