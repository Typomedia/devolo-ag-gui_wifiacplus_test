
var _errorcount = "${_errorcount}";

var wlanOn = "on";
var wlanRadio0 = "${wlanRadio0}";
var wlanRadio1 = "${wlanRadio1}";
if ( wlanRadio0 != "on" && wlanRadio1 != "on" ){
	wlanOn = "off";
}
var wpsOn = "${wpsOn}";
var wpsOnM = wpsOn;
var wpsOn_post = "${wpsOn_post}";
var wpsOn_err = "${err_wpsOn}";
if ( _errorcount != "0" && wpsOn_post != "" ){
	wpsOn = wpsOn_post;
}

var wpsOn1 = "${wpsOn1}";
var wpsOn1_post = "${wpsOn1_post}";
var wpsOn1_err = "${err_wpsOn1}";
if ( _errorcount != "0" && wpsOn1_post != "" ){
	wpsOn1 = wpsOn1_post;
}

var pbcRegistrar = "${pbcRegistrar_post}";
var pinRegistrar = "${pinRegistrar_post}";
var pinRegistrar_err = "${err_pinRegistrar}";

ifBubbleEnabled = "${ifBubbleEnabled}";
showBubbleDialog = "${showBubbleDialog}";
bubbleText = "${bubbleText}";

$( document ).ready(function() {
	$("#buttonApply").click(function(e){
		var iV = $('#tmp_wpsOnOff').prop('checked');
		switch ( iV ){
			case false:
				$("#wpsOn,#wpsOn1").val("no");
			break;
			case true:
				$("#wpsOn,#wpsOn1").val("yes");
			break;
		}
		if(wpsOnM == "no" && iV){
			$("#post_okdir").val("wireless");
        	$("#post_okpage").val("wps");
		}else{
			$("#post_okdir").val("wireless");
        	$("#post_okpage").val("home");
		}
		$("#form_post").submit();
		// $("#spinner").hide();	
		e.preventDefault();
	});
	$("#tmp_wpsOnOff").click(function(e){
		var iV = $('#tmp_wpsOnOff').prop('checked');
		switch ( iV ){
			case false:
				wpsSetOff();
			break;
			case true:
				wpsSetOn();
			break;
		}
	});
	$("#butWpsSoft").click(function(e){
		$("#post_okdir").val("spec");
        $("#post_okpage").val("result");
        $("#post_okfollowdir").val("status");
        $("#post_okfollowpage").val("wireless");
        $("#post_okplain").val("1");
        $("#post_oktype").val("wlanstatus");
		$("#post_pbc,#post_pbc1").attr("disabled",false);
		$("#post_pin,#post_pin1").attr("disabled",true);
		$("#wpsOn,#wpsOn1").attr("disabled",true);
		//$("#wpsOn,#wpsOn1").attr("disabled",false).val("yes");
		$("#spinner").show();
		$("#form_post").submit();
		// $("#spinner").hide();	
		e.preventDefault();
	});
    $("#butWpsPin").click(function(e){
    	v = $("#wpsPin").val();
    	if ( v.match(/^([0-9]{4}|[0-9]{8}|[0-9]{4}[\- ][0-9]{4})$/) ){
			$("#post_okdir").val("spec");
        	$("#post_okpage").val("result");
        	$("#post_okfollowdir").val("status");
        	$("#post_okfollowpage").val("wireless");
        	$("#post_okplain").val("1");
        	$("#post_oktype").val("wlanstatus");
			$("#post_pbc,#post_pbc1").attr("disabled",true);
			$("#post_pin,#post_pin1").attr("disabled",false).val($("#wpsPin").val());
			$("#wpsOn,#wpsOn1").attr("disabled",true);
			//$("#wpsOn,#wpsOn1").attr("disabled",false).val("yes");
			$("#spinner").show();
			$("#form_post").submit();
		}else{
			$("#spinner").hide();
			$("#errWpsPin").show();
		}
		// $("#spinner").hide();	
		e.preventDefault();
	});
	init();
});

function init(){
	setWpsMode();	
	setErrors();
}

/* wps mode */
function setWpsMode(){
	if ( wpsOn == "yes" || wpsOn1 == "yes" ){
		$("#tmp_wpsOnOff").prop("checked", true);
		toggleWpsMode("show");
	}else{
		toggleWpsMode("hide");
	}
}
function toggleWpsMode(v){
	switch (v){
		case "show":
		if(wpsOnM=="yes"){
			$("#divWps").removeClass("inactive").addClass("active");
		}
		break;
		case "hide":
		$("#divWps").removeClass("active").addClass("inactive");
		break;
	}
}
function wpsSetOff(){
	$("#wpsOn,#wpsOn1").val("no");
	toggleWpsMode("hide");
}
function wpsSetOn(){
	$("#wpsOn,#wpsOn1").val("yes");
	toggleWpsMode("show");
}

function setErrors(){
	if(pinRegistrar_err != ""){
		$("#err_wpsPinFormGroup").addClass("has-error");
		$("#err_wpsPinMessage").addClass("active");
	}
}

