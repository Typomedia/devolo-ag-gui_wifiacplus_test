<!-- GuestRestrictionActive
Wireless.GuestRestrictionActive yes / no -->
<input type="hidden"
    id="wlanGuRestAct"
    name=":sys:Wireless.GuestRestrictionActive:format=raw"
    class="guest"
    value="<~ if ${_errorcount} eq `0` then ${wlanGuRestAct} else ${wlanGuRestAct_post} ~>"
/>

<!-- Wireless.GuestTimeout
Wireless.GuestTimeout 0 / ... -->
<input type="hidden"
    id="wlanGuTimeOut"
    name=":sys:Wireless.GuestTimeout"
    disabled="disabled"
    class="guest"
    value="<~ if ${_errorcount} eq `0` then `0` else ${wlanGuTimeOut_post} ~>"
/>

<!-- ######### 2,4 Ghz -->
<!-- Active
Wireless.Radio[0].AP[1].Active on / off -->
<input type="hidden"
id="wlanRadio0"
class="guestActive"
name=":sys:Wireless.Radio[0].AP[1].Active"
value="<~ if ${_errorcount} eq `0` then ${wlanRadio0} else ${wlanRadio0_post} ~>"
/>

<!-- Security
Wireless.Radio[0].AP[1].Security WPA / none -->
<input type="hidden"
id="security0"
class="guest"
name=":sys:Wireless.Radio[0].AP[1].Security"
value="<~ if ${_errorcount} eq `0` then ${security0} else ${security0_post} ~>"
/>

<!-- ######### 5 Ghz -->
<!-- Active
Wireless.Radio[1].AP[1].Active on / off -->
<input type="hidden"
id="wlanRadio1"
class="guestActive"
name=":sys:Wireless.Radio[1].AP[1].Active"
value="<~ if ${_errorcount} eq `0` then ${wlanRadio1} else ${wlanRadio1_post} ~>"
/>

<!-- SSID
Wireless.Radio[1].AP[1].SSID .... -->
<input type="hidden"
id="ssid1"
class="guest"
name=":sys:Wireless.Radio[1].AP[1].SSID"
value="<~ if ${_errorcount} eq `0` then ${ssid1} else ${ssid1_post} ~>"
/>

<!-- Security
Wireless.Radio[1].AP[1].Security WPA / none -->
<input type="hidden"
id="security1"
class="guest"
name=":sys:Wireless.Radio[1].AP[1].Security"
value="<~ if ${_errorcount} eq `0` then ${security1} else ${security1_post} ~>"
/>

<!-- WPAKey
Wireless.Radio[1].AP[1].WPAKey ... -->
<input type="hidden"
id="wpaKey1"
class="guest"
name=":sys:Wireless.Radio[1].AP[1].WPAKey"
value="<~ if ${_errorcount} eq `0` then ${wpakey1} else ${wpakey1_post} ~>"
/>

<input type="hidden"
name="_okdir"
value="wireless"
id="post_okdir" />

<input type="hidden"
name="_okpage"
value="home"
id="post_okpage" />
