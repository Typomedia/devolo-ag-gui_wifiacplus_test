var _errorcount = "${_errorcount}";
var wtSchedActive = "${wtSchedActive}";
var wtSchedActive_post = "${wtSchedActive_post}";
var wtSchedActive_err = "${err_wtSchedActive}";
if (_errorcount != "0" && wtSchedActive_post != "") {
    wtSchedActive = wtSchedActive_post;
}
var wtSchedWait = "${wtSchedWait}";
var wtSchedWait_post = "${wtSchedWait_post}";
var wtSchedWait_err = "${err_wtSchedWait}";
if (_errorcount != "0" && wtSchedWait_post != "") {
    wtSchedWait = wtSchedWait_post;
}
var ntptTime = "${ntpTime}";
var ntptTimeValid = "${ntpTimeValid}";
var ntptTimeH1 = "${ntptTimeH1}";
var fromTrans = "${fromTrans}";
var toTrans = "${toTrans}";
ifBubbleEnabled = "${ifBubbleEnabled}";
showBubbleDialog = "${showBubbleDialog}";
bubbleText = "${bubbleText}";
var errTimeInvalid = "${errTimeInvalid}";
var errTimeBegin = "${errTimeBeginn}";
var errTimeOverlap = "${errTimeOverlap}";
var formValid;
var formValidAll;
var formValidSingle;
var onAll;
var offAll;
var onSingle;
var offSingle;
var uuid = 0;
var checkBut = false;
var timeTable = new Array();
timeTable[0] = new Object();
timeTable[0]['dayshort'] = 'mon';
timeTable[0]['daylong'] = 'Monday';
timeTable[0]['daytrans'] = '${day_mon}';
timeTable[0]['entries'] = new Array();
timeTable[1] = new Object();
timeTable[1]['dayshort'] = 'tue';
timeTable[1]['daylong'] = 'Tuesday';
timeTable[1]['daytrans'] = '${day_tue}';
timeTable[1]['entries'] = new Array();
timeTable[2] = new Object();
timeTable[2]['dayshort'] = 'wed';
timeTable[2]['daylong'] = 'Wednesday';
timeTable[2]['daytrans'] = '${day_wed}';
timeTable[2]['entries'] = new Array();
timeTable[3] = new Object();
timeTable[3]['dayshort'] = 'thu';
timeTable[3]['daylong'] = 'Thursday';
timeTable[3]['daytrans'] = '${day_thu}';
timeTable[3]['entries'] = new Array();
timeTable[4] = new Object();
timeTable[4]['dayshort'] = 'fri';
timeTable[4]['daylong'] = 'Friday';
timeTable[4]['daytrans'] = '${day_fri}';
timeTable[4]['entries'] = new Array();
timeTable[5] = new Object();
timeTable[5]['dayshort'] = 'sat';
timeTable[5]['daylong'] = 'Saturday';
timeTable[5]['daytrans'] = '${day_sat}';
timeTable[5]['entries'] = new Array();
timeTable[6] = new Object();
timeTable[6]['dayshort'] = 'sun';
timeTable[6]['daylong'] = 'Sunday';
timeTable[6]['daytrans'] = '${day_sun}';
timeTable[6]['entries'] = new Array();
var Entry;
var dM;
var eM;

$(document).ready(function() {

    // click on add
    $(document.body).on('click', '.timeTableAdd:not(.semiTransparent)', function() {
        var a = $(this).attr("id").split("_");
        addEmptyEntry(a[0], a[1], a[2], a[3], false);
    });
    // click on del
    $(document.body).on('click', '.timeTableDel:not(.semiTransparent)', function() {
        var a = $(this).attr("id").split("_");
        delEntry(a[0], a[1], a[2], a[3]);
    });
    // click on delAll
    $(document.body).on('click', '#dateDelAll:not(.semiTransparent)', function() {
        delEntryAll();
    });
    // click on addAll
    $(document.body).on('click', '#dateAddAll:not(.semiTransparent)', function() {
        addEntryAll();
    });
    // check field typing
    $(document).on('keyup keypress', '#dateOffAll,#dateOnAll', function(e) {
        if(e.type == "keypress"){
            e.preventDefault;
            // return false;
        }else{
            var comb = $(this).val();
            switch($(this).attr("id")){
                case "dateOffAll":
                    comb+=$("#dateOnAll").val();
                break;  
                case "dateOnAll":
                    comb+=$("#dateOffAll").val();
                break;
            }
            if(comb!=""){
                $("#dateDelAll,#dateAddAll").removeClass("semiTransparent semiTransparentCurs").attr("disabled", false);
            }else{
                $("#dateDelAll,#dateAddAll").addClass("semiTransparent semiTransparentCurs").attr("disabled", true);
            }
        }
    });
    $("#buttonApply").click(function(e) {
        switch (wtSchedActive) {
            case "on":
                $("#timeTableWildcard").prop("disabled", false);
                formValid = true;
                checkForm();
                if (formValid) {
                    fillTimeTable();
                    $("#form_post").submit();
                    // $("#spinner").hide();
                } else {
                    $("#spinner").hide();
                }
                break;
            case "off":
                $("#timeTableWildcard").prop("disabled", true);
                $("#form_post").submit();
                break;
        }
        // e.preventDefault();
    });
    $("#tmp_wlantimeOnOff").click(function(e) {
        var iV = $('#tmp_wlantimeOnOff').prop('checked');
        switch (iV) {
            case false:
                wtSchedActiveSetOff();
                break;
            case true:
                wtSchedActiveSetOn();
                break;
        }
    });
    $("#tmp_wtWaitOnOff").click(function(e) {
        var iV = $('#tmp_wtWaitOnOff').prop('checked');
        switch (iV) {
            case false:
                wtWaitOff();
                break;
            case true:
                wtWaitOn();
                break;
        }
    });
    setNtpt();
    init();
    renderTimeTable();
});

function setNtpt(){
    temp = eval("(<~ query `NTPClient.CurrentTime:format=json` json ~>)"); 
    if(jQuery.type(temp.CurrentTime) == "string"){
        ntptTime = temp.CurrentTime;
    }
    temp = eval("(<~ query `NTPClient.TimeIsValid:format=json` json ~>)");
    if(jQuery.type(temp.TimeIsValid) == "string"){
        ntptTimeValid = temp.TimeIsValid;
    } 
}

function init() {
    checkNTPT();
    tmplDayMarkup();
    tmplEntryMarkup();
    setWtSchedActiveMode();
    setWtWaitMode();
    getSavedEntries();
}

function checkNTPT() {
    if (ntptTime == "") {
        $("#headerHint").show().find("#headerHintText").html(ntptTimeH1);
    }
}
/* wtactive-mode */
function setWtSchedActiveMode() {
    if (wtSchedActive == "on") {
        $("#tmp_wlantimeOnOff").prop("checked", true);
        toggleWtSchedActiveMode("show");
    } else {
        toggleWtSchedActiveMode("hide");
    }
}

function toggleWtSchedActiveMode(v) {
    switch (v) {
        case "show":
            toggleWtWaitMode("show");
            $("#timeTableDiv").removeClass("inactive").addClass("active");
            break;
        case "hide":
            toggleWtWaitMode("hide");
            $("#timeTableDiv").removeClass("active").addClass("inactive");
            break;
    }
}

function wtSchedActiveSetOff() {
    $("#wtSchedActive").val("off");
    toggleWtSchedActiveMode("hide");
    wtSchedActive = "off";
}

function wtSchedActiveSetOn() {
    $("#wtSchedActive").val("on");
    toggleWtSchedActiveMode("show");
    wtSchedActive = "on";
}
/* wtwait */
function setWtWaitMode() {
    if (wtSchedWait == "yes") {
        $("#tmp_wtWaitOnOff").prop("checked", true);
    } else {
        $("#tmp_wtWaitOnOff").prop("checked", false);
    }
}

function toggleWtWaitMode(v) {
    switch (v) {
        case "show":
            $("#wtWait").removeClass("inactive").addClass("active");
            break;
        case "hide":
            $("#wtWait").removeClass("active").addClass("inactive");
            break;
    }
}

function wtWaitOff() {
    $("#wtSchedWait").val("no");
}

function wtWaitOn() {
    $("#wtSchedWait").val("yes");
}

function getSavedEntries() {
    savedEntries = eval("(<~ query `Wireless.Schedule.Day{*}:format=json` json ~>)");
    var c = 0;
    if (isArray(savedEntries.Day)) {
        $.each(savedEntries.Day, function(i, v) {
            if (isArray(v.Entry)) {
                $.each(v.Entry, function(ie, ve) {
                    timeTable[c]['entries'].push(ve);
                });
            } else if (isObject(v.Entry)) {
                if (v.Entry.Index) {
                    timeTable[c]['entries'].push(v.Entry);
                }
            }
            c++;
        });
    }
    // console.log(savedEntries);
    // console.log(timeTable);
}

function getEntriesPost() {}

function fillTimeTable() {
    $("#timeTableWildcard").val("");
    $("input[id^='timeTablePost']").remove();
    $.each(timeTable, function(i, v) {
        var vT = v;
        var c = 0;
        var cc = 0;
        var jS = "";
        var addTime = new Object();
        addTime.Day = new Object();
        addTime.Day.Day = "";
        $("div[id^='timeTableEntry_" + i + "']").each(function(i, v) {
            cc++;
            if (c == 0) {
                addTime.Day.Day = vT.daylong;
                addTime.Day.Entry = new Array();
            }
            $on = $(this).find(".maskDateOn").val();
            $off = $(this).find(".maskDateOff").val();
            if ($on != "" && $off != "") {
                $on = unifyTimeFormat($on);
                $off = unifyTimeFormat($off);
                c++;
                var addEntry = new Object();
                addEntry.Index = c.toString();
                addEntry.On = $on;
                addEntry.Off = $off;
                addTime.Day.Entry.push(addEntry);
            }
        });
        $cl = $("#timeTableWildcard").clone();
        $n = $cl.attr("name");
        $n = $n.replace("_WILDCARD_DAY_", vT.daylong);
        $n = $n.replace("_WILDCARD_INDEX_", c);
        $cl.attr("name", $n);
        $cl.attr("id", "timeTablePost" + cc);
        $cl.val(JSON.stringify(addTime));
        $("#form_post").append($cl);
    });
}

function tmplDayMarkup() {
    dM = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
    dM += '<div class="row">';
    dM += '<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8"><hr></div>';
    dM += '</div>';
    dM += '<div class="row">';
    dM += '<div class="row" id="row$' + '{dayshort}">';
    dM += '<p class="caption-type-2">$' + '{daytrans}</p>';
    dM += '</div>';
    dM += '</div>';
    dM += '</div>';
}

function tmplEntryMarkup() {
    eM = '<div id="$' + '{entryId}" class="row">';
    eM += ' <div class="col-xs-3 col-sm-4 col-md-2 col-lg-2">';
    eM += '  <div class="form-group">';
    eM += '   <input type="text" class="form-control caption-type-2 maskDateOn" value="$' + '{On}" placeholder="'+fromTrans+'">';
    eM += '  </div>';
    eM += ' </div>';
    eM += ' <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    eM += '  <p class="component-title text-center">&nbsp;</p>';
    eM += ' </div>';
    eM += ' <div class="col-xs-3 col-sm-4 col-md-2 col-lg-2">';
    eM += '  <div class="form-group">';
    eM += '   <input type="text" class="form-control caption-type-2 maskDateOff" value="$' + '{Off}" placeholder="'+toTrans+'">';
    eM += '  </div>';
    eM += ' </div>';
    eM += ' <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">';
    eM += '  <p class="component-title">&nbsp;</p>';
    eM += ' </div>';
    eM += ' <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">';
    eM += '  <div class="button-add timeTableDel" id="$' + '{delId}">';
    eM += '   <a href="javascript:void(0);">';
    eM += '    <div class="icon- button-delete-icon">l</div>';
    eM += '   </a>';
    eM += '     </div>';
    eM += ' </div>';
    eM += ' <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">';
    eM += '     <div class="button-add timeTableAdd $' + '{addClass}" id="$' + '{addId}">';
    eM += '   <a href="javascript:void(0);">';
    eM += '    <div class="icon- button-add-icon">b</div>';
    eM += '   </a>';
    eM += '  </div>';
    eM += ' </div>';
    eM += ' <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 error-message-box">';
    eM += '         <p class="text-error" id="$' + '{errorId}"></p>';
    eM += ' </div>';
    eM += '</div>';
}

function renderTimeTable() {
    $("#timeTable").html("");
    $.template("timeTableTmpl", dM);
    $.tmpl("timeTableTmpl", timeTable).appendTo("#timeTable");
    // console.log(timeTable);
    $.each(timeTable, function(index, value) {
        var c = 0;
        var cE = value.entries.length;
        var hA = false;
        if (cE > 1) {
            hA = true;
        }
        value.entries.sort(entrySort);
        $.each(value.entries, function(indexe, valuee) {
            if (hA == true && c < cE - 1) {
                valuee.addClass = "inactive";
            } else {
                valuee.addClass = "";
            }
            valuee.entryId = "timeTableEntry_" + index + "_" + value.dayshort + "_" + uuid;
            valuee.delId = "timeTableDel_" + index + "_" + value.dayshort + "_" + uuid;
            valuee.addId = "timeTableAdd_" + index + "_" + value.dayshort + "_" + uuid;
            valuee.errorId = "timeTableErr_" + index + "_" + value.dayshort + "_" + uuid;
            $.template("entryTmpl", eM);
            $.tmpl("entryTmpl", valuee).appendTo("#row" + value.dayshort);
            uuid++;
            c++;
        });
        if (c == 0) {
            addEmptyEntry("timeTableEntry", index, value.dayshort, uuid, true);
        }
    });
    checkBut = true;
    checkBuFormAddState();
}

function entrySort(a, b) {
    var aOn = a.On;
    var bOn = b.On;
    return ((aOn < bOn) ? -1 : ((aOn > bOn) ? 1 : 0));
}

function addEmptyEntry(a, b, c, d, e) {
    switch (e) {
        case true:
            addEmptyForm(a, b, c, d);
            checkBuForm(a, b, c, d);
            break;
        default:
            // add via button
            checkFormSingle(a, b, c, d);
            if (formValidSingle) {
                addEmptyForm(a, b, c, d);
                checkBuForm(a, b, c, d);
            }
            break;
    }
}

function addEmptyForm(a, b, c, d) {
    uuid++;
    Entry = new Object();
    Entry.Index = new Object();
    Entry.On = "";
    Entry.Off = "";
    Entry.entryId = "timeTableEntry_" + b + "_" + c + "_" + uuid;
    Entry.delId = "timeTableDel_" + b + "_" + c + "_" + uuid;
    Entry.addId = "timeTableAdd_" + b + "_" + c + "_" + uuid;
    Entry.errorId = "timeTableErr_" + b + "_" + c + "_" + uuid;
    $.template("entryTmpl", eM);
    $.tmpl("entryTmpl", Entry).appendTo("#row" + c);
}

function addEntryAll() {
    checkFormAll();
    if (formValidAll) {
        // loop all days
        $.each(timeTable, function(index, value) {
            // find last empty entry for each day
            $lE = $("div[id^='timeTableEntry_" + index + "']:last");
            $aR = $lE.attr("id").split("_");
            $onI = $lE.find(".maskDateOn");
            $offI = $lE.find(".maskDateOff");
            if ($onI.val() == "" && $offI.val() == "") {
                $onI.val(onAll);
                $offI.val(offAll);
            } else {
                // if not available, add empty entry and fill
                addEmptyEntry($aR[0], $aR[1], $aR[2], $aR[3], true);
                $("#" + Entry.entryId).find(".maskDateOn").val(onAll);
                $("#" + Entry.entryId).find(".maskDateOff").val(offAll);
            }
        });
        showOnChangeMessage();
        checkForm();
        delEntryAll();
    }
}

function delEntry(a, b, c, d) {
    showOnChangeMessage();
    delForm(a, b, c, d);
    checkForm();
}

function delEntryAll() {
    $tA = $("#timeTableAll");
    $("#dateOnAll,#dateOffAll").val("");
    releaseErrorText($tA);
    releaseHasError($tA, true, true);
    disableEntryAllBut();
}

function disableEntryAllBut(){
    $("#dateDelAll,#dateAddAll").addClass("semiTransparent semiTransparentCurs").attr("disabled", true);
}

function delForm(a, b, c, d) {
    var l = $("div[id^='timeTableEntry_" + b + "']").length;
    if (l == 1) {
        addEmptyForm(a, b, c, d);
    }
    $("#timeTableEntry_" + b + "_" + c + "_" + d).remove();
    checkBuForm(a, b, c, d);
}

function checkBuFormAddState(){
    arr=["0","1","2","3","4","5","6"];
    $.each(arr, function(index,value){
        co = $("div[id^='timeTableEntry_" + value + "']").length;
        // console.log(index,co);
        if(co==1){
            $("div[id^='timeTableEntry_" + value + "']:first").find(".timeTableDel").addClass("semiTransparent semiTransparentCurs");
        }else{
            $("div[id^='timeTableEntry_" + value + "']:first").find(".timeTableDel").removeClass("semiTransparent");
        }
        // console.log(index,co);
    });
}

function checkBuForm(a, b, c, d) {
    $("div[id^='timeTableEntry_" + b + "']:not(:last)").find(".timeTableAdd").hide();
    $("div[id^='timeTableEntry_" + b + "']:last").find(".timeTableAdd").show();
    if(checkBut){
        checkBuFormAddState();
    }
}

function checkForm() {
    $("div[id^='timeTableEntry']").each(function(index) {
        var on = $(this).find(".maskDateOn").val();
        var off = $(this).find(".maskDateOff").val();
        var c = checkEntry($(this), on, off);
        formValid = formValid && c;
    });
}

function checkFormSingle(a, b, c, d) {
    $sE = $("#timeTableEntry_" + b + "_" + c + "_" + d);
    onSingle = $sE.find(".maskDateOn").val();
    offSingle = $sE.find(".maskDateOff").val();
    if (onSingle == "") {
        onSingle = "false";
    }
    if (offSingle == "") {
        offSingle = "false";
    }
    // console.log(onSingle,offSingle);
    var c = checkEntry($sE, onSingle, offSingle);
    formValidSingle = c;
}

function checkFormAll() {
    $tA = $("#timeTableAll");
    onAll = $tA.find("#dateOnAll").val();
    offAll = $tA.find("#dateOffAll").val();
    if (onAll == "") {
        onAll = "false";
    }
    if (offAll == "") {
        offAll = "false";
    }
    var c = checkEntry($tA, onAll, offAll);
    formValidAll = c;
}

function unifyTimeFormat(v) {
    if (v != "") {
        var a = v.split(":");
        if (a.length == 2) {
            if (a[0].length == 1) {
                v = "0" + a[0] + ":" + a[1];
            }
        }
    }
    return v;
}

function checkEntry(a, b, c) {
    retvalEntry = true;
    dontCheck = false;
    if (b == "" && c == "") {
        releaseHasError(a, true, true);
        releaseErrorText(a);
        dontCheck = true;
        // return true;
    } else {
        b = unifyTimeFormat(b);
        c = unifyTimeFormat(c);
        eB = errReg['timeValid'].exec(b);
        eC = errReg['timeValid'].exec(c);
        if (eB && eC) {
            if (b >= c) {
                setErrorText(a, errTimeBegin);
                var c = setHasError(a, b, c);
                retvalEntry = retvalEntry && false;
            }else{
                releaseHasError(a, true, true);
                releaseErrorText(a);
                retvalEntry = retvalEntry && true;
            }
        } else {
            if (!eB) {
                setErrorText(a, errTimeInvalid);
                var ret = setHasError(a, b, false);
                retvalEntry = retvalEntry && false;
            } else {
                releaseHasError(a, b, false);
                retvalEntry = retvalEntry && true;
            }
            if (!eC) {
                setErrorText(a, errTimeInvalid);
                var ret = setHasError(a, false, c);
                retvalEntry = retvalEntry && false;
            } else {
                releaseHasError(a, false, c);
                retvalEntry = retvalEntry && true;
            }
        }
    }
    // console.log("retvalEntry", retvalEntry);
    if ( retvalEntry && !dontCheck ){
        // check overlap
        arr = checkEntryOverlap([], a, b, c);
        if(arr[0] && arr[1]){
            releaseHasError(a, b, false);
            retvalEntry = retvalEntry && true;
        }else{
            if (!arr[0]) {
                setErrorText(a, errTimeOverlap);
                var ret = setHasError(a, b, false);
            } else {
                releaseHasError(a, b, false);
            }
            if (!arr[1]) {
                setErrorText(a, errTimeOverlap);
                var ret = setHasError(a, false, c);
            } else {
                releaseHasError(a, false, c);
            }
            retvalEntry = retvalEntry && false;
        }
    }
    return retvalEntry;
}

function checkEntryOverlap(a, i, on, off) {
    if(jQuery.type(a) != "array"){
        a=["0","1","2","3","4","5","6"];
    }
    $arrDay = a;
    $iE = false;
    if (i){
        $elTe = i;
        $elTeId = $elTe.attr("id");
        $arrTe = $elTeId.split("_");
        $arrDay = [$arrTe[1]];
        $iE = true;
    }
    $on = toNumberTime(on);
    $off = toNumberTime(off);
    $retOl = [true,true];
    if (jQuery.type($arrDay) == "array") {
        $.each($arrDay, function(index, value) {
            $("div[id^='timeTableEntry_" + value + "']").each(function(indexTe, valueTe) {
                $i = $(this).attr("id");
                if($i != $elTeId){
                    $onI = toNumberTime($(this).find(".maskDateOn").val());
                    $offI = toNumberTime($(this).find(".maskDateOff").val());
                    $retOl = numberInRange($on,$off,$onI,$offI);
                    // console.log($retOl);
                    if($retOl[0] == false || $retOl[1] == false){
                        return false;
                    }
                }
            });
        });
    }
    return $retOl;
}

function toNumberTime(t){
    // console.log(t);
    n = t.toString().replace(":","");
    return parseInt(n);
}

function numberInRange(a,b,c,d){
    arr = [];
    arr[0] = !(a >= c && a <= d);
    arr[1] = !(b >= c && b <= d);
    return arr;
}

function releaseErrorText(a) {
    $c = a.find(".error-message-box");
    $c.hide();
    $c.find(".text-error").html('');
}

function setErrorText(a, b) {
    $c = a.find(".error-message-box");
    $c.show();
    $c.find(".text-error").html(b);
}

function setHasError(a, b, c) {
    if (b && c) {
        a.find(".form-group").addClass("has-error");
        return true;
    }
    if (b) {
        a.find(".maskDateOn").parent().addClass("has-error");
        return true;
    }
    if (c) {
        a.find(".maskDateOff").parent().addClass("has-error");
        return true;
    }
}

function releaseHasError(a, b, c) {
    if (b && c) {
        a.find(".form-group").removeClass("has-error");
        return true;
    }
    if (b) {
        a.find(".maskDateOn").parent().removeClass("has-error");
        return true;
    }
    if (c) {
        a.find(".maskDateOff").parent().removeClass("has-error");
        return true;
    }
}