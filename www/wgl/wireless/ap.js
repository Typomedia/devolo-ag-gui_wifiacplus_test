
var _errorcount = "${_errorcount}";

/* radiomode */
var radioMode = "${radioMode}";
var radioMode_post = "${radioMode_post}";
var err_radioMode = "${err_radioMode}";
if ( err_radioMode != '' || radioMode_post != '' ){
	radioMode = radioMode_post;
}

/* unified */
var unifiedRadios = "${unifiedRadios}";
var unifiedTitleSSID = "<~ trans `wireless_ssid_2_4_5GHz` ~>";
var notUnifiedTitleSSID = "<~ trans `wireless_ssid_2_4GHz` ~>";
var unifiedTitleChannel1 = "<~ trans `radio_wireless_2_4GHz` ~>";
var unifiedTitleChannel2 = "<~ trans `radio_wireless_5GHz` ~>";

/* wlanradio */
var wlanRadio0 = "${wlanRadio0}";
var wlanRadio0_post = "${wlanRadio0_post}";
var wlanRadio1 = "${wlanRadio1}";
var wlanRadio1_post = "${wlanRadio1_post}";

var showBubbleDialog = "${showBubbleDialog}";
var bubbleText = "${bubbleText}";

/* networkname hide/show */
var networkNameHide0_ch;
var networkNameHide1_ch;
var networkNameHide0 = "${networkNameHide0}";
var networkNameHide0_post = "${networkNameHide0_post}";
if ( !networkNameHide0 && !networkNameHide0_post ){
	networkNameHide0 = "off";
	networkNameHide0_ch = false;
}
var networkNameHide1 = "${networkNameHide1}";
var networkNameHide1_post = "${networkNameHide1_post}";
if ( !networkNameHide1 && !networkNameHide1_post ){
	networkNameHide1 = "off";
	networkNameHide1_ch = false;
}

/* channel */
var channel0 = "${channel0}";
var channel0_post = "${channel0_post}";
if ( !channel0 && !channel0_post ){
	channel0 = "0";
}
var channel1 = "${channel1}";
var channel1_post = "${channel1_post}";
if ( !channel1 && !channel1_post ){
	channel1 = "0";
}

/* errors */
var err_ssid0 = "${err_ssid0}";
var err_ssid1 = "${err_ssid1}";
var err_wpaKey0 = "${err_wpaKey0}";
var err_wpaKey1 = "${err_wpaKey1}";
var err_wpaMode0 = "${err_wpaMode0}";
var err_wpaMode1 = "${err_wpaMode1}";

var formValid;
var errMess ={
    charInvalid: "<~ trans `misc_chars_allowed_text` ~>: <~ trans `misc_chars_allowed_symbols` ~>",
    toShortSSID: "<~ trans `error_wireless_ssid_min_length` ~>",
    toLongSSID: "<~ trans `error_wireless_ssid_max_length` ~>",
    toShortWpaKey: "<~ trans `error_wireless_key_min_length` ~>",
    toLongWpaKey: "<~ trans `error_wireless_key_max_length` ~>"
}

/* security */
var security0 = "${security0}";
var security0_post = "${security0_post}";
if ( !security0 && !security0_post ){
	security0 = "WPA";
}
var security1 = "${security1}";
var security1_post = "${security1_post}";
if ( !security1 && !security1_post ){
	security1 = "WPA";
}

/* wpamode */
var wpaMode0 = "${wpaMode0}";
var wpaMode0_post = "${wpaMode0_post}";
if ( !wpaMode0 && !wpaMode0_post ){
	wpaMode0 = "WPA";
}
var wpaMode1 = "${wpaMode1}";
var wpaMode1_post = "${wpaMode1_post}";
if ( !wpaMode1 && !wpaMode1_post ){
	wpaMode1 = "WPA";
}

var secCheck = true;
var secCheckMes = "";
var secCheckMes0 = "<~ trans `wireless_ssid_2_4GHz_error` ~>";
var secCheckMes1 = "<~ trans `wireless_ssid_5GHz_error` ~>";
var secCheckMes2 = "<~ trans `wireless_ssid_2_4_5GHz_error` ~>";

$( document ).ready(function() {
	$("#buttonApply").click(function(e){
		var iV = $('#tmp_wifiOnOff').prop('checked');
		switch ( iV ){
			case false:
				$("#wlanRadio1,#wlanRadio0").val("off");
			break;
			case true:
				$("#wlanRadio1,#wlanRadio0").val("on");
			break;
		}
		checkForm();
		if(formValid){
			// disable unneeded inputs
			switch(iV){
				case false:
					disableInputs("radio0");
					disableInputs("radio1");
				break;
				case true:
					switch(radioMode){
						case "radio0":
						disableInputs("radio1");
						break;
						case "radio1":
						disableInputs("radio0");
						break;
					}
				break;
			}
			// if unifiedradios and radiomode, clone 2,4 ghz to 5 ghz fields
			if(unifiedRadios == "yes" && radioMode == "all"){
				cloneUnified();
			}	
			$("#form_post").submit();
		}else{
			$("#spinner").hide();
		}	
		e.preventDefault();
	});
	$("#tmp_wifiOnOff").click(function(e){
		var iV = $('#tmp_wifiOnOff').prop('checked');
		switch ( iV ){
			case false:
				wifiSetOff();
			break;
			case true:
				wifiSetOn();
			break;
		}
	});
	$(".radiomode").click(function(e){
		radioMode = $(this).val();
		setRadioMode();
	});
	$("#tmp_unifiedRadiosOnOff").click(function(e){
		var iV = $('#tmp_unifiedRadiosOnOff').prop('checked');
		switch ( iV ){
			case false:
				unifiedRadiosSetOff();
			break;
			case true:
				unifiedRadiosSetOn();
			break;
		}
	});
	$(".networkNameHide").click(function(e){
		var i = this.id;
		var aS = i.split("_");
		var iV = $(this).prop('checked');
		iV == true ? iV = "on" : iV = "off";
		clickNetworkNameHide(iV,aS[1]);
	});
	$(".securityDiv").find(":input").click(function(e){
		$id = this.id;
		$a = $id.split("_");
		$s = $a[1];
		$n = $a[0].replace("security", "");
		switch ($s){
			case "none":
			disablePassword($n);
			break;
			case "WPA2":
			enablePassword($n);
			if($(this).hasClass("forRadio0")){
				$("#wpaMode0").val("WPA2");
			}else{
				$("#wpaMode1").val("WPA2");
			}
			break;
			case "both":
			if($(this).hasClass("forRadio0")){
				$("#wpaMode0").val("both");
			}else{
				$("#wpaMode1").val("both");
			}
			enablePassword($n);
			break;
		}
	});
	init();
});

function init(){
	setWifiMode();
	initRadioMode();
	setUnifiedRadios();
	setNetworkNameHide();
	// fillChannel();
	setChannel();
	setEncryption();
	setErrors();
}

/* errors */
function setErrors(){
	if(err_wpaKey0 != ""){
		$("#err_wpaKey0FormGroup").addClass("has-error");
		$("#err_wpaKey0Message").addClass("active");
	}
	if(err_wpaKey1 != ""){
		$("#err_wpaKey1FormGroup").addClass("has-error");
		$("#err_wpaKey1Message").addClass("active");
	}
	if(err_ssid0 != ""){
		$("#err_ssid0FormGroup").addClass("has-error");
		$("#err_ssid0Message").addClass("active");
	}
	if(err_ssid1 != ""){
		$("#err_ssid1FormGroup").addClass("has-error");
		$("#err_ssid1Message").addClass("active");
	}
}

// disabling ununsed frequency inputs
function disableInputs(a){
	$("."+a).find("input").each(function(){
		$(this).attr("disabled", true);
	});
}

/* radio mode */
function initRadioMode(){
	$("#radiomode_"+radioMode).prop("checked", true);
	toggleRadioMode();
}
function setRadioMode(i){
	toggleRadioMode();
}
function toggleRadioMode(){
	switch (radioMode){
		case "all":
		$("#divUnifiedRadios").removeClass("inactive").addClass("active");
		if(unifiedRadios=="yes"){
			$("#div_radiomode_radio1").removeClass("active").addClass("inactive");
			$("#div_radiomode_radio0").removeClass("inactive").addClass("active");
			$("#unifiedTitleSSID").html(unifiedTitleSSID);
			$("#unifiedTitleChannel1").html(unifiedTitleChannel1);
			$("#unifiedTitleChannel2").html(unifiedTitleChannel2);
			$("#divUnifiedChannel5GHz").removeClass("inactive").addClass("active");
			$("#divider-column").removeClass("active").addClass("inactive");
		}else{
			$("div[id^='div_radiomode_']").removeClass("inactive").addClass("active");
			$("#unifiedTitleSSID").html(notUnifiedTitleSSID);
			$("#unifiedTitleChannel1").html("");
			$("#divUnifiedChannel5GHz").removeClass("active").addClass("inactive");
			$("#divider-column").removeClass("inactive").addClass("active");
		}
		// $("#divider-column").removeClass("inactive").addClass("active");
		break;
		case "radio0":
		$("#divUnifiedRadios").removeClass("active").addClass("inactive");
		$("#unifiedTitleSSID").html(notUnifiedTitleSSID);
		$("#unifiedTitleChannel1").html("");
		$("#divUnifiedChannel5GHz").removeClass("active").addClass("inactive");
		$("#div_radiomode_radio1").removeClass("active").addClass("inactive");
		$("#div_radiomode_radio0").addClass("active").removeClass("inactive");
		$("#divider-column").removeClass("active").addClass("inactive");
		break;
		case "radio1":
		$("#divUnifiedRadios").removeClass("active").addClass("inactive");
		$("#div_radiomode_radio0").removeClass("active").addClass("inactive");
		$("#div_radiomode_radio1").addClass("active").removeClass("inactive");
		$("#unifiedTitleChannel2").html("");
		$("#divider-column").removeClass("active").addClass("inactive");
		break;
	}
}

// unified radios
function setUnifiedRadios(){
	if ( unifiedRadios == "yes"){
		$("#tmp_unifiedRadiosOnOff").prop("checked", true);
	}
}
/*
function toggleUnifiedRadios(v){
	switch (v){
		case "show":
		$("#div_radiomode_radio1").removeClass("active").addClass("inactive");
		break;
		case "hide":
		$("#div_radiomode_radio1").removeClass("inactive").addClass("active");
		break;
	}
}
*/
function unifiedRadiosSetOff(){
	$("#unifiedRadios").val("no");
	unifiedRadios = "no";
	toggleRadioMode();
}
function unifiedRadiosSetOn(){
	$("#unifiedRadios").val("yes");
	unifiedRadios = "yes";
	toggleRadioMode();
}
function cloneUnified(){
	$("#ssid1").val($("#ssid0").val());
	$("#channel1").val($("#channel1unified").val());
	$("#networkNameHide1").val($("#networkNameHide0").val());
	if($("#security0_WPA2").is(":checked")){
		$("#security1_WPA2").prop("checked", true);
	}else if($("#security0_both").is(":checked")){
		$("#security1_both").prop("checked", true);
	}else{
		$("#security1_none").prop("checked", true);
	}
	$("#wpaMode1").val($("#wpaMode0").val());
	/*
	if($("#security0_WPA").is(":checked")){
		$("#security1_WPA").prop("checked", true);
	}else{
		$("#security1_none").prop("checked", true);
	}
	*/
	$("#wpaKey1").val($("#wpaKey0").val());
	if($("#wpaKey0").is(":disabled")){
		$("#wpaKey1").prop("disabled",true);
	}else{
		$("#wpaKey1").prop("disabled",false);
	}
}

/* wifi mode */
function setWifiMode(){
	if ( wlanRadio0 == "on" || wlanRadio1 == "on" ){
		$("#tmp_wifiOnOff").prop("checked", true);
		toggleWifiMode("show");
	}else{
		toggleWifiMode("hide");
	}
}
function toggleWifiMode(v){
	switch (v){
		case "show":
		$("#divWlanRadios").removeClass("inactive").addClass("active");
		break;
		case "hide":
		$("#divWlanRadios").removeClass("active").addClass("inactive");
		break;
	}
}
function wifiSetOff(){
	$("#wlanRadio1,#wlanRadio0").val("off");
	toggleWifiMode("hide");
}
function wifiSetOn(){
	$("#wlanRadio1,#wlanRadio0").val("on");
	toggleWifiMode("show");
}

/* networknamehide */
function setNetworkNameHide(){
	var nN0;
	var nN1;
	switch ( _errorcount ){
		case "0":
		if ( networkNameHide0 == "on" ){
			networkNameHide0_ch = true;
		}else{
			networkNameHide0_ch = false;
		}
		if ( networkNameHide1 == "on" ){
			networkNameHide1_ch = true;
		}else{
			networkNameHide1_ch = false;
		}
		nN0 = networkNameHide0;
		nN1 = networkNameHide1;
		break;
		default:
		if ( networkNameHide0_post == "on" ){
			networkNameHide0_ch = true;
		}else{
			networkNameHide0_ch = false;
		}
		if ( networkNameHide1_post == "on" ){
			networkNameHide1_ch = true;
		}else{
			networkNameHide1_ch = false;
		}
		nN0 = networkNameHide0_post;
		nN1 = networkNameHide1_post;
		break;
	}
	$("#tmp_networkNameHide0").val(nN0);
	$("#tmp_networkNameHide0").prop("checked", networkNameHide0_ch);
	$("#networkNameHide0").val(nN0);
	$("#tmp_networkNameHide1").val(nN1);
	$("#tmp_networkNameHide1").prop("checked", networkNameHide1_ch);
	$("#networkNameHide1").val(nN1);
}
function clickNetworkNameHide(v,i){
	$("#"+i).val(v);
}

/* channel */
function fillChannel(){
	var sT = "";
	sT+="<option value='0'><~ trans `wireless_auto` ~></option>";
	for(i=1;i<=140;i++){
		sT+="<option value='"+i+"'>"+i+"</option>";
	}
	$("#channel0,#channel1").html(sT);
}
function setChannel(){
	switch ( _errorcount ){
		case "0":
		$("#channel0 option[value='"+channel0+"']").prop("selected", true);
		$("#channel1 option[value='"+channel1+"']").prop("selected", true);
		$("#channel1unified option[value='"+channel1+"']").prop("selected", true);
		break;
		default:
		$("#channel0 option[value='"+channel0_post+"']").prop("selected", true);
		$("#channel1 option[value='"+channel1_post+"']").prop("selected", true);
		$("#channel1unified option[value='"+channel1_post+"']").prop("selected", true);
		break;
	}
}

// encrpytion mode 
function setEncryption(){
	switch ( _errorcount ){
		case "0":
		if(wpaMode0 == "WPA2" && security0 != "none"){
			$("#security0_WPA2").prop("checked", true);
		}else if(wpaMode0 == "both" && security0 != "none"){
			$("#security0_both").prop("checked", true);
		}else{
			$("#security0_none").prop("checked", true);
		}
		if(wpaMode1 == "WPA2" && security1 != "none"){
			$("#security1_WPA2").prop("checked", true);
		}else if(wpaMode1 == "both" && security1 != "none"){
			$("#security1_both").prop("checked", true);
		}else{
			$("#security1_none").prop("checked", true);
		}
		break;
		default:
		if(wpaMode0_post == "WPA2" && security0 != "none"){
			$("#security0_WPA2").prop("checked", true);
		}else if(wpaMode0_post == "both" && security0 != "none"){
			$("#security0_both").prop("checked", true);
		}else{
			$("#security0_none").prop("checked", true);
		}
		if(wpaMode1_post == "WPA2" && security1 != "none"){
			$("#security1_WPA2").prop("checked", true);
		}else if(wpaMode1_post == "both" && security1 != "none"){
			$("#security1_both").prop("checked", true);
		}else{
			$("#security1_none").prop("checked", true);
		}
		break;
	}
	if ( security0 == "none" ){
		disablePassword("0");
		if(wlanRadio0 == "on"){
			secCheck = false;
			if(unifiedRadios != "yes"){
				secCheckMes+="<p>"+secCheckMes0+"</p>";
			}else{
				secCheckMes="<p>"+secCheckMes2+"</p>";
			}
		}
	}
	if ( security1 == "none"){
		disablePassword("1");
		if(wlanRadio1 == "on"){
			secCheck = false;
			if(unifiedRadios != "yes"){
				secCheckMes+="<p>"+secCheckMes1+"</p>";
			}
		}
	}
	if(!secCheck){
		// message no encrpytion
		$("#headerHint").removeClass("inactive").addClass("active");
		$("#headerHintText").html(secCheckMes);
	}
}
function disablePassword(v){
	$("#wpaKey"+v+"div").addClass("semiTransparent");
	$("#wpaKey"+v).attr("disabled", true);
}
function enablePassword(v){
	$("#wpaKey"+v+"div").removeClass("semiTransparent");
	$("#wpaKey"+v).attr("disabled", false);
}

// check form 
function checkForm(){
	formValid = true;
	var obj = $("#form_post");
	obj.find("input[data-ifValidation='true']:enabled").each(function () {
            if($(this).is(":visible")){
                eM = $(this).data("errormessageid");
                
                $(this).closest(".form-group").removeClass("has-error");
                $("#"+eM).removeClass("active");

                var validation = $(this).data("validation") ? $(this).data("validation") : "charValid";
                var validationEK = $(this).data("validationerrorkey") ? $(this).data("validationerrorkey") : "charInvalid";
                var minLength = $(this).data("minlength") ? $(this).data("minlength") : -1;
                var minLengthEK = $(this).data("minlengtherrorkey") ? $(this).data("minlengtherrorkey") : -1;
                var maxLength = $(this).data("maxlength") ? $(this).data("maxlength") : -1;
                var maxLengthEK = $(this).data("maxlengtherrorkey") ? $(this).data("maxlengtherrorkey") : -1;
                var errorMId = eM ? eM : -1;
                var userInput = $(this).val();

			    if(minLength > userInput.length) { 
			 	    showError(this, errorMId, errMess[minLengthEK]); 
			 	    formValid = false;
			 	    return true;
			 	} else if(maxLength < userInput.length) { 
			 	    showError(this, errorMId, errMess[maxLengthEK]); 
			 	    formValid = false;
			 	    return true;
			    }else if(errReg[validation] && !errReg[validation].test(userInput)) {
			 	    showError(this, errorMId, errMess[validationEK]); 
			 	    formValid = false;
			 	    return true;
			    } else { // no validation errors
			 	    return true; 
			    }
            }
	});
}
function showError(a,b,c){
	$(a).closest(".form-group").addClass("has-error");
	$("#"+b).addClass("active").find(".text-error").html(c);
}
