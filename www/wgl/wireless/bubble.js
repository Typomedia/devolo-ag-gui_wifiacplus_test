
var _errorcount = "${_errorcount}";

var wlanOn = "on";
var wlanRadio0 = "${wlanRadio0}";
var wlanRadio1 = "${wlanRadio1}";
if ( wlanRadio0 != "on" && wlanRadio1 != "on" ){
	wlanOn = "off";
}

var lastSyncTime = "${lastSyncTime}";

var notAvail = "${notAvail}";

bubbleEnabled = "${bubbleEnabled}";

showBubbleDialog = "${showBubbleDialog}";
bubbleText = "${bubbleText}";

var target = "settings";

$( document ).ready(function() {
	$("#buttonApply").click(function(e){
		$("#bubbleEnabled").prop("disabled",false);
		$("#post_okdir").val("wireless").prop("disabled", false);
		$("#post_okpage").val("home").prop("disabled", false);
		$("#wpsClone0,#wpsClone1").prop("disabled",true);
		$("#form_post").submit();
		// $("#spinner").hide();	
		e.preventDefault();
	});
	$("#tmp_bubbleOnOff").click(function(e){
		var iV = $('#tmp_bubbleOnOff').prop('checked');
		switch ( iV ){
			case false:
				bubbleSetOff();
				showBubbleDialog = "no";
				$("#modalFooter").hide();
        		$("#postShowDialogAgain").prop("disabled", true);
			break;
			case true:
				bubbleSetOn();
				// evtl. show bubble message and set bubbleShowAgain = yes
				ifBubbleEnabled = "yes";
				showBubbleDialog = "yes";
				onBubbleSite = "yes";
				// if(showBubbleDialog == "yes"){
				//	$("#postShowDialogAgain").val("yes");
				// }
				// showBubbleDialog("no");
			break;
		}
	});
	$("#buttonWPSClone").click(function(e){
		$("#bubbleEnabled").prop("disabled", true);
		$("#wpsClone0,#wpsClone1").prop("disabled",false).val("webui");
		switch(target){
			case "settings":
			$("#post_okdir").val("spec");
			$("#post_okpage").val("result");
			$("#post_okfollowdir").val("status");
			$("#post_okfollowpage").val("home");
			$("#post_okplain").val("1");
			$("#post_oktype").val("clonemode");
			break;
		}
		$(".okFormFields").prop("disabled", false);
		$("#spinner").show();	
		$("#form_post").submit();
		e.preventDefault();
	});
	init();
});

function init(){
	setBubbleMode();
	setLastSyncTime();
	setWPSCloneMode();	
}

/* bubble */
function setBubbleMode(){
	if (bubbleEnabled == "yes"){
		$("#tmp_bubbleOnOff").prop("checked", true);
		bubbleSetOn();
	}
}
function bubbleSetOff(){
	$("#bubbleEnabled").val("no");
	$("#divBubbleActive").removeClass("active").addClass("inactive");
	$("#divLastSync,#divConnectedDevices").removeClass("active").addClass("inactive");
}
function bubbleSetOn(){
	$("#bubbleEnabled").val("yes");
	$("#divBubbleActive").removeClass("inactive").addClass("active");
	$("#divLastSync,#divConnectedDevices").removeClass("inactive").addClass("active");
}

/* wps clone mode */
function setWPSCloneMode(){
	if (wlanOn == "on"){
		$("#divWPSClone").show();
	}
}
function setLastSyncTime(){
	if (lastSyncTime != ""){
		$("#timeLastSync").html(lastSyncTime);
	}
}