function getType(o) {
    return typeof obj;
}

function isBoolean(o) {
    return typeof o == "boolean";
}

function isString(o) {
    return typeof o == "string";
}

function isNumber(o) {
    return typeof o == "number";
}

function isFunction(o) {
    return typeof o == "function";
}

function isObject(o) {
    return typeof o == "object";
}

function isUndefined(o) {
    return typeof o == "undefined";
}

function isArray(o) {
    if (getObjType(o) == "Array") return true;
    else return false;
}

function $dvl(i) {
    return $("#" + i)[0];
}

function getValue(i) {
    return $("#" + i).val();
}

function setValue(i, v) {
    $("#" + i).val(v);
}

function copyValue(s, d) {
    $("#" + d).val($("#" + s).val());
}

function getProp(i, p) {
    return $("#" + i).prop(p);
}

function setProp(i, p, v) {
    $("#" + i).prop(p, v);
}

function copyProp(s, d, p) {
    $("#" + d).prop(p, $("#" + s).prop(p));
}

function getAttr(i, a) {
    return $("#" + i).attr(a);
}

function setAttr(i, a, v) {
    $("#" + i).attr(a, v);
}

function copyAttr(s, d, a) {
    $("#" + d).attr(a, $("#" + s).attr(a));
}

function getHtml(i) {
    return $("#" + i).html();
}

function setHtml(i, v) {
    $("#" + i).html(v);
}

function getCss(i, a) {
    return $("#" + i).css(a);
}

function setCss(i, a, v) {
    $("#" + i).css(a, v);
}

function getColor(i) {
    return getCss(i, "color");
}

function setColor(i, v) {
    setCss(i, "color", v);
}

function submitForm(i) {
    $("#" + i)[0].submit();
}

function getLength(i) {
    return getValue(i).length;
}

function checkLength(i, min, max) {
    return (getLength(i) >= min) && (getLength(i) <= max);
}

function utf8Escape(str) {
    return escape(utf8Encode(str));
}

function activeInactive(i) {
    return $("#" + i).removeClass("active").addClass("inactive")
}

function inactiveActive(i) {
    return $("#" + i).removeClass("inactive").addClass("active")
}

function hasError(i) {
    return $("#" + i).addClass("has-error")
}

function hasNoError(i) {
    return $("#" + i).removeClass("has-error")
}

function getData(i, d) {
    return $("#" + i).data(d);
}

function getObjType(obj) {
    if (obj == null) return "null";
    type = typeof obj;
    if (type != "object") return type;
    type = Object.prototype.toString.apply(obj);
    type = type.substring(8, type.length - 1);
    if (type != "Object") return type;
    if (obj.constructor == Object) return type;
    return "undefined";
}
Array.prototype.removeValArray = function(x) {
    var i;
    for (i in this) {
        if (this[i].toString() == x.toString()) {
            this.splice(i, 1)
        }
    }
}
Array.prototype.checkDoubValArray = function(x) {
    var i;
    for (i in this) {
        if (this[i].toString() == x.toString()) {
            return false;
        }
    }
    return true;
}
String.prototype.htmlEncode = function () {
    return String(this)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');

}

function setNavMainBuHi(v) {
    $("#navMainButton_" + v).removeClass("button-disabled");
}

function addslashes(str) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}
var ifModalFooterAll = "yes";
var ifModalFooter = "yes";
var ifBubbleEnabled = "no";
var showBubbleDialog = "no";
var onBubbleSite = "no";
var onChangeMessage = "yes";
var bubbleText = "";
// for checkForm and error messages
var errReg = {
    charValid: /^([0-9a-zA-Z]|\!|\"|\#|\$|\%|\&|\'|\(|\)|\*|\+|,|\-|\.|\/|\:|\;|\<|\=|\>|\?|\@|\[|\\|\]|\^|\_|\`|\{|\||\}|\~|\ )*$/,
    hourValid: /^(|[0-1]?[0-9]|[2][0-3])$/,
    minuteValid: /^(|[0-9]|[0-5][0-9])?$/,
    timeValid: /^([0-1]?[0-9]|[2][0-3]):([0-5][0-9])(:[0-5][0-9])?$/,
    macAdrValid: /^([0-9A-F]{2}[:-]?){5}([0-9A-F]{2})$/,
    ipValid: /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/,
    hostValid: /^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/
};
$(document).ready(function() {
    $(".linkNewPage").click(function(e) {
        $("#spinner").show();
    });
    $("#modalFooter, #modalFooterClose").click(function(e) {
        $("#modalFooter").hide();
        hideOnChangeMessage();
    });
    $(document).on('change', 'select,input:not([type=text],[type=password],[type=image],[type=file],[type=button],[type=submit])', function(e) {
        showOnChangeMessage();
    });
    // $(document).on('keyup', 'input[type=text]:not(.dynamic)', function(e) {
    // showOnChangeMessage();
    // });
    $(document).on('keyup keypress', 'input[type=text],input[type=password]', function(e) {
        cl = $(this).hasClass("dynamic");
        if(e.which == 13){
            if(e.type == "keypress"){
                e.preventDefault;
                return false;
            }else{
                if(cl){
                    dC = $(this).data("idclick");
                    if(dC){
                        $("#"+dC).click();
                    }
                    e.preventDefault();
                    return false;
                }
                // else{
                //    $("#buttonApply").click();
                // }
            }
        }else{
            if(!cl){
                showOnChangeMessage();
            }
        }
    });
    // $(document).on('submit', function(e) {
    //    hideOnChangeMessage();
    // });
    $("#buttonApplyT").on('click', function(e) {
        $("#buttonApply").click();
    });
    $(window).unload(function() {
        $('#spinner').hide();
    });
});

function showOnChangeMessage() {
    $("#divButtonApplyInactive").addClass("inactive").removeClass("active");
    $("#divButtonApply").removeClass("inactive").addClass("active");
    /*
    if (ifModalFooter == "yes") {
        $("#modalFooter").show();
        $("#modalFooterText").show();
        if (showBubbleDialog == "yes" && ifBubbleEnabled == "yes") {
            $("#modalFooterText2").html(bubbleText);
            $("#postShowDialogAgain").val(onBubbleSite).prop("disabled", false);
        } else {
            $("#modalFooterText2").html("");
            $("#postShowDialogAgain").prop("disabled", true);
        }
    }
    */
}

function hideOnChangeMessage() {
    // $("#modalFooter").hide();
    $("#divButtonApplyInactive").addClass("active").removeClass("inactive");
    $("#divButtonApply").removeClass("active").addClass("inactive");
}

function _getJSON(u, f, t) {
    var url = u;
    var func = f;
    xhr = $.ajax({
        dataType: "json",
        url: url,
        success: function(json) {
            func(json, 0);
        },
        error: function(e) {
            func(false, e);
        }
    });
}

function prefLngToNetmask(v) {
    var byte = 255;
    var prefLen = parseInt(v);
    var netmaskString = "";
    if (prefLen < 0) {
        netmaskString = "";
    } else if (prefLen < 8) {
        for (var i = (7 - prefLen); i >= 0; i--) {
            byte -= Math.pow(2, i);
        }
        netmaskString = byte + ".0.0.0";
    } else if (prefLen < 16) {
        for (var i = (7 - (prefLen - 8)); i >= 0; i--) {
            byte -= Math.pow(2, i);
        }
        netmaskString = "255." + byte + ".0.0";
    } else if (prefLen < 24) {
        for (var i = (7 - (prefLen - 16)); i >= 0; i--) {
            byte -= Math.pow(2, i);
        }
        netmaskString = "255.255." + byte + ".0";
    } else if (prefLen < 32) {
        for (var i = (7 - (prefLen - 24)); i >= 0; i--) {
            byte -= Math.pow(2, i);
        }
        netmaskString = "255.255.255." + byte;
    } else if (prefLen == 32) {
        netmaskString = "255.255.255.255";
    } else {
        netmaskString = "";
    }
    return netmaskString;
}

