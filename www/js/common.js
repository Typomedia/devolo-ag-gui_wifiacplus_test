function getType(o){return typeof obj;}
function isBoolean(o){return typeof o=="boolean";}
function isString(o){return typeof o=="string";}
function isNumber(o){return typeof o=="number";}
function isFunction(o){return typeof o=="function";}
function isObject(o){return typeof o=="object";}
function isUndefined(o){return typeof o=="undefined";}
function isArray(o){if(getObjType(o)=="Array")return true;else return false;}

function $dvl(i){return $("#"+i)[0];}
function getValue(i){return $("#"+i).val();}
function setValue(i,v){$("#"+i).val(v);}
function copyValue(s,d){$("#"+d).val($("#"+s).val());}
function getProp(i,p){return $("#"+i).prop(p);}
function setProp(i,p,v){$("#"+i).prop(p,v);}
function copyProp(s,d,p){$("#"+d).prop(p,$("#"+s).prop(p));}
function getAttr(i,a){return $("#"+i).attr(a);}
function setAttr(i,a,v){$("#"+i).attr(a,v);}
function copyAttr(s,d,a){$("#"+d).attr(a,$("#"+s).attr(a));}
function getHtml(i){return $("#"+i).html();}
function setHtml(i,v){$("#"+i).html(v);}
function getCss(i,a){return $("#"+i).css(a);}
function setCss(i,a,v){$("#"+i).css(a,v);}
function getColor(i){return getCss(i,"color");}
function setColor(i,v){setCss(i,"color",v);}
function submitForm(i){$("#"+i)[0].submit();}
function getLength(i){return getValue(i).length;}
function checkLength(i,min,max){return (getLength(i)>=min)&&(getLength(i)<=max);}
function utf8Escape(str) { return escape(utf8Encode(str)); }

function setError(i, e)
{
    if (!e || e == "")
        e = "&nbsp;";
    setHtml("error_" + i, e);
}
function numErrors()
{
    var num = 0;
    $(".error_msg").each(function() {
        if ($(this).css("visibility") != "hidden" && $(this).css("display") != "none")
            if ($(this).html() != "" && $(this).html() != "&nbsp;")
                num++;
    });
    return num;
}
function doCustomValidation(i, t, func)
{
    $.getJSON("/cgi-bin/htmlmgr?_file=validate&_lang=" + global_language + "&_type=" + t + "&_value=" + getValue(i), function(data, status) {
        if (status == "success")
        {
            func(i, data);
            if (typeof(on_validation) == "function")
                on_validation(numErrors());
        }
    });
}
function doValidation(i, t)
{
    doCustomValidation(i, t, function(i, data) {
        setError(i, data);
    });
}

var initial_value_list   = new Array();
var initial_checked_list = new Array();
// addModHandler... must not be called before the control has been
// set to its initial value!
function addHandler(i, evname, func)
{
	var elem = document.getElementById(i);
	if (elem.addEventListener)  // all browsers except IE < 9.0
	{
		elem.addEventListener(evname, func, false);
	}
	else  // IE < 9.0
	{
		elem.attachEvent("on" + evname, func);
	}
}
function addValidateHandler(i, func)
{
	addHandler(i, "change", func);
	addHandler(i, "keyup",  func);
	addHandler(i, "paste",  func);
	func();
}
function addModHandlerCheckbox(i)
{
	initial_checked_list[i] = document.getElementById(i).checked;
	addHandler(i, "click", checkModified);
}
function addModHandlerEdit(i)
{
	initial_value_list[i] = document.getElementById(i).value;
	addHandler(i, "change", checkModified);
	addHandler(i, "keyup",  checkModified);
	addHandler(i, "paste",  setModified);
}
function addModHandlerSelect(i)
{
    initial_value_list[i] = document.getElementById(i).value;
    addHandler(i, "click", checkModified);
    addHandler(i, "keyup",  checkModified);
}
function setModified(e)
{
	var mm = document.getElementById("modified_msg");
	var mm2 = document.getElementById("modified_msg2");
	if (mm) 
	{
		mm.style.visibility = "visible";
	}

	if (mm2)
	{
		mm2.style.visibility = "visible";
	}
}
function checkModified(e)
{
	var mm = document.getElementById("modified_msg");
	var mm2 = document.getElementById("modified_msg2");

	if (!mm || mm.style.visibility == "visible")
	{
		return true;
	}
	
	for (var i in initial_value_list)
	{
		var elem = document.getElementById(i);
		if (elem && !elem.disabled && elem.value != initial_value_list[i])
		{
			setModified(e);
			return true;
		}
	}
	for (var i in initial_checked_list)
	{
		var elem = document.getElementById(i);
		if (elem && elem.disabled == false && elem.checked != initial_checked_list[i])
		{
			setModified(e);
			return true;
		}
	}
	
	return true;
}

function getObjType(obj)
{
    if (obj == null) return "null";
    type = typeof obj;
    if (type != "object") return type;
    type = Object.prototype.toString.apply(obj);
    type = type.substring(8, type.length - 1);
    if (type != "Object") return type;
    if (obj.constructor == Object) return type;
    return "undefined";
}

/** @class dvlXhr
 * Handle the XML HTTP request.
 */

/*@cc_on @if (@_win32 && @_jscript_version >= 5) if (!window.XMLHttpRequest)
window.XMLHttpRequest = function() { return new ActiveXObject('Microsoft.XMLHTTP') }
@end @*/

function dvlXhr(method, type, trimResp)
{
  var xhr = new XMLHttpRequest();

  var method = (method == "POST")?"POST":"GET";
  var type = (type == "Xml")?"Xml":"Text";
  var trim = (trimResp && (trimResp == true))?true:false;

  this.send = function(url, dat, cmd)
  {
    xhr.open(method, url, true);
    if(cmd)
    {
      xhr.onreadystatechange = function()
      {
        if(xhr.readyState == 4)
        {
          var error = true;
          var response = null;
          if(xhr.status == 200)
          {
            error = false;
            response = new String(xhr.responseText);
            if(type == "Xml")
            {
              if (window.ActiveXObject)
              {
                var xml = new ActiveXObject("Microsoft.XMLDOM");
                xml.loadXML(response);
                response = xml;
              }
              else if (document.implementation)
              {
                response = (new DOMParser()).parseFromString(response, "text/xml");
              }
            }
            else if((type = "Text") && (trim == true)) response = response.trim();
          }
          cmd(response, error);
        }
      }
    }
    xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    xhr.send(dat);
  }
  
  this.abort = function()
  {
    xhr.abort();
  }
}/** @class dvlXhrFio
 * In-Order-Execution of several XMLHttpRequests.
 */

function dvlXhrFifo(method, type, trim)
{
  if(!trim) var trim = false;
  var xhr = new dvlXhr(method, type, trim);
  var fifo = new Array();
  var lock = false;
  var cmdCall = null;

  this.add = function(url, data, cmd)
  {
    fifo.push({"url":url, "data":data, "cmd":cmd});
    if(!lock) process("", false);
  }

  this.clear = function(cmd)
  {
    var newfifo = new Array();
    
    if (cmd != null)
    {
      for (var i = 0; i < fifo.length; i++)
      {
        if (fifo[i]["cmd"] != cmd)
        {
          newfifo.push(fifo[i]);
        }
      }
    }
    
    fifo = newfifo;
  }

  function process(resp, err)
  {
    if(cmdCall) cmdCall(resp, err);
    if(fifo.length >= 1)
    {
      lock = true;
      cmdCall = fifo[0].cmd;
      xhr.send(fifo[0].url, fifo[0].data, process);
      fifo.splice(0,1);
    }
    else
    {
      cmdCall = null;
      lock = false;
    }
  }  
}
function utf8Encode(inStr)
{
    inStr = inStr.replace(/\r\n/g,"\n");
    var outStr = "";

    for (var strPos = 0; strPos < inStr.length; strPos++)
    {
        var curChar = inStr.charCodeAt(strPos);

        if (curChar < 128) outStr += String.fromCharCode(curChar);
        else if ((curChar >= 128) && (curChar < 2048))
        {
            outStr += String.fromCharCode((curChar >> 6) | 192);
            outStr += String.fromCharCode((curChar & 63) | 128);
        }
        else if ((curChar >= 2048) && (curChar < 65536))
        {
            outStr += String.fromCharCode((curChar >> 12) | 224);
            outStr += String.fromCharCode(((curChar >> 6) & 63) | 128);
            outStr += String.fromCharCode((curChar & 63) | 128);
        }
        else
        {
            outStr += String.fromCharCode((curChar >> 18) | 240);
            outStr += String.fromCharCode(((curChar >> 12) & 63) | 128);
            outStr += String.fromCharCode(((curChar >> 6) & 63) | 128);
            outStr += String.fromCharCode((curChar & 63) | 128);
        }
    }

    return outStr;
}

function tagEncode(inStr)
{
    var outStr = "";

    for (var strPos = 0; strPos < inStr.length; strPos++)
    {
        var curChar = inStr.charCodeAt(strPos);

        if (curChar == 60) outStr += '&lt;';
        else if (curChar == 62) outStr += '&gt;';
        else outStr += String.fromCharCode(curChar);
    }

    return outStr;
}
